

#include <assert.h>
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>




extern "C" {

  void
  __VERIFIER_assert(int x)
  {
    if (!x) {
      void *array[10];
      size_t size;

      // get void*'s for all entries on the stack
      size = backtrace(array, 10);

      // print out all the frames to stderr
      fprintf(stderr, "Assert failed");
      backtrace_symbols_fd(array, size, STDERR_FILENO);
      exit(1);
    }
  }

  char**
  vulfi_init(int argc, char **argv, int *new_argc)
  {
    *new_argc = argc;
    return argv;
  }

  void vulfi_finalize(void) {
    return;
  }

}
