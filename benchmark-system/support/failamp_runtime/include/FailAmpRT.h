#ifndef FAILAMPRT_H
#define FAILAMPRT_H


#include <signal.h>
#include <stdio.h>
#include <stdlib.h>




#ifdef __cplusplus
extern "C" {
#endif

  void
  catch_detection(int sig)
  {
    if (sig == SIGUSR1) {
      fprintf(stderr, "Error detected\n");
      exit(42);
    }
  }


  void failamp_init() {
    static int first = 1;
    if (first) {
      if (signal(SIGUSR1, catch_detection) == SIG_ERR) {
	fprintf(stderr, "FATAL ERROR: Unable to register signal handler!\n");
	exit(-1);
      }
      first = 0;
    }
    return;
  }

#ifdef __cplusplus
}
#endif


#endif // #ifndef FAILAMPRT_H
