#ifndef UTILITIES_HPP
#define UTILITIES_HPP


#include <cmath>
#include <ctime>
#include <vector>




typedef double DATA_TYPE;
#define DATA_TYPE_NAME "double"
#define DATA_TYPE_FMT "%1.15e"
#define SQRT(x) (sqrt(x))
#define EXP(x) (exp(x))
#define POW(x, y) (pow(x, y))



void* xmalloc(long size);
DATA_TYPE* malloc_single_1d(long n);
DATA_TYPE* malloc_multi_1d(long n);
DATA_TYPE* malloc_single_2d(long n, long m);
DATA_TYPE** malloc_multi_2d(long n, long m);
DATA_TYPE* malloc_single_3d(long n, long m, long l);
DATA_TYPE*** malloc_multi_3d(long n, long m, long l);


DATA_TYPE rand_data_type();
void init_single_1d(long n, DATA_TYPE* data, int seed);
void init_multi_1d(long n, DATA_TYPE* data, int seed);
void init_single_2d(long n, long m, DATA_TYPE* data, int seed);
void init_multi_2d(long n, long m, DATA_TYPE** data, int seed);
void init_single_3d(long n, long m, long l, DATA_TYPE* data, int seed);
void init_multi_3d(long n, long m, long l, DATA_TYPE*** data, int seed);


void set_single_1d(long n, DATA_TYPE* data, DATA_TYPE val);
void set_multi_1d(long n, DATA_TYPE* data, DATA_TYPE val);
void set_single_2d(long n, long m, DATA_TYPE* data, DATA_TYPE val);
void set_multi_2d(long n, long m, DATA_TYPE** data, DATA_TYPE val);
void set_single_3d(long n, long m, long l, DATA_TYPE* data, DATA_TYPE val);
void set_multi_3d(long n, long m, long l, DATA_TYPE*** data, DATA_TYPE val);


void copy_single_1d(long n, DATA_TYPE* dest, DATA_TYPE* src);
void copy_multi_1d(long n, DATA_TYPE* dest, DATA_TYPE* src);
void copy_single_2d(long n, long m, DATA_TYPE* dest, DATA_TYPE* src);
void copy_multi_2d(long n, long m, DATA_TYPE** dest, DATA_TYPE** src);
void copy_single_3d(long n, long m, long l, DATA_TYPE* dest, DATA_TYPE* src);
void copy_multi_3d(long n, long m, long l, DATA_TYPE*** dest, DATA_TYPE*** src);


void print_single_1d(long n, DATA_TYPE* data);
void print_multi_1d(long n, DATA_TYPE* data);
void print_single_2d(long n, long m, DATA_TYPE* data);
void print_multi_2d(long n, long m, DATA_TYPE** data);
void print_single_3d(long n, long m, long l, DATA_TYPE* data);
void print_multi_3d(long n, long m, long l, DATA_TYPE*** data);


void free_single_1d(DATA_TYPE* data);
void free_multi_1d(DATA_TYPE* data);
void free_single_2d(long n, long m, DATA_TYPE* data);
void free_multi_2d(long n, long m, DATA_TYPE** data);
void free_single_3d(long n, long m, long l, DATA_TYPE* data);
void free_multi_3d(long n, long m, long l, DATA_TYPE*** data);


#define MY_CLOCK CLOCK_MONOTONIC_RAW

typedef struct timespec timepoint;

class time_series {
public:
  time_series();

  void start();
  double end();
  void reset();

  double average();
  double stddev();
  double sum();


private:
  bool active;
  timepoint start_time;
  std::vector<double> times;
};


#endif // #ifndef UTILITIES_HPP
