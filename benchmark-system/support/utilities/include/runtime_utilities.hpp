// no include protector since including this in any place other than the
// file with main is not intended use

#if defined(USE_SMACK)
#include "smack.h"
#endif

#if defined(USE_VULFI)
#include "Corrupt.hpp"
#endif

#if defined(USE_PRESAGE)
#include "PresageRT.h"
#endif

#if defined(USE_FAILAMP)
#include "FailAmpRT.h"
#endif


#include <assert.h>
#include <stdlib.h>
#include <string.h>




char** init(int vargc, char** vargv, int* argcp)
{
#if defined(USE_PRESAGE)
  psgProtect(NULL, 0, 0);
#endif

#if defined(USE_FAILAMP)
  failamp_init();
#endif


#if defined(USE_SMACK)
  *argcp = 1 + __VERIFIER_nondet_unsigned_char()%10;
  return;

#elif defined(USE_VULFI)
  return vulfi_init(vargc, vargv, argcp);

#else
  *argcp = vargc;
  return vargv;

#endif
}


void finalize(void)
{
#if defined(USE_VULFI)
  vulfi_finalize();
#endif
}


long parse_long(const char* str, long min, long max, long* out)
{
  assert(min < max);

#if defined(USE_SMACK)
  if (max - min < UCHAR_MAX) {
    *out = min + __VERIFIER_nondet_unsigned_char();
  } else {
    *out = min + __VERIFIER_nondet_unsigned_char()%(max-min);
  }
  return 0;

#else
  assert(str != nullptr);

  char* endptr;
  long long parsed = strtoll(str, &endptr, 0);
  const char* expected_endptr = str + strlen(str);

  if (endptr != expected_endptr) {
    return 1;
  }

  if ((parsed < (long long) min) || (parsed > (long long) max)) {
    return 1;
  }

  *out = (long) parsed;
  return 0;

#endif
}
