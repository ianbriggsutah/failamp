

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




using namespace std;




time_series::time_series() {
  this->active = false;
  this->times = vector<double>{};
}


void
time_series::start()
{
  assert(this->active == false);

  clock_gettime(MY_CLOCK, &this->start_time);
  this->active = true;
}


double
time_series::end()
{
  assert(this->active == true);

  timepoint end;
  clock_gettime(MY_CLOCK, &end);

  // from http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
  timepoint elapsed_tp;

  if (end.tv_nsec < this->start_time.tv_nsec) {
    elapsed_tp.tv_sec = end.tv_sec - this->start_time.tv_sec - 1;
    elapsed_tp.tv_nsec = (1000000000 + end.tv_nsec - this->start_time.tv_nsec);
  } else {
    elapsed_tp.tv_sec = end.tv_sec - this->start_time.tv_sec;
    elapsed_tp.tv_nsec = end.tv_nsec - this->start_time.tv_nsec;
  }

  const double elapsed = (((double) elapsed_tp.tv_sec)
			  + (((double) elapsed_tp.tv_nsec) / 1.0e9));

  this->times.push_back(elapsed);
  this->active = false;

  return elapsed;
}


void
time_series::reset()
{
  assert(this->active == false);

  this->times.clear();
}


double
time_series::average()
{
  assert(this->times.size() >= 1);

  const double sum = this->sum();
  const double avg = sum / this->times.size();

  return avg;
}


double
time_series::stddev()
{
  assert(this->times.size() >= 1);

  if (this->times.size() == 2) {
    return NAN;
  }

  const double K = this->average();
  double sum = 0.0;
  double sum_sqr = 0.0;

  for (const double x : this->times) {
    const double x_m_K = x - K;
    sum += x_m_K;
    const double x_m_K_sqr = x_m_K * x_m_K;
    sum_sqr += x_m_K_sqr;
  }

  const size_t n = this->times.size();
  const double sum_sum = sum * sum;
  const double sum_sum_avg = sum_sum / n;
  const double sum_sqr_m_sum_sum_avg = sum_sqr - sum_sum_avg;
  const double variance = sum_sqr_m_sum_sum_avg / (n - 1);
  const double dev = std::sqrt(variance);

  return dev;
}


double
time_series::sum()
{
  double sum = 0.0;
  for (const double x : this->times) {
    sum += x;
  }

  return sum;
}
