

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




void
copy_single_1d(long n, DATA_TYPE* dest, DATA_TYPE* src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);

  for (long i=0; i<n; i++) {
    dest[i] = src[i];
  }
}


void
copy_single_2d(long n, long m, DATA_TYPE* dest, DATA_TYPE* src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);
  assert(m > 0);

  copy_single_1d(n*m, dest, src);
}


void
copy_single_3d(long n, long m, long l, DATA_TYPE* dest, DATA_TYPE* src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  copy_single_1d(n*m*l, dest, src);
}




void
copy_multi_1d(long n, DATA_TYPE* dest, DATA_TYPE* src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);

  copy_single_1d(n, dest, src);
}


void
copy_multi_2d(long n, long m, DATA_TYPE** dest, DATA_TYPE** src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);
  assert(m > 0);

  for (long i=0; i<m; i++) {
    assert(src[i] != nullptr);
    assert(dest[i] != nullptr);
    copy_multi_1d(n, dest[i], src[i]);
  }
}


void
copy_multi_3d(long n, long m, long l, DATA_TYPE*** dest, DATA_TYPE*** src)
{
  assert(src != nullptr);
  assert(dest != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  for (long i=0; i<l; i++) {
    assert(src[i] != nullptr);
    assert(dest[i] != nullptr);
    copy_multi_2d(n, m, dest[i], src[i]);
  }
}
