

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




void
free_single_1d(DATA_TYPE* data)
{
  assert(data != nullptr);

  free(data);
}


void
free_single_2d(long n, long m, DATA_TYPE* data)
{
  assert(n > 0);
  assert(m > 0);
  assert(data != nullptr);

  free_single_1d(data);
}


void
free_single_3d(long n, long m, long l, DATA_TYPE* data)
{
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);
  assert(data != nullptr);

  free_single_1d(data);
}




void
free_multi_1d(DATA_TYPE* data)
{
  assert(data != nullptr);

  free_single_1d(data);
}


void
free_multi_2d(long n, long m, DATA_TYPE** data)
{
  assert(n > 0);
  assert(m > 0);
  assert(data != nullptr);

  for (long i=0; i<n; i++) {
    assert(data[i] != nullptr);
    free_multi_1d(data[i]);
    data[i] = nullptr;
  }
  free(data);
}


void
free_multi_3d(long n, long m, long l, DATA_TYPE*** data)
{
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);
  assert(data != nullptr);

  for (long i=0; i<l; i++) {
    assert(data[i] != nullptr);
    free_multi_2d(n, m, data[i]);
    data[i] = nullptr;
  }
  free(data);
}
