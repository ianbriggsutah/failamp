

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




DATA_TYPE
rand_data_type()
{
  return ((DATA_TYPE) rand()) / ((DATA_TYPE) RAND_MAX) * ((DATA_TYPE) 1e32);
}




static void
seedless_init(long n, DATA_TYPE* data)
{
  for (long i=0; i<n; i++) {
    data[i] = rand_data_type();
  }
}




void
init_single_1d(long n, DATA_TYPE* data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);

  srand(seed);
  seedless_init(n, data);
}


void
init_single_2d(long n, long m, DATA_TYPE* data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  srand(seed);
  seedless_init(n*m, data);
}


void
init_single_3d(long n, long m, long l, DATA_TYPE* data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  srand(seed);
  seedless_init(n*m*l, data);
}




void
init_multi_1d(long n, DATA_TYPE* data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);

  srand(seed);
  seedless_init(n, data);
}


void
init_multi_2d(long n, long m, DATA_TYPE** data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  srand(seed);

  for (long i=0; i<m; i++) {
    assert(data[i] != nullptr);
    seedless_init(n, data[i]);
  }
}


void
init_multi_3d(long n, long m, long l, DATA_TYPE*** data, int seed)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  srand(seed);

  for (long i=0; i<l; i++) {
    assert(data[i] != nullptr);
    for (long j=0; j<m; j++) {
      assert(data[i][j] != nullptr);
      for (long k=0; k<n; k++) {
	seedless_init(n, data[i][j]);
      }
    }
  }
}
