

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




void
print_single_1d(long n, DATA_TYPE* data)
{
  assert(data != nullptr);
  assert(n > 0);

  printf("%ld\n", n);
  for (long i=0; i<n; i++) {
    printf(DATA_TYPE_FMT, data[i]);
    printf(" ");
  }
  printf("\n");
}


void
print_single_2d(long n, long m, DATA_TYPE* data)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  printf("%ld %ld\n", n, m);
  for (long i=0; i<m; i++) {
    for (long j=0; j<n; j++) {
      printf(DATA_TYPE_FMT, data[i*n + j]);
      printf(" ");
    }
    printf("\n");
  }
}


void
print_single_3d(long n, long m, long l, DATA_TYPE* data)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  printf("%ld %ld %ld\n", n, m, l);
  for (long i=0; i<l; i++) {
    for (long j=0; j<m; j++) {
      for (long k=0; k<n; k++) {
	printf(DATA_TYPE_FMT, data[i*m*n + j*n + k]);
	printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
}




void
print_multi_1d(long n, DATA_TYPE* data)
{
  assert(data != nullptr);
  assert(n > 0);

  print_single_1d(n, data);
}


void
print_multi_2d(long n, long m, DATA_TYPE** data)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  printf("%ld %ld\n", n, m);
  for (long i=0; i<m; i++) {
    assert(data[i] != nullptr);
    for (long j=0; j<n; j++) {
      printf(DATA_TYPE_FMT, data[i][j]);
      printf(" ");
    }
    printf("\n");
  }
}


void
print_multi_3d(long n, long m, long l, DATA_TYPE*** data)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  printf("%ld %ld %ld\n", n, m, l);
  for (long i=0; i<l; i++) {
    assert(data[i] != nullptr);
    for (long j=0; j<m; j++) {
      assert(data[i][j] != nullptr);
      for (long k=0; k<n; k++) {
	printf(DATA_TYPE_FMT, data[i][j][k]);
	printf(" ");
      }
      printf("\n");
    }
    printf("\n");
  }
}
