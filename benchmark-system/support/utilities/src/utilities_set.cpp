

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




void
set_single_1d(long n, DATA_TYPE* data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);

  for (long i=0; i<n; i++) {
    data[i] = val;
  }
}


void
set_single_2d(long n, long m, DATA_TYPE* data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  set_single_1d(n*m, data, val);
}


void
set_single_3d(long n, long m, long l, DATA_TYPE* data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  set_single_1d(n*m*l, data, val);
}




void
set_multi_1d(long n, DATA_TYPE* data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);

  set_single_1d(n, data, val);
}


void
set_multi_2d(long n, long m, DATA_TYPE** data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);

  for (long i=0; i<m; i++) {
    assert(data[i] != nullptr);
    set_multi_1d(n, data[i], val);
  }
}


void
set_multi_3d(long n, long m, long l, DATA_TYPE*** data, DATA_TYPE val)
{
  assert(data != nullptr);
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  for (long i=0; i<l; i++) {
    assert(data[i] != nullptr);
    set_multi_2d(n, m,  data[i], val);
  }
}
