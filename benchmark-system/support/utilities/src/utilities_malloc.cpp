

#include "utilities.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>




void*
xmalloc(long size)
{
  assert(size > 0);

  void* tmp = malloc(size);
  if (tmp == nullptr) {
    fprintf(stderr, "Unable to malloc memory: exiting unceremoniously\n");
    exit(2);
  }

  return tmp;
}




DATA_TYPE*
malloc_single_1d(long n)
{
  assert(n > 0);

  return (DATA_TYPE*) xmalloc(sizeof(DATA_TYPE)*n);
}


DATA_TYPE*
malloc_single_2d(long n, long m)
{
  assert(n > 0);
  assert(m > 0);

  return malloc_single_1d(n*m);
}


DATA_TYPE*
malloc_single_3d(long n, long m, long l)
{
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  return malloc_single_1d(n*m*l);
}




DATA_TYPE*
malloc_multi_1d(long n)
{
  assert(n > 0);

  return malloc_single_1d(n);
}


DATA_TYPE**
malloc_multi_2d(long n, long m)
{
  assert(n > 0);
  assert(m > 0);

  DATA_TYPE** temp = (DATA_TYPE**) xmalloc(sizeof(DATA_TYPE*)*n);
  for (long i=0; i<n; i++) {
    temp[i] = malloc_multi_1d(m);
  }

  return temp;
}


DATA_TYPE***
malloc_multi_3d(long n, long m, long l)
{
  assert(n > 0);
  assert(m > 0);
  assert(l > 0);

  DATA_TYPE*** temp = (DATA_TYPE***) xmalloc(sizeof(DATA_TYPE**)*n);
  for (long i=0; i<l; i++) {
    temp[i] = malloc_multi_2d(m, n);
  }

  return temp;
}
