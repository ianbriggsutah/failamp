

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#include "Corrupt.hpp"


#define UNUSED(x) (void)x;

long long detectCounter = 0;

static char* target_name = (char*) "UNDEFINED EXECUTABLE TARGET NAME";

static enum Mode mode = UNSET;
static struct Analysis anal_data;
static struct Injections inj_data;




/*******************************************************************************/
/* general helper functions                                                    */
/*******************************************************************************/
static void
usage(void)
{
  assert(target_name != NULL);
  char* usage_text = (char*)
    "The executable has been modified with vulfi.\n"
    "To perform error injection first run with the following arguments.\n"
    "This will set up an anylisis of the executable.\n"
    "  %s analyze <analyze_filename> <target arguments ...>\n"
    "Then to perform the actual injection run with these arguments.\n"
    "  %s inject <analyze_filename> <error_count> <lower_bit_index> <upper_bit_index> <injection_log_filename> <target_arguments ...>\n"
    "This will inject the requested number of bit flips in the bit range specified.\n"
    "or to perform a targeted injection run with these arguments.\n"
    "  %s targeted <analyze_filename> <instruction_number> <invocation_number> <bit_index> <injection_log_filename> <target_arguments ...>\n"
    "This will inject the requested error.\n";
  fprintf(stderr, usage_text, target_name, target_name, target_name);
}


static void
error(const char* msg)
{
  assert(msg != NULL);

  fprintf(stderr, "ERROR: ");
  fprintf(stderr, "%s", msg);
  usage();
  exit(1);
}


static void
resource_error(const char* msg)
{
  assert(msg != NULL);

  fprintf(stderr, "RESOURCE ERROR: ");
  fprintf(stderr, "%s", msg);
  exit(2);
}


static void
internal_error(const char* msg)
{
  assert(msg != NULL);

  fprintf(stderr, "INTERNAL ERROR: ");
  fprintf(stderr, "%s", msg);
  exit(3);
}


static void*
safe_malloc(const size_t size)
{
  void* retval = malloc(size);
  if (retval == NULL) {
    resource_error("unable to malloc memory\n");
  }

  return retval;
}


static uint64_t
static_random()
{
  static int first_run = 1;
  static const size_t statelen = 256;
  static char statebuf[statelen];
  static struct random_data buf;

  if (first_run == 1) {
    unsigned int seed = ((unsigned int) time(NULL)) * ((unsigned int) getpid());
    initstate_r(seed, statebuf, statelen, &buf);
    first_run = 0;
  }

  int32_t result;
  int valid = random_r(&buf, &result);
  if (valid == -1) {
    internal_error("random_r failed\n");
  }

  return (uint64_t) result;
}


static uint64_t
random_choice(uint64_t low, uint64_t high)
{
  static size_t valid_bits = 0;
  static uint64_t rand_bitfield = 0;

  assert(low <= high);

  if (low == high) {
    return low;
  }

  const uint64_t width          = high - low + 1;
  const uint64_t repeats        = UINT64_MAX / width;
  const uint64_t retry_if_under = repeats * width;

  uint64_t rand;
  do {
    if (2 > valid_bits) {
      rand_bitfield <<= 31;
      rand_bitfield |= static_random();
      valid_bits += 31;
    }
    rand = ((static_random() << 33) |
	    (static_random() << 2) |
	    (rand_bitfield & 0x3));
    rand_bitfield >>= 2;
    valid_bits -= 2;
  } while (rand > retry_if_under);

  const uint64_t offset = rand % width;
  const uint64_t retval = low + offset;

  assert(high >= retval);
  assert(retval >= low);
  return retval;
}


static char*
new_string(const char* str)
{
  assert(str != NULL);

  const int chars = strlen(str)+10;
  char* retval = (char*) safe_malloc(sizeof(char) * chars);
  strncpy(retval, str, chars);

  return retval;
}




/*******************************************************************************/
/* argument processing helper functions                                        */
/*******************************************************************************/
static char*
hash_file(const char* filename)
{
  assert(filename != NULL);

  const int cmd_len = 5 + 1 + strlen(filename) + 1;
  char* cmd = (char*) safe_malloc(sizeof(char) * cmd_len);
  const int written = snprintf(cmd, cmd_len, "cksum %s", filename);
  if ((written < 0) || (written == cmd_len)) {
    internal_error("call to chksum not written fully\n");
  }

  FILE* fp = popen(cmd, "r");
  if (fp == NULL) {
    internal_error("unable to run cksum\n");
  }

  const int buflen = 4096;
  char* buf = (char*) safe_malloc(sizeof(char) * buflen); //BAD static buffer size

  const char* valid = fgets(buf, buflen, fp);
  if (valid == NULL) {
    internal_error("unable to read from cksum\n");
  }

  const int close_status = pclose(fp);
  if (close_status == -1) {
    internal_error("unable to run cksum\n");
  } else if (!WIFEXITED(close_status)) {
    internal_error("cksum returned an error\n");
  }

  char* c = buf;
  while (*c != ' ') {
    if (*c == '\0') { internal_error("unable to parse output of cksum\n"); }
    c++;
  }
  c++;
  while (*c != ' ') {
    if (*c == '\0') { internal_error("unable to parse output of cksum\n"); }
    c++;
  }
  *c = '\0';

  assert(cmd != NULL);
  free(cmd);
  return buf;
}


static enum Mode
get_mode(const char* str)
{
  if (strcmp(str, "analyze") == 0 ||
      strcmp(str, "Analyze") == 0 ||
      strcmp(str, "ANALYZE") == 0) {
    return ANALYZE;
  }

  if (strcmp(str, "inject") == 0 ||
      strcmp(str, "Inject") == 0 ||
      strcmp(str, "INJECT") == 0) {
    return INJECT;
  }

  if (strcmp(str, "targeted") == 0 ||
      strcmp(str, "Targeted") == 0 ||
      strcmp(str, "TARGETED") == 0) {
    return TARGETED;
  }

  error("mode must be one of (analyze, inject)");
  return UNSET;
}


static char**
copy_argv(const int argc, char* *argv, const int executable_argc)
{
  assert(argc > 0);
  assert(executable_argc < argc);
  assert(argv != NULL);

  char** argv2 = (char**) safe_malloc(sizeof(char*) * executable_argc);

  assert(argv[0] != NULL);
  argv2[0] = new_string(argv[0]);

  for (int i=(argc - executable_argc + 1), j=1; i<argc; i++, j++) {
    assert(argv[i] != NULL);
    argv2[j] = new_string(argv[i]);
  }

  return argv2;
}


static void
swap(char** a, const int i, const int j)
{
  assert(a != NULL);
  assert(i >= 0);
  assert(j >= 0);

  char* temp = a[i];
  a[i] = a[j];
  a[j] = temp;
}


static size_t
get_size_t(const char* str)
{
  assert(str != NULL);

  const int len = strlen(str);
  const char* expected_endptr = str + len;

  char* endptr;
  const unsigned long long retval = strtoull(str, &endptr, 0);

  if ((retval == ULLONG_MAX) && (errno == ERANGE)) {
    error("input value was too large\n");
  }

  if (endptr != expected_endptr) {
    error("input string expecting number was non-numeric\n");
  }

  return (size_t) retval;
}




/*******************************************************************************/
/* analysis data operations                                                    */
/*******************************************************************************/
static int
check_analysis_data(void)
{
  assert(anal_data.analysis_filename != NULL);

  assert(anal_data.target_file_hash != NULL);

  assert(anal_data.target_argv != NULL);
  for (int i=0; i<anal_data.target_argc; i++) {
    assert(anal_data.target_argv[i] != NULL);
  }

  assert(anal_data.sites != NULL);
  for (size_t i=0; i<anal_data.site_len; i++) {
    assert(anal_data.sites[i].instruction != NULL);
  }

  return 1;
}


static void
zero_analysis_data_counts(void)
{
  assert(check_analysis_data() == 1);

  for (size_t i=0; i<anal_data.site_len; i++) {
    anal_data.sites[i].count = 0;
  }
}


static void
free_analysis_data(void)
{
  assert(check_analysis_data() == 1);

  free(anal_data.analysis_filename);
  anal_data.analysis_filename = NULL;

  free(anal_data.target_file_hash);
  anal_data.target_file_hash = NULL;

  for (int i=0; i<anal_data.target_argc; i++) {
    free(anal_data.target_argv[i]);
    anal_data.target_argv[i] = NULL;
  }
  free(anal_data.target_argv);
  anal_data.target_argv = NULL;

  for (size_t i=0; i<anal_data.site_len; i++) {
    free(anal_data.sites[i].instruction);
    anal_data.sites[i].instruction = NULL;
  }
  free(anal_data.sites);
  anal_data.sites = NULL;
}


static void
write_analysis_file(void)
{
  assert(check_analysis_data() == 1);

  FILE* fp = fopen(anal_data.analysis_filename, "w");
  if (fp == NULL) {
    error("unable to open analysis file\n");
  }

  int valid;

  valid = fprintf(fp, "%s\n", anal_data.target_file_hash);
  if (valid <= 0) {
    error("unable to write analysis file\n");
  }

  valid = fprintf(fp, "%d\n", anal_data.target_argc);
  if (valid <= 0) {
    error("unable to write analysis file\n");
  }

  for (int i=0; i<anal_data.target_argc; i++) {
    valid = fprintf(fp, "%s\n", anal_data.target_argv[i]);
    if (valid <= 0) {
      error("unable to write analysis file\n");
    }
  }

  valid = fprintf(fp, "%zu\n", anal_data.site_len);
  if (valid <= 0) {
    error("unable to write analysis file\n");
  }

  for (size_t i=0; i<anal_data.site_len; i++) {
    valid = fprintf(fp, "%s\n", anal_data.sites[i].instruction);
    if (valid <= 0) {
      error("unable to write analysis file\n");
    }

    valid = fprintf(fp, "%zu\n", anal_data.sites[i].count);
    if (valid <= 0) {
      error("unable to write analysis file\n");
    }
  }

  fclose(fp);
}


static void
checked_fgets(char* buf, int buflen, FILE* fp)
{
  assert(buf != NULL);
  assert(fp != NULL);

  char* valid = fgets(buf, buflen, fp);
  if (valid == NULL) {
    fclose(fp);
    error("unable to read analysis file\n");
  }
  buf[strcspn(buf, "\n")] = '\0';
}


static void
read_analysis_file(const char* filename)
{
  assert(filename != NULL);

  FILE* fp = fopen(filename, "r");
  if (fp == NULL) {
    error("unable to open analysis file\n");
  }
  anal_data.analysis_filename = new_string(filename);

  const int buflen = 4096;
  char* buf = (char*) safe_malloc(sizeof(char) * buflen); // BAD static buffer size

  checked_fgets(buf, buflen, fp);
  anal_data.target_file_hash = new_string(buf);

  checked_fgets(buf, buflen, fp);
  anal_data.target_argc = (int) get_size_t(buf);

  anal_data.target_argv = (char* *) safe_malloc(sizeof(char*) * anal_data.target_argc);
  for (int i=0; i<anal_data.target_argc; i++) {
    checked_fgets(buf, buflen, fp);
    anal_data.target_argv[i] = new_string(buf);
  }

  checked_fgets(buf, buflen, fp);
  anal_data.site_len = get_size_t(buf);
  anal_data.site_backing_len = anal_data.site_len;

  anal_data.sites = (struct Site*) safe_malloc(sizeof(struct Site) * anal_data.site_len);
  for (size_t i=0; i<anal_data.site_len; i++) {
    checked_fgets(buf, buflen, fp);
    anal_data.sites[i].instruction = new_string(buf);

    checked_fgets(buf, buflen, fp);
    anal_data.sites[i].count = get_size_t(buf);
  }

  assert(buf != NULL);
  free(buf);
  fclose(fp);
  assert(check_analysis_data() == 1);
}


static struct Site*
get_site(const char* instruction)
{
  assert(instruction != NULL);

  for (size_t i=0; i<anal_data.site_len; i++) {
    struct Site* s = & anal_data.sites[i];
    if (strcmp(instruction, s->instruction) == 0) {
      return s;
    }
  }

  return NULL;
}


static void
increment_instruction_count(const char* instruction)
{
  assert(instruction != NULL);

  struct Site* my_site = get_site(instruction);

  if (my_site == NULL) {
    if (anal_data.site_len+1 >= anal_data.site_backing_len) {
      anal_data.site_backing_len *= 2;

      struct Site* new_sites = (struct Site*) realloc(anal_data.sites, sizeof(struct Site) * anal_data.site_backing_len);
      if (new_sites == NULL) {
	resource_error("unable to realloc memory\n");
      }
      anal_data.sites = new_sites;
    }

    anal_data.sites[anal_data.site_len].instruction = new_string(instruction);
    anal_data.sites[anal_data.site_len].count = 0;
    my_site = & anal_data.sites[anal_data.site_len];
    anal_data.site_len += 1;
  }

  my_site->count += 1;
}




/*******************************************************************************/
/* injection data  helper functions                                            */
/*******************************************************************************/
static int
check_injection_data(void)
{
  if (inj_data.injection_log_filename == NULL) { return 0; }

  if (inj_data.errors == NULL) { return 0; }
  for (size_t i=0; i<inj_data.len; i++) {
    if (inj_data.errors[i].instruction == NULL) { return 0; }
  }

  return 1;
}


static void
free_injection_data(void)
{
  assert(check_injection_data() == 1);

  free(inj_data.injection_log_filename);
  inj_data.injection_log_filename = NULL;

  for (size_t i=0; i<inj_data.len; i++) {
    free(inj_data.errors[i].instruction);
    inj_data.errors[i].instruction = NULL;
  }
  free(inj_data.errors);
  inj_data.errors = NULL;
}


static void
generate_injection_data(size_t count, size_t lower_bit, size_t upper_bit)
{
  assert(lower_bit <= upper_bit);

  inj_data.errors = (struct Error*) safe_malloc(sizeof(struct Error) * count);
  inj_data.len = count;

  for (size_t i=0; i<count; i++) {
    struct Error* err = & inj_data.errors[i];

    size_t instruction_index = (size_t) random_choice(0, anal_data.site_len-1);
    struct Site* chosen = & anal_data.sites[instruction_index];

    err->instruction = new_string(chosen->instruction);
    err->invocation = (size_t) random_choice(0, chosen->count-1);
    err->bit = (int) random_choice(lower_bit, upper_bit);

    for (size_t j=0; j<i; j++) {
      struct Error* already_made = & inj_data.errors[j];
      if (err->invocation == already_made->invocation &&
	  strcmp(err->instruction, already_made->instruction) == 0) {
	i -= 1;
	break;
      }
    }
  }
}


static void
write_injection_log(const struct Error* err)
{
  static int first_run = 1;

  assert(err != NULL);

  FILE* fp;
  int valid;
  if (first_run == 1) {
    fp = fopen(inj_data.injection_log_filename, "w");
    if (fp == NULL) {
      error("unable to open injection log file\n");
    }
    valid = fprintf(fp, "Instruction; Invocation; Bit\n");
    if (valid <= 0) {
      fclose(fp);
      error("unable to write analysis file\n");
    }
    first_run = 0;
  } else {
    fp = fopen(inj_data.injection_log_filename, "a");
    if (fp == NULL) {
      error("unable to open injection log file\n");
    }
  }

  assert(err->instruction != NULL);
  valid = fprintf(fp, "%s; %zu; %d\n", err->instruction, err->invocation, err->bit);
  if (valid <= 0) {
    fclose(fp);
    error("unable to write analysis file\n");
  }

  fclose(fp);
}


static int
get_injection(const char* instruction)
{
  assert(instruction != NULL);

  struct Site* my_site = get_site(instruction);
  assert(my_site != NULL);

  int target_bit = -1;
  for (size_t i=0; i<inj_data.len; i++) {
    struct Error* err = &inj_data.errors[i];
    if (my_site->count == err->invocation &&
	strcmp(instruction, err->instruction) == 0) {
      target_bit = err->bit;
      write_injection_log(err);
      break;
    }
  }

  my_site->count += 1;
  return target_bit;
}




/*******************************************************************************/
/* externally visible functions                                                 */
/*******************************************************************************/
char**
vulfi_init(int argc, char* *argv, int* new_argc)
{
  static int first_run = 1;

  assert(argc >= 1);
  assert(argv != NULL);
  assert(argv[0] != NULL);
  assert(new_argc != NULL);

  if (first_run == 0) {
    error("vilfi_init called twice\n");
  }
  first_run = 0;

  target_name = new_string(argv[0]);

  if (argc < 3) {
    error("insufficent arguments given to vulfi\n");
  }

  char* executable_hash = hash_file(argv[0]);
  mode = get_mode(argv[1]);
  char* analysis_filename = argv[2];

  int consumed = 0;
  int executable_argc = 0;
  char** executable_argv = NULL;
  size_t error_count;
  size_t upper_bit;
  size_t lower_bit;
  switch (mode) {
  case UNSET:
    internal_error("an error ehich was previously thought to be unable to occur has occured");

  case ANALYZE:
    consumed = 2;
    executable_argc = argc - consumed;
    executable_argv = copy_argv(argc, argv, executable_argc);

    anal_data.analysis_filename = new_string(analysis_filename);
    anal_data.target_file_hash = executable_hash;
    anal_data.target_argc = executable_argc;
    anal_data.target_argv = executable_argv;
    anal_data.site_len = 0;
    anal_data.site_backing_len = 128;
    anal_data.sites = (struct Site*) safe_malloc(sizeof(struct Site) * anal_data.site_backing_len);
    break;

  case INJECT:
    consumed = 6;
    if (argc < consumed + 1) {
      error("insufficent arguments given to vulfi\n");
    }
    executable_argc = argc - consumed;
    executable_argv = copy_argv(argc, argv, executable_argc);

    read_analysis_file(analysis_filename);

    if (strcmp(anal_data.target_file_hash, executable_hash) != 0) {
      printf("'%s' != '%s'\n\n", anal_data.target_file_hash, executable_hash);
      error("analysis data was made with a different executable\n");
    }

    if (executable_argc != anal_data.target_argc) {
      error("analysis data was made with a different argument count\n");
    }

    for (int i=0; i<executable_argc; i++) {
      if (strcmp(executable_argv[i], anal_data.target_argv[i]) != 0) {
	error("analysis data was made with a different arguments\n");
      }
    }

    error_count = get_size_t(argv[3]);
    if (error_count == 0) {
      fprintf(stderr, "WARNING: requested 0 errors to be injected\n");
    }

    lower_bit = get_size_t(argv[4]);
    upper_bit = get_size_t(argv[5]);
    if (lower_bit > upper_bit) {
      error("lower bit index for fault is higher than upper bit index\n");
    }
    if (upper_bit >= 64) {
      error("upper bit index is more than 63\n");
    }

    generate_injection_data(error_count, lower_bit, upper_bit);
    inj_data.injection_log_filename = new_string(argv[6]);
    zero_analysis_data_counts();
    break;

  case TARGETED:
    consumed = 6;
    if (argc < consumed + 1) {
      error("insufficent arguments given to vulfi\n");
    }
    executable_argc = argc - consumed;
    executable_argv = copy_argv(argc, argv, executable_argc);

    read_analysis_file(analysis_filename);

    if (strcmp(anal_data.target_file_hash, executable_hash) != 0) {
      printf("'%s' != '%s'\n\n", anal_data.target_file_hash, executable_hash);
      error("analysis data was made with a different executable\n");
    }

    if (executable_argc != anal_data.target_argc) {
      error("analysis data was made with a different argument count\n");
    }

    for (int i=0; i<executable_argc; i++) {
      if (strcmp(executable_argv[i], anal_data.target_argv[i]) != 0) {
	error("analysis data was made with a different arguments\n");
      }
    }

    inj_data.errors = (struct Error*) safe_malloc(sizeof(struct Error) * 1);
    inj_data.len = 1;

    struct Error* err = & inj_data.errors[0];

    const size_t instruction_index = get_size_t(argv[3]);
    if (instruction_index >= anal_data.site_len) {
      error("targeted instuction number is too high\n");
    }
    struct Site* target = & anal_data.sites[instruction_index];
    err->instruction = new_string(target->instruction);

    const size_t call_number = get_size_t(argv[4]);
    if (call_number >= target->count) {
      error("targeted invocation is too high\n");
    }
    err->invocation = call_number;

    const size_t bit_position = get_size_t(argv[5]);
    if (bit_position >= 64) {
      error("targeted bit position is more than 63\n");
    }
    err->bit = bit_position;

    inj_data.injection_log_filename = new_string(argv[6]);
    zero_analysis_data_counts();
    break;

  }

  *new_argc = executable_argc;
  swap(argv, 0, consumed);
  return &(argv[consumed]);
}


void
vulfi_finalize(void)
{
  static int first_run = 1;

  if (first_run == 0) {
    error("vulfi_finalize was called twice\n");
  }
  first_run = 0;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    write_analysis_file();
    break;

  case INJECT:
  case TARGETED:
    free_injection_data();
    break;
  }

  free_analysis_data();

  assert(target_name != NULL);
  free(target_name);
  target_name = NULL;
}


int
injectSoftErrorIntTy1(int data, const char* instrName, int __mask)
{
  UNUSED(__mask);
  int injection_bit;
  int flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 0);
    flip_mask = ((int) 1) << injection_bit;
    return data ^ flip_mask;
  }

}


int
injectSoftErrorIntTy8(char data, const char* instrName, char __mask)
{
  UNUSED(__mask);
  int injection_bit;
  char flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 7);
    flip_mask = ((char) 1) << injection_bit;
    return data ^ flip_mask;
  }
}


short
injectSoftErrorIntTy16(short data, const char* instrName, short __mask)
{
  UNUSED(__mask);
  int injection_bit;
  short flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 15);
    flip_mask = ((short) 1) << injection_bit;
    return data ^ flip_mask;
  }
}


int
injectSoftErrorIntTy32(int data, const char* instrName, int __mask)
{
  UNUSED(__mask);
  int injection_bit;
  int flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 31);
    flip_mask = ((int) 1) << injection_bit;
    return data ^ flip_mask;
  }
}


long long
injectSoftErrorIntTy64(long long data, const char* instrName, long long __mask)
{
  UNUSED(__mask);
  int injection_bit;
  long long flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 63);
    flip_mask = ((long long)1) << injection_bit;
    return data ^ flip_mask;
  }
}


float
injectSoftErrorFloatTy(float data, const char* instrName, float __mask)
{
  UNUSED(__mask);
  int injection_bit;
  uint32_t hex;
  uint32_t flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 31);

    memcpy((void*) &hex, (void*) &data, sizeof(float));

    flip_mask = ((uint32_t) 1) << injection_bit;
    hex ^= flip_mask;

    memcpy((void*) &data, (void*) &hex, sizeof(float));
    return data;
  }
}


double
injectSoftErrorDoubleTy(double data, const char* instrName, double __mask)
{
  UNUSED(__mask);
  int injection_bit;
  uint64_t hex;
  uint64_t flip_mask;

  switch (mode) {
  case UNSET:
    internal_error("vulfi_init was not called");

  case ANALYZE:
    increment_instruction_count(instrName);
    return data;

  case INJECT:
  case TARGETED:
    injection_bit = get_injection(instrName);
    if (injection_bit == -1) {
      return data;
    }

    assert(injection_bit >= 0);
    assert(injection_bit <= 63);

    memcpy((void*) &hex, (void*) &data, sizeof(double));

    flip_mask = ((uint64_t) 1) << injection_bit;
    hex ^= flip_mask;

    memcpy((void*) &data, (void*) &hex, sizeof(double));
    return data;
  }
}

int printFaultSitesData(void){ return 0; }
int printFaultInjectionData(void){ return 0; }
