#!/usr/bin/env python3

import argparse
import multiprocessing
import os
import random
import shlex
import shutil
import subprocess
import sys
import tempfile
import time
import traceback

import multiprocessing.pool as pool
import os.path as path

from multiprocessing import Lock




def read_injection_csv(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    if lines[0] != 'Instruction; Invocation; Bit\n':
        msg = "ERROR: vulfi generated file '{}' is not correctly formed"
        print(msg.format(filename))
        print("Contents:")
        print("".join(lines))
        sys.exit(-1)
    injections = list()
    for line in lines[1:]:
        data = line.split(";")
        injections.append({"instruction" : data[0].strip(),
                           "invocation"  : data[1].strip(),
                           "bit"         : data[2].strip()})
    os.remove(filename)
    return injections


def do_run(golden_results, executable, args, injection_filename, work_dir):
    try:
        cmd = shlex.split(executable + " " + " ".join(args))

        p = subprocess.Popen(cmd,
                             stderr=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             cwd=work_dir)
        (out, err) = p.communicate()

        out = out.decode('utf-8').strip()
        err = err.decode('utf-8').strip()
        retcode = p.returncode

        injections = None
        if args[0] == "inject":
            injections = read_injection_csv(injection_filename)

    except Exception as e:
        print("ERROR: Unable to run command")
        print("python exception:")
        print(e)
        print("\ncommand output:")
        print(out)
        print("\ncommand error:")
        print(err)
        print("\ncommand used:")
        print(cmd)
        sys.exit(-1)

    caught = False
    if "Error detected" in err:
        caught = True

    if retcode != 0:
        catagory = "Crash"

    elif golden_results == None:
        catagory = "Analysis"

    elif golden_results == out:
        catagory = "Benign Corruption"

    else:
        catagory = "Silent Corruption"

    return caught, catagory, out, injections


def do_analysis_run(executable, analysis_filename, args, work_dir):
    analysis_args = (["analyze",           # mode
                       analysis_filename]  # analysis filename
                      + [args])            # command arguments
    caught, catagory, out, injections = do_run(None,
                                       executable,
                                       analysis_args,
                                       None,
                                       work_dir)
    assert(catagory == "Analysis")
    assert(injections == None)
    assert(caught == False)
    return out


def do_random_run(golden_results,
                  executable,
                  analysis_filename,
                  injection_filename,
                  args,
                  work_dir):
    injection_args = (["inject",           # mode
                       analysis_filename,  # analysis data
                       "1",                # number of errors
                       "0",                # lower bit
                       "63",               # upper bit
                       injection_filename] # output data
                      + [args])            # command arguments
    return do_run(golden_results,
                  executable,
                  injection_args,
                  injection_filename,
                  work_dir)


def do_targeted_run(golden_results,
                    executable,
                    analysis_filename,
                    args,
                    target_tuple,
                    work_dir):
    injection_args = (["targeted",         # mode
                       analysis_filename,  # analysis data
                       target_tuple[0],    # instruction
                       target_tuple[1],    # invocation
                       target_tuple[2]]    # bit
                      + [args])            # command arguments
    return do_run(golden_results,
                  executable,
                  injection_args,
                  None,
                  work_dir)


def do_serial_runs(runs, executable, args, debug=False):
    log = list()
    start_dir = os.getcwd()
    results = dict()

    try:
        if debug:
            my_dir = start_dir
        else:
            my_dir = tempfile.mkdtemp()

        my_executable = path.join(my_dir, path.basename(executable))
        analysis_filename = path.join(my_dir, "analysis.txt")
        injection_filename = path.join(my_dir, "injection_log.txt")

        shutil.copy(executable, my_executable)

        golden_results = None
        for _ in range(5):
            try:
                golden_results = do_analysis_run(my_executable,
                                                 analysis_filename,
                                                 args,
                                                 my_dir)
                break

            except Exception as e:
                print(e)
                print("Retrying")
                time.sleep(1)
                continue

        if golden_results == None:
            print("Unable to compute golden results")
            sys.exit(-1)

        for i in range(runs):
            caught, catagory, _, injections = do_random_run(golden_results,
                                                         my_executable,
                                                         analysis_filename,
                                                         injection_filename,
                                                         args,
                                                         my_dir)
            injection_tuple = (injections[0]["instruction"],
                               injections[0]["invocation"],
                               injections[0]["bit"])

            log.append("\t".join([injection_tuple[0],
                                  injection_tuple[1],
                                  injection_tuple[2],
                                  str(caught),
                                  catagory]) + "\n")

            key = (caught, catagory)
            if key in results:
                results[key] += 1
            else:
                results[key] = 1

    except KeyboardInterrupt:
        print("\nCaught KeyboardInterrupt, bye")
        sys.exit(-1)

    except Exception as e:
        print("\nUnknown error occured:\n{}".format(e))

    finally:
        os.chdir(start_dir)
        if debug:
            os.remove(my_executable)
        else:
            shutil.rmtree(my_dir)

    return results, log


RESULT_LOCK = Lock()
RESULTS = dict()
log_label = "\t".join(["instruction",
                       "invocation",
                       "bit",
                       "original_type",
                       "transformed_type"]) + "\n"
LOG = [log_label]

def combine_serial_runs(tup):
    RESULT_LOCK.acquire()
    results, log = tup
    for key in results:
        if key in RESULTS:
            RESULTS[k] += results[k]
        else:
            RESULTS[k] = results[k]

    LOG.extend(log)
    RESULT_LOCK.release()

def process_error(e):
    print("Exception returned: {}".format(e))

def do_parallel_runs(runs, executable, args, pool, procs):
    per = runs // procs
    rest = runs - (per*procs)
    assert(runs == (rest + (procs)*per))

    results = list()
    first = True
    for p in range(procs):
        if rest == 0:
            r = pool.apply_async(do_serial_runs,
                                 args=(per,
                                       executable,
                                       args,
                                       False),
                                 callback=combine_serial_runs,
                                 error_callback=process_error)
        else:
            r = pool.apply_async(do_serial_runs,
                                 args=(per+1,
                                       executable,
                                       args,
                                       False),
                                 callback=combine_serial_runs,
                                 error_callback=process_error)
            rest -= 1

        results.append(r)

    for r in results:
        r.wait()

    return RESULTS, LOG





def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("executable",
                        help="the target executable processed with Vulfi "
                        "Note: the executable must print only the final state "
                        "of the execution to allow for SDC and benign cases "
                        "to be properly classified. Also the runs must be "
                        "deterministic. If randomness is used a seed must be "
                        "set eigther in the source code or via a command line "
                        "argument")
    parser.add_argument("runs", nargs="?", type=int, default=10,
                        help="the number of times to run the executable")
    parser.add_argument("--args", nargs="+", default="",
                        help="arguments to pass onto the execution")
    parser.add_argument("--procs", type=str, default="1",
                        help="the number of processes used to concurrently run")
    parser.add_argument("--debug", action="store_true",
                        help="performs a single run in the current directory")
    parser.add_argument("--log", type=str,
                        help="outputs individual run data to a logfile")
    args = parser.parse_args()

    if not path.exists(args.executable):
        print("ERROR: executable file not found '{}'".format(args.executable))
        sys.exit(-1)
    args.executable = path.abspath(args.executable)

    if args.runs <= 0:
        print("ERROR: number of runs must be positive '{}'".format(args.runs))
        sys.exit(-1)

    try:
        procs = int(args.procs)
        if procs <= 0:
            msg = "ERROR: number of procs must be positive '{}'"
            print(msg.format(args.procs))
            sys.exit(-1)
        args.procs = procs
    except ValueError:
        if args.procs == "":
            args.procs = 1
        else:
            msg = "ERROR: number of procs must be an integer '{}'"
            print(msg.format(args.procs))
            sys.exit(-1)

    return args


def main():
    args = get_args()

    if args.debug:
        print("Debug run.")
        print()
        results, log = do_serial_runs(1,
                                      args.executable,
                                      args.args,
                                      debug=True)
    elif args.procs == 1:
        print("Runs will be processed serially")
        print()
        results, log = do_serial_runs(args.runs,
                                      args.executable,
                                      args.args)
    else:
        procs = min(args.runs+1, args.procs)
        print("Creating a pool with '{}' workers".format(procs))
        print()
        with multiprocessing.pool.ThreadPool(processes=procs) as pool:
            results, log = do_parallel_runs(args.runs,
                                            args.executable,
                                            args.args,
                                            pool,
                                            procs)

    print()
    order = ["Silent Corruption",
             "Benign Corruption",
             "Crash"]

    total = 0
    for t in order:
        for c in (True, False):
            label = "Caught" if c else "Uncaught"
            num = results.get((c, t), 0)
            print("{} {}: {}".format(label, t, num))
            total += num

    print("Total: {}".format(total))

    if total != args.runs:
        print("ERROR: Requested run count not met")
        sys.exit(-1)

    if args.log:
        with open(args.log, "w") as f:
            f.write("".join(log))

if __name__ == "__main__":
    main()
