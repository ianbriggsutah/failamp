#!/usr/bin/env python3

import argparse

from os import listdir
from os.path import isfile, join




def read_file(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    return [l.strip() for l in lines]


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("directory")
    args = parser.parse_args()
    return args


def main():
    args = get_args()
    filenames = [f for f in listdir(args.directory)
                 if isfile(join(args.directory, f))]
    filenames.sort()
    all_lines = [read_file(join(args.directory, f)) for f in filenames]
    iters = max([len(l) for l in all_lines])
    cols = max([max([l.count("\t") for l in lines]) for lines in all_lines])
    for i in range(iters):
        for l in all_lines:
            if len(l) < i:
                print("\t"*cols, end="")
            else:
                this_cols = l[i].count("\t")
                rest = cols-this_cols
                print(l[i]+"\t"*rest, end="")
            print("\t", end="")
        print()


if __name__ == "__main__":
    main()
