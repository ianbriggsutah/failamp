#!/usr/bin/env python3

import argparse
import os
import shlex
import shutil
import resource
import subprocess
import sys

from os import path
from math import sqrt




def get_free():
    try:
        p = subprocess.Popen(shlex.split("free -k"),
                             stderr=subprocess.PIPE,
                             stdout=subprocess.PIPE)
        (out, err) = p.communicate()

        out = out.decode('utf-8').strip()
        err = err.decode('utf-8').strip()
        retcode = p.returncode
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print("ERROR: Unable to run command")
        print("python exception:")
        print(e)
        print(exc_type, fname, exc_tb.tb_lineno)
        print("\ncommand output:")
        print(out)
        print("\ncommand error:")
        print(err)
        print("\ncommand used:")
        print(cmd)
        sys.exit(-1)

    lines = out.splitlines()
    mem_line = lines[1].split()
    free = mem_line[3]

    return int(free)


def limit_memory(mem):
    resource.setrlimit(resource.RLIMIT_AS, (1024*mem, resource.RLIM_INFINITY))


def do_run(executable, args):
    global NAME

    try:
        free = get_free()

        cmd = shlex.split(executable + " " + " ".join([str(a) for a in args]))

        p = subprocess.Popen(cmd,
                             stderr=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             preexec_fn=lambda : limit_memory(int(free*0.8)))
        (out, err) = p.communicate()

        out = out.decode('utf-8').strip()
        err = err.decode('utf-8').strip()
        retcode = p.returncode

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print("ERROR: Unable to run command")
        print("python exception:")
        print(e)
        print(exc_type, fname, exc_tb.tb_lineno)
        print("\ncommand output:")
        print(out)
        print("\ncommand error:")
        print(err)
        print("\ncommand used:")
        print(cmd)
        sys.exit(-1)

    if retcode != 0:
        print("*"*80)
        print("Command: {}".format(executable + " " + " ".join([str(a) for a in args])))
        print("Benchmark: {}".format(NAME))
        print("Retcode: {}".format(retcode))
        print("Stdout " + "-"*(80 - 7))
        print(out)
        print("Stderr " + "-"*(80 - 7))
        print(err)
        print("*"*80)
        sys.exit(0)

    lines = out.splitlines()
    info = dict()
    for line in lines:
        pair = line.split(":")
        assert(len(pair) == 2)
        clean_pair = [p.strip() for p in pair]
        if clean_pair[0] in {"memory_footprint", "init_time", "kernel_time"}:
            clean_pair[1] = float(clean_pair[1])
        info[clean_pair[0]] = clean_pair[1]

    if NAME == None:
        NAME = info["Benchmark"] + "-" + info["Layout"]

    return info


def find_base_size(executable, static_args, dynamic_args, target_time):
    info = do_run(executable, static_args + dynamic_args)
    time = info["kernel_time"]
    #print("debug: ({}) {}".format(time, " ".join([str(a) for a in static_args + dynamic_args])))
    state = "under" if time < target_time else "over"
    last_state = state
    tolerance = target_time * 0.05
    step = [int("1" + str(d)[1:]) for d in dynamic_args]

    while time < target_time - tolerance or time > target_time + tolerance:
        if state == "under" and (last_state == "under" or last_state == None):
            dynamic_args = [d+s for d,s in zip(dynamic_args, step)]
        if state == "over" and (last_state == "over" or last_state == None):
            dynamic_args = [d-s for d,s in zip(dynamic_args, step)]
        if state == "under" and last_state == "over":
            state = None
            step = [s//10 for s in step]
            dynamic_args = [d+s*5 for d,s in zip(dynamic_args, step)]
        if state == "over" and last_state == "under":
            state = None
            step = [s//10 for s in step]
            dynamic_args = [d-s*5 for d,s in zip(dynamic_args, step)]


        info = do_run(executable, static_args + dynamic_args)
        time = info["kernel_time"]
        #print("debug: ({}) {}".format(time, " ".join([str(a) for a in static_args + dynamic_args])))
        last_state = state
        state = "under" if time < target_time else "over"

    #print("found")
    return dynamic_args, time


def average_runs(executable, static_args, dynamic_args, repeats, time=None):
    times = list()
    if time is not None:
        times.append(time)

    while len(times) < repeats:
        info = do_run(executable, static_args + dynamic_args)
        #print("{}".format(info["kernel_time"]))
        times.append(info["kernel_time"])

    avg = sum(times) / len(times)
    stddev = sqrt(sum([(t-avg)**2 for t in times]) / len(times))
    return avg, stddev


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("executable",
                        help="the target executable to be timed")
    parser.add_argument("failamp_executable",
                        help="the target executable to be timed")

    parser.add_argument("runs", nargs="?", type=int, default=10,
                        help="the number of runs to average")
    parser.add_argument("--static-args", nargs="+", default=[""])
    parser.add_argument("--dynamic-args", nargs="+", default=[""])
    parser.add_argument("--target-time", type=int, default=30)

    args = parser.parse_args()

    if not path.exists(args.executable):
        print("ERROR: executable file not found '{}'".format(args.executable))
        sys.exit(-1)
    args.executable = path.abspath(args.executable)

    if not path.exists(args.failamp_executable):
        print("ERROR: failamp_executable file not found '{}'".format(args.failamp_executable))
        sys.exit(-1)
    args.failamp_executable = path.abspath(args.failamp_executable)

    if args.runs <= 0:
        print("ERROR: number of runs must be positive '{}'".format(args.runs))
        sys.exit(-1)

    return args

NAME = None
def main():
    args = parse_args()

    executable = args.executable
    static_args = [int(p) for p in args.static_args if p != ""]
    dynamic_args = [int(p) for p in args.dynamic_args if p != ""]
    target_time = args.target_time
    repeats = args.runs

    #print("normal")
    dynamic_args, time = find_base_size(executable,
                                        static_args,
                                        dynamic_args,
                                        target_time)
    #print("final: {}\n".format(" ".join([str(a) for a in dynamic_args])))
    avg_time, stddev_time = average_runs(executable,
                                         static_args,
                                         dynamic_args,
                                         repeats,
                                         time)

    #print("failamp")
    failamp_executable = args.failamp_executable
    failamp_avg_time, failamp_stddev_time = average_runs(failamp_executable,
                                                         static_args,
                                                         dynamic_args,
                                                         repeats)


    print("{}\t{}\t{}\t{}\t{}\t{}\t{}".format(NAME,
                                              avg_time, stddev_time,
                                              failamp_avg_time, failamp_stddev_time,
                                              (failamp_avg_time/avg_time - 1.0) * 100,
                                              " ".join([str(a) for a in dynamic_args])))


if __name__ == "__main__":
    main()
