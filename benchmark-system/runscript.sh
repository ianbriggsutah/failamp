#!/bin/bash

set -e

# Base
SB=./benchmarks/polybench_single
MB=./benchmarks/polybench_multi


# Overhead script
O=./support/scripts/overhead.py

# Repeats
R=10

# tsteps args
T="--static-args 32"

# Target time
I="--target-time 60"




echo -e "Benchmark\tNormal Time\tStddev\tFailamp Time\tStddev\tOverhead\tArguments"


# Single

# One arg
${O} ${SB}/cholesky/bin/normal_main_cholesky_single ${SB}/cholesky/bin/failamp_main_cholesky_single ${R} ${I} \
     --dynamic-args 4000

${O} ${SB}/durbin/bin/normal_main_durbin_single ${SB}/durbin/bin/failamp_main_durbin_single ${R} ${I} \
     --dynamic-args 100000

${O} ${SB}/floyd_warshall/bin/normal_main_floyd_warshall_single ${SB}/floyd_warshall/bin/failamp_main_floyd_warshall_single ${R} ${I} \
     --dynamic-args 2000

${O} ${SB}/gemver/bin/normal_main_gemver_single ${SB}/gemver/bin/failamp_main_gemver_single ${R} ${I} \
     --dynamic-args 40000

${O} ${SB}/gesummv/bin/normal_main_gesummv_single ${SB}/gesummv/bin/failamp_main_gesummv_single ${R} ${I} \
      --dynamic-args 10000

${O} ${SB}/lu/bin/normal_main_lu_single ${SB}/lu/bin/failamp_main_lu_single ${R} ${I} \
     --dynamic-args 1000

${O} ${SB}/ludcmp/bin/normal_main_ludcmp_single ${SB}/ludcmp/bin/failamp_main_ludcmp_single ${R} ${I} \
     --dynamic-args 1000

${O} ${SB}/mvt/bin/normal_main_mvt_single ${SB}/mvt/bin/failamp_main_mvt_single ${R} ${I} \
     --dynamic-args 10000

${O} ${SB}/nussinov/bin/normal_main_nussinov_single ${SB}/nussinov/bin/failamp_main_nussinov_single ${R} ${I} \
     --dynamic-args 1000

${O} ${SB}/trisolv/bin/normal_main_trisolv_single ${SB}/trisolv/bin/failamp_main_trisolv_single ${R} ${I} \
      --dynamic-args 10000




# Two arg tsteps
${O} ${SB}/adi/bin/normal_main_adi_single ${SB}/adi/bin/failamp_main_adi_single ${R} ${T} ${I} \
     --dynamic-args 1000

${O} ${SB}/heat_3d/bin/normal_main_heat_3d_single ${SB}/heat_3d/bin/failamp_main_heat_3d_single ${R} ${T} ${I} \
     --dynamic-args 100

${O} ${SB}/jacobi_1d/bin/normal_main_jacobi_1d_single ${SB}/jacobi_1d/bin/failamp_main_jacobi_1d_single ${R} ${T} ${I} \
     --dynamic-args 100000000

${O} ${SB}/jacobi_2d/bin/normal_main_jacobi_2d_single ${SB}/jacobi_2d/bin/failamp_main_jacobi_2d_single ${R} ${T} ${I} \
     --dynamic-args 10000

${O} ${SB}/seidel_2d/bin/normal_main_seidel_2d_single ${SB}/seidel_2d/bin/failamp_main_seidel_2d_single ${R} ${T} ${I} \
     --dynamic-args 1000




# Two arg
${O} ${SB}/atax/bin/normal_main_atax_single ${SB}/atax/bin/failamp_main_atax_single ${R} ${I} \
      --dynamic-args 10000 10000

${O} ${SB}/bicg/bin/normal_main_bicg_single ${SB}/bicg/bin/failamp_main_bicg_single ${R} ${I} \
      --dynamic-args 10000 10000

${O} ${SB}/correlation/bin/normal_main_correlation_single ${SB}/correlation/bin/failamp_main_correlation_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/covariance/bin/normal_main_covariance_single ${SB}/covariance/bin/failamp_main_covariance_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/deriche/bin/normal_main_deriche_single ${SB}/deriche/bin/failamp_main_deriche_single ${R} ${I} \
     --dynamic-args 10000 10000

${O} ${SB}/gramschmidt/bin/normal_main_gramschmidt_single ${SB}/gramschmidt/bin/failamp_main_gramschmidt_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/symm/bin/normal_main_symm_single ${SB}/symm/bin/failamp_main_symm_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/syr2k/bin/normal_main_syr2k_single ${SB}/syr2k/bin/failamp_main_syr2k_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/syrk/bin/normal_main_syrk_single ${SB}/syrk/bin/failamp_main_syrk_single ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${SB}/trmm/bin/normal_main_trmm_single ${SB}/trmm/bin/failamp_main_trmm_single ${R} ${I} \
     --dynamic-args 1000 1000




# Three arg tsteps
${O} ${SB}/fdtd_2d/bin/normal_main_fdtd_2d_single ${SB}/fdtd_2d/bin/failamp_main_fdtd_2d_single ${R} ${T} ${I} \
     --dynamic-args 10000 10000




# Three arg
${O} ${SB}/doitgen/bin/normal_main_doitgen_single ${SB}/doitgen/bin/failamp_main_doitgen_single ${R} ${I} \
     --dynamic-args 100 100 100

${O} ${SB}/gemm/bin/normal_main_gemm_single ${SB}/gemm/bin/failamp_main_gemm_single ${R} ${I} \
     --dynamic-args 1000 1000 1000




# Four arg
${O} ${SB}/2mm/bin/normal_main_2mm_single ${SB}/2mm/bin/failamp_main_2mm_single ${R} ${I} \
     --dynamic-args 1000 1000 1000 1000




# Five arg
${O} ${SB}/3mm/bin/normal_main_3mm_single ${SB}/3mm/bin/failamp_main_3mm_single ${R} ${I} \
     --dynamic-args 1000 1000 1000 1000 1000




# Multi

# One arg
${O} ${MB}/cholesky/bin/normal_main_cholesky_multi ${MB}/cholesky/bin/failamp_main_cholesky_multi ${R} ${I} \
     --dynamic-args 1000

${O} ${MB}/durbin/bin/normal_main_durbin_multi ${MB}/durbin/bin/failamp_main_durbin_multi ${R} ${I} \
     --dynamic-args 100000

${O} ${MB}/floyd_warshall/bin/normal_main_floyd_warshall_multi ${MB}/floyd_warshall/bin/failamp_main_floyd_warshall_multi ${R} ${I} \
     --dynamic-args 1000

${O} ${MB}/gemver/bin/normal_main_gemver_multi ${MB}/gemver/bin/failamp_main_gemver_multi ${R} ${I} \
     --dynamic-args 10000

${O} ${MB}/gesummv/bin/normal_main_gesummv_multi ${MB}/gesummv/bin/failamp_main_gesummv_multi ${R} ${I} \
      --dynamic-args 10000

${O} ${MB}/lu/bin/normal_main_lu_multi ${MB}/lu/bin/failamp_main_lu_multi ${R} ${I} \
     --dynamic-args 1000

${O} ${MB}/ludcmp/bin/normal_main_ludcmp_multi ${MB}/ludcmp/bin/failamp_main_ludcmp_multi ${R} ${I} \
     --dynamic-args 1000

${O} ${MB}/mvt/bin/normal_main_mvt_multi ${MB}/mvt/bin/failamp_main_mvt_multi ${R} ${I} \
     --dynamic-args 10000

${O} ${MB}/nussinov/bin/normal_main_nussinov_multi ${MB}/nussinov/bin/failamp_main_nussinov_multi ${R} ${I} \
     --dynamic-args 1000

${O} ${MB}/trisolv/bin/normal_main_trisolv_multi ${MB}/trisolv/bin/failamp_main_trisolv_multi ${R} ${I} \
      --dynamic-args 1000




# Two arg tsteps
${O} ${MB}/adi/bin/normal_main_adi_multi ${MB}/adi/bin/failamp_main_adi_multi ${R} ${T} ${I} \
     --dynamic-args 1000

${O} ${MB}/heat_3d/bin/normal_main_heat_3d_multi ${MB}/heat_3d/bin/failamp_main_heat_3d_multi ${R} ${T} ${I} \
     --dynamic-args 100

${O} ${MB}/jacobi_1d/bin/normal_main_jacobi_1d_multi ${MB}/jacobi_1d/bin/failamp_main_jacobi_1d_multi ${R} ${T} ${I} \
     --dynamic-args 100000000

${O} ${MB}/jacobi_2d/bin/normal_main_jacobi_2d_multi ${MB}/jacobi_2d/bin/failamp_main_jacobi_2d_multi ${R} ${T} ${I} \
     --dynamic-args 10000

${O} ${MB}/seidel_2d/bin/normal_main_seidel_2d_multi ${MB}/seidel_2d/bin/failamp_main_seidel_2d_multi ${R} ${T} ${I} \
     --dynamic-args 1000




# Two arg
${O} ${MB}/atax/bin/normal_main_atax_multi ${MB}/atax/bin/failamp_main_atax_multi ${R} ${I} \
      --dynamic-args 10000 10000

${O} ${MB}/bicg/bin/normal_main_bicg_multi ${MB}/bicg/bin/failamp_main_bicg_multi ${R} ${I} \
      --dynamic-args 1000 1000

${O} ${MB}/correlation/bin/normal_main_correlation_multi ${MB}/correlation/bin/failamp_main_correlation_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/covariance/bin/normal_main_covariance_multi ${MB}/covariance/bin/failamp_main_covariance_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/deriche/bin/normal_main_deriche_multi ${MB}/deriche/bin/failamp_main_deriche_multi ${R} ${I} \
     --dynamic-args 10000 10000

${O} ${MB}/gramschmidt/bin/normal_main_gramschmidt_multi ${MB}/gramschmidt/bin/failamp_main_gramschmidt_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/symm/bin/normal_main_symm_multi ${MB}/symm/bin/failamp_main_symm_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/syr2k/bin/normal_main_syr2k_multi ${MB}/syr2k/bin/failamp_main_syr2k_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/syrk/bin/normal_main_syrk_multi ${MB}/syrk/bin/failamp_main_syrk_multi ${R} ${I} \
     --dynamic-args 1000 1000

${O} ${MB}/trmm/bin/normal_main_trmm_multi ${MB}/trmm/bin/failamp_main_trmm_multi ${R} ${I} \
     --dynamic-args 1000 1000




# Three arg tsteps
${O} ${MB}/fdtd_2d/bin/normal_main_fdtd_2d_multi ${MB}/fdtd_2d/bin/failamp_main_fdtd_2d_multi ${R} ${T} ${I} \
     --dynamic-args 100000 100000




# Three arg
${O} ${MB}/doitgen/bin/normal_main_doitgen_multi ${MB}/doitgen/bin/failamp_main_doitgen_multi ${R} ${I} \
     --dynamic-args 100 100 100

${O} ${MB}/gemm/bin/normal_main_gemm_multi ${MB}/gemm/bin/failamp_main_gemm_multi ${R} ${I} \
     --dynamic-args 1000 1000 1000




# Four arg
${O} ${MB}/2mm/bin/normal_main_2mm_multi ${MB}/2mm/bin/failamp_main_2mm_multi ${R} ${I} \
     --dynamic-args 1000 1000 1000 1000




# Five arg
${O} ${MB}/3mm/bin/normal_main_3mm_multi ${MB}/3mm/bin/failamp_main_3mm_multi ${R} ${I} \
     --dynamic-args 1000 1000 1000 1000 1000
