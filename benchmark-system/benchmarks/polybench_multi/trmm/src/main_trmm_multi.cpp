

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_trmm_multi(long m, long n,
		  DATA_TYPE alpha,
		  DATA_TYPE** __restrict__ A,  // m by m
		  DATA_TYPE** __restrict__ B); // m by n




void
data_init_trmm_multi(long m, long n,
		     DATA_TYPE* __restrict__ alpha,
		     DATA_TYPE** __restrict__ A, // m by m
		     DATA_TYPE** __restrict__ B, // m by n
		     int seed)
{
  *alpha = ((DATA_TYPE) m);
  init_multi_2d(m, m, A, seed);
  init_multi_2d(m, n, B, seed+1);
}


void
run_trmm_multi(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE** C = malloc_multi_2d(m, m);
  DATA_TYPE** A = malloc_multi_2d(m, n);

  size_t bytes = sizeof(DATA_TYPE) * (m*m + m*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\ttrmm\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_trmm_multi(m, n, &alpha, C, A, n+m);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_trmm_multi(m, n, alpha, C, A);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("C\n");
  // print_multi_2d(m, m, C);
  // printf("A\n");
  // print_multi_2d(m, n, A);

  free_multi_2d(m, m, C);
  free_multi_2d(m, n, A);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_trmm_multi(m, n);

  finalize();

  return 0;
}
