

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_gemm_multi(long ni, long nj, long nk,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE** __restrict__ C,  // ni by nj
		  DATA_TYPE** __restrict__ A,  // ni by nk
		  DATA_TYPE** __restrict__ B); // nk by nj


void
data_init_gemm_multi(long ni, long nj, long nk,
		     DATA_TYPE* __restrict__ alpha,
		     DATA_TYPE* __restrict__ beta,
		     DATA_TYPE** __restrict__ C, // ni by nj
		     DATA_TYPE** __restrict__ A, // ni by nk
		     DATA_TYPE** __restrict__ B, // nk by nj
		     int seed)
{
  *alpha = (DATA_TYPE) ni;
  *beta = (DATA_TYPE) nj;
  init_multi_2d(ni, nj, C, seed);
  init_multi_2d(ni, nk, A, seed+1);
  init_multi_2d(nk, nj, B, seed+2);
}


void
run_gemm_multi(long ni, long nj, long nk)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE** C = malloc_multi_2d(ni, nj);
  DATA_TYPE** A = malloc_multi_2d(ni, nk);
  DATA_TYPE** B = malloc_multi_2d(nk, nj);

  size_t bytes = sizeof(DATA_TYPE) * (ni*nj + ni*nk + nk*nj);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tgemm\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\t%ld\n", ni, nj, nk);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_gemm_multi(ni, nj, nk, &alpha, &beta, C, A, B, ni+nj+nk);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_gemm_multi(ni, nj, nk, alpha, beta, C, A, B);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("C\n");
  // print_multi_2d(ni, nj, C);
  // printf("A\n");
  // print_multi_2d(ni, nk, A);
  // printf("B\n");
  // print_multi_2d(nk, nj, B);


  free_multi_2d(ni, nj, C);
  free_multi_2d(ni, nk, A);
  free_multi_2d(nk, nj, B);
}


int
main(int vargc, char** vargv)
{
  long ni, nj, nk;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 4) ||
      (parse_long(argv[1], 2, LONG_MAX, &ni) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &nj) != 0) ||
      (parse_long(argv[3], 2, LONG_MAX, &nk) != 0)) {
    printf("Usage:\n");
    printf("%s [ni nj nk]\n", argv[0]);
    printf("  2 <= ni <= LONG_MAX\n");
    printf("  2 <= nj <= LONG_MAX\n");
    printf("  2 <= nk <= LONG_MAX\n");
    return 1;
  }

  run_gemm_multi(ni, nj, nk);

  finalize();

  return 0;
}
