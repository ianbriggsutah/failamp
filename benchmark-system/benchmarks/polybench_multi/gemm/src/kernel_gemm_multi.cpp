

#include "utilities.hpp"




void
kernel_gemm_multi(long ni, long nj, long nk,
		   DATA_TYPE alpha,
		   DATA_TYPE beta,
		   DATA_TYPE** __restrict__ C, // ni by nj
		   DATA_TYPE** __restrict__ A, // ni by nk
		   DATA_TYPE** __restrict__ B) // nk by nj
{
  long i, j, k;

  //BLAS PARAMS
  //TRANSA = 'N'
  //TRANSB = 'N'
  // => Form C := alpha*A*B + beta*C,
  //A is NIxNK
  //B is NKxNJ
  //C is NIxNJ
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      C[i][j] *= beta;
    }
    for (k=0; k<nk; k++) {
      for (j=0; j<nj; j++) {
	C[i][j] += alpha * A[i][k] * B[k][j];
      }
    }
  }
}
