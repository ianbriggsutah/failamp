

#include "utilities.hpp"




void
kernel_3mm_multi(long ni, long nj, long nk, long nl, long nm,
		 DATA_TYPE** __restrict__ E, // ni by nj
		 DATA_TYPE** __restrict__ A, // ni by nk
		 DATA_TYPE** __restrict__ B, // nk by nj
		 DATA_TYPE** __restrict__ F, // nj by nl
		 DATA_TYPE** __restrict__ C, // nj by nm
		 DATA_TYPE** __restrict__ D, // nm by nl
		 DATA_TYPE** __restrict__ G) // ni by nl
{
  long i, j, k;

  /* E := A*B */
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      E[i][j] = (DATA_TYPE) 0.0;
      for (k=0; k<nk; ++k) {
	E[i][j] += A[i][k] * B[k][j];
      }
    }
  }
  /* F := C*D */
  for (i=0; i<nj; i++) {
    for (j=0; j<nl; j++) {
      F[i][j] = (DATA_TYPE) 0.0;
      for (k=0; k<nm; ++k) {
	F[i][j] += C[i][k] * D[k][j];
      }
    }
  }
  /* G := E*F */
  for (i=0; i<ni; i++) {
    for (j=0; j<nl; j++) {
      G[i][j] = (DATA_TYPE) 0.0;
      for (k=0; k<nj; ++k) {
	G[i][j] += E[i][k] * F[k][j];
      }
    }
  }
}
