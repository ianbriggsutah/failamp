

#include "utilities.hpp"




void
kernel_doitgen_multi(long nr, long nq, long np,
		     DATA_TYPE*** __restrict__ A,   // nr by nq by np
		     DATA_TYPE** __restrict__ C4,  // np by np
		     DATA_TYPE* __restrict__ sum) // np
{
  long r, q, p, s;

  for (r=0; r<nr; r++) {
    for (q=0; q<nq; q++) {
      for (p=0; p<np; p++) {
	sum[p] = (DATA_TYPE) 0.0;
	for (s=0; s<np; s++) {
	  sum[p] += A[r][q][s] * C4[s][p];
	}
      }
      for (p=0; p<np; p++) {
	A[r][q][p] = sum[p];
      }
    }
  }
}
