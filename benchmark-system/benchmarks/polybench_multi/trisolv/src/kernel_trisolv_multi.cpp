

#include "utilities.hpp"




void
kernel_trisolv_multi(long n,
		     DATA_TYPE** __restrict__ L, // n by n
		     DATA_TYPE* __restrict__ x, // n
		     DATA_TYPE* __restrict__ b) // n
{
  long i, j;

  for (i=0; i<n; i++) {
    x[i] = b[i];
    for (j=0; j <i; j++) {
      x[i] -= L[i][j] * x[j];
    }
    x[i] = x[i] / L[i][i];
  }
}
