

#include "utilities.hpp"




void
kernel_mvt_multi(long n,
		 DATA_TYPE* __restrict__ x1,  // n
		 DATA_TYPE* __restrict__ x2,  // n
		 DATA_TYPE* __restrict__ y_1, // n
		 DATA_TYPE* __restrict__ y_2, // n
		 DATA_TYPE** __restrict__ A)   // n by n
{
  long i, j;

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      x1[i] = x1[i] + A[i][j] * y_1[j];
    }
  }
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      x2[i] = x2[i] + A[j][i] * y_2[j];
    }
  }
}
