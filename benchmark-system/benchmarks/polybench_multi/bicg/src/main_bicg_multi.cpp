

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_bicg_multi(long m, long n,
		  DATA_TYPE** __restrict__ A,  // n by m
		  DATA_TYPE* __restrict__ s,  // m
		  DATA_TYPE* __restrict__ q,  // n
		  DATA_TYPE* __restrict__ p,  // m
		  DATA_TYPE* __restrict__ r); // n




void
data_init_bicg_multi(long m, long n,
		     DATA_TYPE** __restrict__ A, // n by m
		     DATA_TYPE* __restrict__ r, // n
		     DATA_TYPE* __restrict__ p, // m
		     int seed)
{
  init_multi_2d(n, m, A, seed);
  init_multi_1d(n, r, seed+1);
  init_multi_1d(m, p, seed+2);
}


void
run_bicg_multi(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE** A = malloc_multi_2d(n, m);
  DATA_TYPE* s = malloc_multi_1d(m);
  DATA_TYPE* q = malloc_multi_1d(n);
  DATA_TYPE* p = malloc_multi_1d(m);
  DATA_TYPE* r = malloc_multi_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (n*m + 2*n + 2*m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tbicg\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_bicg_multi(m, n, A, r, p, m+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_bicg_multi(m, n, A, s, q, p, r);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_multi_2d(n, m, A);
  // printf("s\n");
  // print_multi_1d(m, s);
  // printf("q\n");
  // print_multi_1d(n, q);
  // printf("p\n");
  // print_multi_1d(m, p);
  // printf("r\n");
  // print_multi_1d(n, r);

  free_multi_2d(n, m, A);
  free_multi_1d(s);
  free_multi_1d(q);
  free_multi_1d(p);
  free_multi_1d(r);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_bicg_multi(m, n);

  finalize();

  return 0;
}
