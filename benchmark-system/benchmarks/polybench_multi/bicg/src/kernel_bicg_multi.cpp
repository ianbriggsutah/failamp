

#include "utilities.hpp"




void
kernel_bicg_multi(long m, long n,
		  DATA_TYPE** __restrict__ A, // n by m
		  DATA_TYPE* __restrict__ s, // m
		  DATA_TYPE* __restrict__ q, // n
		  DATA_TYPE* __restrict__ p, // m
		  DATA_TYPE* __restrict__ r) // n
{
  long i, j;

  for (i=0; i<m; i++) {
    s[i] = 0;
  }
  for (i=0; i<n; i++) {
    q[i] = (DATA_TYPE) 0.0;
    for (j=0; j<m; j++) {
      s[j] = s[j] + r[i] * A[i][j];
      q[i] = q[i] + A[i][j] * p[j];
    }
  }
}
