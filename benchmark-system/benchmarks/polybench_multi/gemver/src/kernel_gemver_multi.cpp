

#include "utilities.hpp"




void
kernel_gemver_multi(long n,
		     DATA_TYPE alpha,
		     DATA_TYPE beta,
		     DATA_TYPE** __restrict__ A,  // n by n
		     DATA_TYPE* __restrict__ u1, // n
		     DATA_TYPE* __restrict__ v1, // n
		     DATA_TYPE* __restrict__ u2, // n
		     DATA_TYPE* __restrict__ v2, // n
		     DATA_TYPE* __restrict__ w,  // n
		     DATA_TYPE* __restrict__ x,  // n
		     DATA_TYPE* __restrict__ y,  // n
		     DATA_TYPE* __restrict__ z)  // n
{
  long i, j;

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      A[i][j] = A[i][j] + u1[i] * v1[j] + u2[i] * v2[j];
    }
  }

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      x[i] = x[i] + beta * A[j][i] * y[j];
    }
  }

  for (i=0; i<n; i++) {
    x[i] = x[i] + z[i];
  }

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      w[i] = w[i] +  alpha * A[i][j] * x[j];
    }
  }
}
