

#include "utilities.hpp"




void
kernel_lu_multi(long n,
		DATA_TYPE** __restrict__ A) // n by n
{
  long i, j, k;

  for (i=0; i<n; i++) {
    for (j=0; j <i; j++) {
      for (k=0; k<j; k++) {
	A[i][j] -= A[i][k] * A[k][j];
      }
      A[i][j] /= A[j][j];
    }
    for (j=i; j<n; j++) {
      for (k=0; k<i; k++) {
	A[i][j] -= A[i][k] * A[k][j];
      }
    }
  }
}
