

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_floyd_warshall_multi(long n,
			    DATA_TYPE** __restrict__ path); // n by n




void
data_init_floyd_warshall_multi(long n,
			       DATA_TYPE** __restrict__ path, // n by n
			       int seed)
{
  init_multi_2d(n, n, path, seed);
}


void
run_floyd_warshall_multi(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE** path = malloc_multi_2d(n, n);

  size_t bytes = sizeof(DATA_TYPE) * (n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tfloyd_warshall\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_floyd_warshall_multi(n, path, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_floyd_warshall_multi(n, path);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("path\n");
  // print_multi_2d(n, n, path);

  free_multi_2d(n, n, path);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_floyd_warshall_multi(n);

  finalize();

  return 0;
}
