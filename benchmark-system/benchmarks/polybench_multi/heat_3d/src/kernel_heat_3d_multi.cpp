

#include "utilities.hpp"




void
kernel_heat_3d_multi(long tsteps, long n,
		     DATA_TYPE*** __restrict__ A, // n by n by n
		     DATA_TYPE*** __restrict__ B) // n by n by n
{
  long t, i, j, k;


  for (t = 1; t <= tsteps; t++) {
    for (i = 1; i < n-1; i++) {
      for (j = 1; j < n-1; j++) {
	for (k = 1; k < n-1; k++) {
	  B[i][j][k] =   ((DATA_TYPE) 0.125) * (A[i+1][j][k] - ((DATA_TYPE) 2.0) * A[i][j][k] + A[i-1][j][k])
	    + ((DATA_TYPE) 0.125) * (A[i][j+1][k] - ((DATA_TYPE) 2.0) * A[i][j][k] + A[i][j-1][k])
	    + ((DATA_TYPE) 0.125) * (A[i][j][k+1] - ((DATA_TYPE) 2.0) * A[i][j][k] + A[i][j][k-1])
	    + A[i][j][k];
	}
      }
    }
    for (i = 1; i < n-1; i++) {
      for (j = 1; j < n-1; j++) {
	for (k = 1; k < n-1; k++) {
	  A[i][j][k] =   ((DATA_TYPE) 0.125) * (B[i+1][j][k] - ((DATA_TYPE) 2.0) * B[i][j][k] + B[i-1][j][k])
	    + ((DATA_TYPE) 0.125) * (B[i][j+1][k] - ((DATA_TYPE) 2.0) * B[i][j][k] + B[i][j-1][k])
	    + ((DATA_TYPE) 0.125) * (B[i][j][k+1] - ((DATA_TYPE) 2.0) * B[i][j][k] + B[i][j][k-1])
	    + B[i][j][k];
	}
      }
    }
  }
}
