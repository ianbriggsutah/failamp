

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_heat_3d_multi(long tsteps, long n,
		     DATA_TYPE*** __restrict__ A,  // n by n by n
		     DATA_TYPE*** __restrict__ B); // n by n by n




void
data_init_heat_3d_multi(long n,
			DATA_TYPE*** __restrict__ A, // n by n by n
			DATA_TYPE*** __restrict__ B, // n by n by n
			int seed)
{
  long i, j, k;
  init_multi_3d(n, n, n, A, seed);

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      for (k=0; k<n; k++) {
	B[i][j][k] = A[i][j][k];
      }
    }
  }
}


void
run_heat_3d_multi(long tsteps, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE*** A = malloc_multi_3d(n, n, n);
  DATA_TYPE*** B = malloc_multi_3d(n, n, n);

  size_t bytes = sizeof(DATA_TYPE) * (2*n*n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\theat_3d\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", tsteps, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_heat_3d_multi(n, A, B, tsteps+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_heat_3d_multi(tsteps, n, A, B);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_multi_3d(n, n, n, A);
  // printf("B\n");
  // print_multi_3d(n, n, n, B);

  free_multi_3d(n, n, n, A);
  free_multi_3d(n, n, n, B);
}


int
main(int vargc, char** vargv)
{
  long tsteps, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &tsteps) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [tsteps n]\n", argv[0]);
    printf("  2 <= tsteps <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_heat_3d_multi(tsteps, n);

  finalize();

  return 0;
}
