

#include "utilities.hpp"




void
kernel_seidel_2d_multi(long tsteps, long n,
		       DATA_TYPE** __restrict__ A) // n by n
{
  long t, i, j;

  for (t=0; t<=(tsteps - 1); t++) {
    for (i=1; i<=(n - 2); i++) {
      for (j=1; j<=(n - 2); j++) {
	A[i][j] = (A[(i-1)][(j-1)] + A[(i-1)][j] + A[(i-1)][(j+1)]
		   + A[i][(j-1)] + A[i][j] + A[i][(j+1)]
		   + A[(i+1)][(j-1)] + A[(i+1)][j] + A[(i+1)][(j+1)])/((DATA_TYPE) 9.0);
      }
    }
  }
}
