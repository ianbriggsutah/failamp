

#include "utilities.hpp"




void
kernel_ludcmp_multi(long n,
		    DATA_TYPE** __restrict__ A, // n by n
		    DATA_TYPE* __restrict__ b, // n
		    DATA_TYPE* __restrict__ x, // n
		    DATA_TYPE* __restrict__ y) // n
{
  long i, j, k;

  DATA_TYPE w;

  for (i=0; i<n; i++) {
    for (j=0; j <i; j++) {
      w = A[i][j];
      for (k=0; k<j; k++) {
	w -= A[i][k] * A[k][j];
      }
      A[i][j] = w / A[j][j];
    }
    for (j=i; j<n; j++) {
      w = A[i][j];
      for (k=0; k<i; k++) {
	w -= A[i][k] * A[k][j];
      }
      A[i][j] = w;
    }
  }

  for (i=0; i<n; i++) {
    w = b[i];
    for (j=0; j<i; j++) {
      w -= A[i][j] * y[j];
    }
    y[i] = w;
  }

  for (i=n-1; i >=0; i--) {
    w = y[i];
    for (j=i+1; j<n; j++) {
      w -= A[i][j] * x[j];
    }
    x[i] = w / A[i][i];
  }
}
