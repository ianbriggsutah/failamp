

#include "utilities.hpp"




void
kernel_cholesky_multi(long n,
		      DATA_TYPE** __restrict__ A) // n by n
{
  long i, j, k;

  for (i=0; i<n; i++) {
    //j<i
    for (j=0; j<i; j++) {
      for (k=0; k<j; k++) {
	A[i][j] -= A[i][k] * A[j][k];
      }
      A[i][j] /= A[j][j];
    }
    // i==j case
    for (k=0; k<i; k++) {
      A[i][i] -= A[i][k] * A[i][k];
    }
    A[i][i] = SQRT(A[i][i]);
  }
}
