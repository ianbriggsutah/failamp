

#include "utilities.hpp"




void
kernel_syrk_multi(long n, long m,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE** __restrict__ C, // n by n
		  DATA_TYPE** __restrict__ A) // n by m
{
  long i, j, k;

  //BLAS PARAMS
  //TRANS = 'N'
  //UPLO  = 'L'
  // =>  Form  C := alpha*A*A**T + beta*C.
  //A is NxM
  //C is NxN
  for (i=0; i<n; i++) {
    for (j=0; j<=i; j++) {
      C[i][j] *= beta;
    }
    for (k=0; k<m; k++) {
      for (j=0; j<=i; j++) {
        C[i][j] += alpha * A[i][k] * A[j][k];
      }
    }
  }
}
