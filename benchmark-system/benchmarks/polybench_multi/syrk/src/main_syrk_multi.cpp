

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_syrk_multi(long n, long m,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE** __restrict__ C,  // n by n
		  DATA_TYPE** __restrict__ A); // n by m




void
data_init_syrk_multi(long n, long m,
		     DATA_TYPE* __restrict__ alpha,
		     DATA_TYPE* __restrict__ beta,
		     DATA_TYPE** __restrict__ C, // n by n
		     DATA_TYPE** __restrict__ A, // n by m
		     int seed)
{
  *alpha = ((DATA_TYPE) n);
  *beta = ((DATA_TYPE) m);
  init_multi_2d(n, n, C, seed);
  init_multi_2d(n, m, A, seed+1);
}


void
run_syrk_multi(long n, long m)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE** C = malloc_multi_2d(n, n);
  DATA_TYPE** A = malloc_multi_2d(n, m);

  size_t bytes = sizeof(DATA_TYPE) * (n*n + n*m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tsyrk\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", n, m);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_syrk_multi(n, m, &alpha, &beta, C, A, n+m);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_syrk_multi(n, m, alpha, beta, C, A);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("C\n");
  // print_multi_2d(n, n, C);
  // printf("A\n");
  // print_multi_2d(n, m, A);

  free_multi_2d(n, n, C);
  free_multi_2d(n, m, A);
}


int
main(int vargc, char** vargv)
{
  long n, m;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &m) != 0)) {
    printf("Usage:\n");
    printf("%s [n m]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    printf("  2 <= m <= LONG_MAX\n");
    return 1;
  }

  run_syrk_multi(n, m);

  finalize();

  return 0;
}
