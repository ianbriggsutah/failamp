

#include "utilities.hpp"




void
kernel_2mm_multi(long ni, long nj, long nk, long nl,
		 DATA_TYPE alpha,
		 DATA_TYPE beta,
		 DATA_TYPE** __restrict__ tmp, // ni by nj
		 DATA_TYPE** __restrict__ A,   // ni by nk
		 DATA_TYPE** __restrict__ B,   // nk by nj
		 DATA_TYPE** __restrict__ C,   // nj by nl
		 DATA_TYPE** __restrict__ D)   // ni by nl
{
  long i, j, k;

  /* D := alpha*A*B*C + beta*D */
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      tmp[i][j] = (DATA_TYPE) 0.0;
      for (k=0; k<nk; ++k) {
	tmp[i][j] += alpha * A[i][k] * B[k][j];
      }
    }
  }
  for (i=0; i<ni; i++) {
    for (j=0; j<nl; j++) {
      D[i][j] *= beta;
      for (k=0; k<nj; ++k) {
	D[i][j] += tmp[i][k] * C[k][j];
      }
    }
  }
}
