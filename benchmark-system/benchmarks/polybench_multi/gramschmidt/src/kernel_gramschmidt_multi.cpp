

#include "utilities.hpp"




void
kernel_gramschmidt_multi(long m, long n,
			 DATA_TYPE** __restrict__ A, // m by n
			 DATA_TYPE** __restrict__ R, // n by n
			 DATA_TYPE** __restrict__ Q) // m by n
{
  long i, j, k;

  DATA_TYPE nrm;

  for (k=0; k<n; k++) {
    nrm = (DATA_TYPE) 0.0;
    for (i=0; i<m; i++) {
      nrm += A[i][k] * A[i][k];
    }
    R[k][k] = SQRT(nrm);
    for (i=0; i<m; i++) {
      Q[i][k] = A[i][k] / R[k][k];
    }
    for (j=k + 1; j<n; j++) {
      R[k][j] = (DATA_TYPE) 0.0;
      for (i=0; i<m; i++) {
	R[k][j] += Q[i][k] * A[i][j];
      }
      for (i=0; i<m; i++) {
	A[i][j] = A[i][j] - Q[i][k] * R[k][j];
      }
    }
  }
}
