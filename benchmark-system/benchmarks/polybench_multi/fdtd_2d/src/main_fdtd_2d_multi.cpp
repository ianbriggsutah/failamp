

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_fdtd_2d_multi(long tmax, long nx, long ny,
		     DATA_TYPE** __restrict__ ex,      // nx by ny
		     DATA_TYPE** __restrict__ ey,      // nx by ny
		     DATA_TYPE** __restrict__ hz,      // nx by ny
		     DATA_TYPE* __restrict__ _fict_); // tmax




void
data_init_fdtd_2d_multi(long tmax, long nx, long ny,
			DATA_TYPE** __restrict__ ex,     // nx by ny
			DATA_TYPE** __restrict__ ey,     // nx by ny
			DATA_TYPE** __restrict__ hz,     // nx by ny
			DATA_TYPE* __restrict__ _fict_, // tmax
			int seed)
{
  init_multi_2d(nx, ny, ex, seed);
  init_multi_2d(nx, ny, ey, seed+1);
  init_multi_2d(nx, ny, hz, seed+2);
  init_multi_1d(tmax, _fict_, seed+3);
}


void
run_fdtd_2d_multi(long tmax, long nx, long ny)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE** ex = malloc_multi_2d(nx, ny);
  DATA_TYPE** ey = malloc_multi_2d(nx, ny);
  DATA_TYPE** hz = malloc_multi_2d(nx, ny);
  DATA_TYPE* _fict_ = malloc_multi_1d(tmax);

  size_t bytes = sizeof(DATA_TYPE) * (3*nx*ny + tmax);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tfdtd_2d\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\t%ld\n", tmax, nx, ny);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_fdtd_2d_multi(tmax, nx, ny, ex, ey, hz, _fict_, tmax+nx+ny);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_fdtd_2d_multi(tmax, nx, ny, ex, ey, hz, _fict_);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("ex\n");
  // print_multi_2d(nx, ny, ex);
  // printf("ey\n");
  // print_multi_2d(nx, ny, ey);
  // printf("hz\n");
  // print_multi_2d(nx, ny, hz);
  // printf("_fict_\n");
  // print_multi_1d(tmax, _fict_);

  free_multi_2d(nx, ny, ex);
  free_multi_2d(nx, ny, ey);
  free_multi_2d(nx, ny, hz);
  free_multi_1d(_fict_);
}


int
main(int vargc, char** vargv)
{
  long tmax, nx, ny;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 4) ||
      (parse_long(argv[1], 2, LONG_MAX, &tmax) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &nx) != 0) ||
      (parse_long(argv[3], 2, LONG_MAX, &ny) != 0)) {
    printf("Usage:\n");
    printf("%s [tmax nx ny]\n", argv[0]);
    printf("  2 <= tmax <= LONG_MAX\n");
    printf("  2 <= nx <= LONG_MAX\n");
    printf("  2 <= ny <= LONG_MAX\n");
    return 1;
  }

  run_fdtd_2d_multi(tmax, nx, ny);

  finalize();

  return 0;
}
