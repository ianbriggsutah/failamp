

#include "utilities.hpp"




void
kernel_fdtd_2d_multi(long tmax, long nx, long ny,
		     DATA_TYPE** __restrict__ ex,     // nx by ny
		     DATA_TYPE** __restrict__ ey,     // nx by ny
		     DATA_TYPE** __restrict__ hz,     // nx by ny
		     DATA_TYPE* __restrict__ _fict_) // tmax
{
  long t, i, j;


  for (t=0; t<tmax; t++) {
    for (j=0; j<ny; j++) {
      ey[0][j] = _fict_[t];
    }
    for (i=1; i<nx; i++) {
      for (j=0; j<ny; j++) {
	ey[i][j] = ey[i][j] - ((DATA_TYPE) 0.5)*(hz[i][j]-hz[(i-1)][j]);
      }
    }
    for (i=0; i<nx; i++) {
      for (j=1; j<ny; j++) {
	ex[i][j] = ex[i][j] - ((DATA_TYPE) 0.5)*(hz[i][j]-hz[i][(j-1)]);
      }
    }
    for (i=0; i<nx - 1; i++) {
      for (j=0; j<ny - 1; j++) {
	hz[i][j] = hz[i][j] - ((DATA_TYPE) 0.7)*  (ex[i][(j+1)] - ex[i][j] +
						   ey[(i+1)][j] - ey[i][j]);
      }
    }
  }
}
