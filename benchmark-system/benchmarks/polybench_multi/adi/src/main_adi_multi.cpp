

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_adi_multi(long tsteps, long n,
		 DATA_TYPE** __restrict__ u,  // n by n
		 DATA_TYPE** __restrict__ v,  // n by n
		 DATA_TYPE** __restrict__ p,  // n by n
		 DATA_TYPE** __restrict__ q); // n by n




void
data_init_adi_multi(long n,
		    DATA_TYPE** __restrict__ u, // n by n
		    int seed)
{
  init_multi_2d(n, n, u, seed);
}


void
run_adi_multi(long tsteps, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE** u = malloc_multi_2d(n, n);
  DATA_TYPE** v = malloc_multi_2d(n, n);
  DATA_TYPE** p = malloc_multi_2d(n, n);
  DATA_TYPE** q = malloc_multi_2d(n, n);

  size_t bytes = sizeof(DATA_TYPE) * (4*n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tadi\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", tsteps, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_adi_multi(n, u, tsteps+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_adi_multi(tsteps, n, u, v, p, q);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("u\n");
  // print_multi_2d(n, n, u);
  // printf("v\n");
  // print_multi_2d(n, n, v);
  // printf("p\n");
  // print_multi_2d(n, n, p);
  // printf("q\n");
  // print_multi_2d(n, n, q);

  free_multi_2d(n, n, u);
  free_multi_2d(n, n, v);
  free_multi_2d(n, n, p);
  free_multi_2d(n, n, q);
}


int
main(int vargc, char** vargv)
{
  long tsteps, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &tsteps) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [tsteps n]\n", argv[0]);
    printf("  2 <= tsteps <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");

    return 1;
  }

  run_adi_multi(tsteps, n);

  finalize();

  return 0;
}
