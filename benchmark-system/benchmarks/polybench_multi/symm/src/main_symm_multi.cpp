

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_symm_multi(long m, long n,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE** __restrict__ C,  // m by n
		  DATA_TYPE** __restrict__ A,  // m by m
		  DATA_TYPE** __restrict__ B); // m by n




void
data_init_symm_multi(long m, long n,
		     DATA_TYPE* __restrict__ alpha,
		     DATA_TYPE* __restrict__ beta,
		     DATA_TYPE** __restrict__ C, // m by n
		     DATA_TYPE** __restrict__ A, // m by m
		     DATA_TYPE** __restrict__ B, // m by n
		     int seed)
{
  *alpha = ((DATA_TYPE) m);
  *beta = ((DATA_TYPE) n);
  init_multi_2d(m, n, C, seed);
  init_multi_2d(m, m, A, seed+1);
  init_multi_2d(m, n, B, seed+2);
}


void
run_symm_multi(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE** C = malloc_multi_2d(m, n);
  DATA_TYPE** A = malloc_multi_2d(m, m);
  DATA_TYPE** B = malloc_multi_2d(m, n);

  size_t bytes = sizeof(DATA_TYPE) * (2*m*n + m*m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tsymm\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_symm_multi(m, n, &alpha, &beta, C, A, B, m+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_symm_multi(m, n, alpha, beta, C, A, B);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("C\n");
  // print_multi_2d(m, n, C);
  // printf("A\n");
  // print_multi_2d(m, m, A);
  // printf("B\n");
  // print_multi_2d(m, n, B);

  free_multi_2d(m, n, C);
  free_multi_2d(m, m, A);
  free_multi_2d(m, n, B);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_symm_multi(m, n);

  finalize();

  return 0;
}
