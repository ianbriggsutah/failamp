

#include "utilities.hpp"




void
kernel_jacobi_1d_multi(long tsteps, long n,
		       DATA_TYPE* __restrict__ A, // n
		       DATA_TYPE* __restrict__ B) // n
{
  long t, i;

  for (t=0; t<tsteps; t++) {
    for (i=1; i<n - 1; i++) {
      B[i] = 0.33333 * (A[i-1] + A[i] + A[i + 1]);
    }
    for (i=1; i<n - 1; i++) {
      A[i] = 0.33333 * (B[i-1] + B[i] + B[i + 1]);
    }
  }
}
