

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_jacobi_1d_multi(long tsteps, long n,
		       DATA_TYPE* __restrict__ A,  // n
		       DATA_TYPE* __restrict__ B); // n




void
data_init_jacobi_1d_multi(long n,
			  DATA_TYPE* __restrict__ A, // n
			  DATA_TYPE* __restrict__ B, // n
			  int seed)
{
  init_multi_1d(n, A, seed);
  init_multi_1d(n, B, seed+1);
}


void
run_jacobi_multi_1d_multi(long tsteps, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* A = malloc_multi_1d(n);
  DATA_TYPE* B = malloc_multi_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (2*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tjacobi_1d\n");
  printf("Layout:   \tmulti\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", tsteps, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_jacobi_1d_multi(n, A, B, tsteps+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_jacobi_1d_multi(tsteps, n, A, B);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_multi_1d(n, A);
  // printf("B\n");
  // print_multi_1d(n, B);

  free_multi_1d(A);
  free_multi_1d(B);
}


int
main(int vargc, char** vargv)
{
  long tsteps, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &tsteps) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [tsteps n]\n", argv[0]);
    printf("  2 <= tsteps <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_jacobi_multi_1d_multi(tsteps, n);

  finalize();

  return 0;
}
