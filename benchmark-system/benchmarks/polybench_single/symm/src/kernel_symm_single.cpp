

#include "utilities.hpp"




void
kernel_symm_single(long m, long n,
		   DATA_TYPE alpha,
		   DATA_TYPE beta,
		   DATA_TYPE* __restrict__ C, // m by n
		   DATA_TYPE* __restrict__ A, // m by m
		   DATA_TYPE* __restrict__ B) // m by n
{
  long i, j, k;
  DATA_TYPE temp2;

  //BLAS PARAMS
  //SIDE = 'L'
  //UPLO = 'L'
  // =>  Form  C := alpha*A*B + beta*C
  // A is MxM
  // B is MxN
  // C is MxN
  //note that due to Fortran array layout, the code below more closely resembles upper triangular case in BLAS
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      temp2 = 0;
      for (k=0; k<i; k++) {
	C[k*n + j] += alpha*B[i*n + j] * A[i*m + k];
	temp2 += B[k*n + j] * A[i*m + k];
      }
      C[i*n + j] = beta * C[i*n + j] + alpha*B[i*n + j] * A[i*m + i] + alpha * temp2;
    }
  }
}
