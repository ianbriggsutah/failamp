

#include "utilities.hpp"




void
kernel_floyd_warshall_single(long n,
			     DATA_TYPE* __restrict__ path) // n by n
{
  long i, j, k;

  for (k=0; k<n; k++) {
    for (i=0; i<n; i++) {
      for (j=0; j<n; j++) {
	path[i*n + j] = path[i*n + j]<path[i*n + k] + path[k*n + j] ?
				      path[i*n + j] : path[i*n + k] + path[k*n + j];
      }
    }
  }
}
