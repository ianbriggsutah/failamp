

#include "utilities.hpp"




void
kernel_ludcmp_single(long n,
		     DATA_TYPE* __restrict__ A, // n by n
		     DATA_TYPE* __restrict__ b, // n
		     DATA_TYPE* __restrict__ x, // n
		     DATA_TYPE* __restrict__ y) // n
{
  long i, j, k;

  DATA_TYPE w;

  for (i=0; i<n; i++) {
    for (j=0; j <i; j++) {
      w = A[i*n + j];
      for (k=0; k<j; k++) {
	w -= A[i*n + k] * A[k*n + j];
      }
      A[i*n + j] = w / A[j*n + j];
    }
    for (j=i; j<n; j++) {
      w = A[i*n + j];
      for (k=0; k<i; k++) {
	w -= A[i*n + k] * A[k*n + j];
      }
      A[i*n + j] = w;
    }
  }

  for (i=0; i<n; i++) {
    w = b[i];
    for (j=0; j<i; j++) {
      w -= A[i*n + j] * y[j];
    }
    y[i] = w;
  }

  for (i=n-1; i >=0; i--) {
    w = y[i];
    for (j=i+1; j<n; j++) {
      w -= A[i*n + j] * x[j];
    }
    x[i] = w / A[i*n + i];
  }
}
