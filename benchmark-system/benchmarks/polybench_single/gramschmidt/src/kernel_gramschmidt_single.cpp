

#include "utilities.hpp"




void
kernel_gramschmidt_single(long m, long n,
			  DATA_TYPE* __restrict__ A, // m by n
			  DATA_TYPE* __restrict__ R, // n by n
			  DATA_TYPE* __restrict__ Q) // m by n
{
  long i, j, k;

  DATA_TYPE nrm;

  for (k=0; k<n; k++) {
    nrm = (DATA_TYPE) 0.0;
    for (i=0; i<m; i++) {
      nrm += A[i*n + k] * A[i*n + k];
    }
    R[k*n + k] = SQRT(nrm);
    for (i=0; i<m; i++) {
      Q[i*n + k] = A[i*n + k] / R[k*n + k];
    }
    for (j=k + 1; j<n; j++) {
      R[k*n + j] = (DATA_TYPE) 0.0;
      for (i=0; i<m; i++) {
	R[k*n + j] += Q[i*n + k] * A[i*n + j];
      }
      for (i=0; i<m; i++) {
	A[i*n + j] = A[i*n + j] - Q[i*n + k] * R[k*n + j];
      }
    }
  }
}
