

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_gramschmidt_single(long m, long n,
			  DATA_TYPE* __restrict__ A,  // m by n
			  DATA_TYPE* __restrict__ R,  // n by n
			  DATA_TYPE* __restrict__ Q); // m by n




void
data_init_gramschmidt_single(long m, long n,
			     DATA_TYPE* __restrict__ A, // m by n
			     DATA_TYPE* __restrict__ R, // n by n
			     DATA_TYPE* __restrict__ Q, // m by n
			     int seed)
{
  init_single_2d(m, n, A, seed);
  init_single_2d(n, n, R, seed+1);
  init_single_2d(m, n, Q, seed+2);
}


void
run_gramschmidt_single(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* A = malloc_single_2d(m, n);
  DATA_TYPE* R = malloc_single_2d(n, n);
  DATA_TYPE* Q = malloc_single_2d(m, n);

  size_t bytes = sizeof(DATA_TYPE) * (2*m*n + n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tgramschmidt\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_gramschmidt_single(m, n, A, R, Q, m+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_gramschmidt_single(m, n, A, R, Q);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_single_2d(m, n, A);
  // printf("R\n");
  // print_single_2d(n, n, R);
  // printf("Q\n");
  // print_single_2d(m, n, Q);

  free_single_2d(m, n, A);
  free_single_2d(n, n, R);
  free_single_2d(m, n, Q);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_gramschmidt_single(m, n);

  finalize();

  return 0;
}
