

#include "utilities.hpp"




void
kernel_3mm_single(long ni, long nj, long nk, long nl, long nm,
		  DATA_TYPE* __restrict__ E, // ni by nj
		  DATA_TYPE* __restrict__ A, // ni by nk
		  DATA_TYPE* __restrict__ B, // nk by nj
		  DATA_TYPE* __restrict__ F, // nj by nl
		  DATA_TYPE* __restrict__ C, // nj by nm
		  DATA_TYPE* __restrict__ D, // nm by nl
		  DATA_TYPE* __restrict__ G) // ni by nl
{
  long i, j, k;

  /* E := A*B */
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      E[i*nj + j] = (DATA_TYPE) 0.0;
      for (k=0; k<nk; ++k) {
	E[i*nj + j] += A[i*nk + k] * B[k*nj + j];
      }
    }
  }
  /* F := C*D */
  for (i=0; i<nj; i++) {
    for (j=0; j<nl; j++) {
      F[i*nl + j] = (DATA_TYPE) 0.0;
      for (k=0; k<nm; ++k) {
	F[i*nl + j] += C[i*nm + k] * D[k*nl + j];
      }
    }
  }
  /* G := E*F */
  for (i=0; i<ni; i++) {
    for (j=0; j<nl; j++) {
      G[i*nl + j] = (DATA_TYPE) 0.0;
      for (k=0; k<nj; ++k) {
	G[i*nl + j] += E[i*nj + k] * F[k*nl + j];
      }
    }
  }
}
