

#include "utilities.hpp"




void
kernel_covariance_single(long m, long n,
			 DATA_TYPE float_n,
			 DATA_TYPE* __restrict__ data, // n by m
			 DATA_TYPE* __restrict__ cov,  // m by m
			 DATA_TYPE* __restrict__ mean) // m
{
  long i, j, k;

  for (j=0; j<m; j++) {
    mean[j] = (DATA_TYPE) 0.0;
    for (i=0; i<n; i++) {
      mean[j] += data[i*m + j];
    }
    mean[j] /= float_n;
  }

  for (i=0; i<n; i++) {
    for (j=0; j<m; j++) {
      data[i*m + j] -= mean[j];
    }
  }

  for (i=0; i<m; i++) {
    for (j=i; j<m; j++) {
      cov[i*m + j] = (DATA_TYPE) 0.0;
      for (k=0; k<n; k++) {
	cov[i*m + j] += data[k*m + i] * data[k*m + j];
      }
      cov[i*m + j] /= float_n - (DATA_TYPE) 1.0;
      cov[j*m + i] = cov[i*m + j];
    }
  }
}
