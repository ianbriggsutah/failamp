

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_covariance_single(long m, long n,
			 DATA_TYPE float_n,
			 DATA_TYPE* __restrict__ data,  // n by m
			 DATA_TYPE* __restrict__ cov,   // m by m
			 DATA_TYPE* __restrict__ mean); // m




void
data_init_covariance_single(long m, long n,
			    DATA_TYPE* __restrict__ float_n,
			    DATA_TYPE* __restrict__ data, // n by m
			    int seed)
{
  *float_n = (DATA_TYPE) m;
  init_single_2d(n, m, data, seed);
}


void
run_covariance_single(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE float_n;
  DATA_TYPE* data = malloc_single_2d(n, m);
  DATA_TYPE* cov = malloc_single_2d(m, m);
  DATA_TYPE* mean = malloc_single_1d(m);

  size_t bytes = sizeof(DATA_TYPE) * (n*m + m*m + m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tcovariance\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_covariance_single(m, n, &float_n, data, m+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_covariance_single(m, n, float_n, data, cov, mean);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("data\n");
  // print_single_2d(n, m, data);
  // printf("cov\n");
  // print_single_2d(m, m, cov);
  // printf("mean\n");
  // print_single_1d(m, mean);

  free_single_2d(n, m, data);
  free_single_2d(m, m, cov);
  free_single_1d(mean);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_covariance_single(m, n);

  finalize();

  return 0;
}
