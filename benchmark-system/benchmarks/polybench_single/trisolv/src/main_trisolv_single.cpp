

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_trisolv_single(long n,
		      DATA_TYPE* __restrict__ L,  // n by n
		      DATA_TYPE* __restrict__ x,  // n
		      DATA_TYPE* __restrict__ b); // n




void
data_init_trisolv_single(long n,
			 DATA_TYPE* __restrict__ L, // n by n
			 DATA_TYPE* __restrict__ x, // n
			 DATA_TYPE* __restrict__ b, // n
			 int seed)
{
  init_single_2d(n, n, L, seed);
  init_single_1d(n, x, seed+1);
  init_single_1d(n, b, seed+2);
}


void
run_trisolv_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* L = malloc_single_2d(n, n);
  DATA_TYPE* x = malloc_single_1d(n);
  DATA_TYPE* b = malloc_single_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (n*n + 2*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\ttrisolv\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_trisolv_single(n, L, x, b, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_trisolv_single(n, L, x, b);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("L\n");
  // print_single_2d(n, n, L);
  // printf("x\n");
  // print_single_1d(n, x);
  // printf("b\n");
  // print_single_1d(n, b);

  free_single_2d(n, n, L);
  free_single_1d(x);
  free_single_1d(b);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_trisolv_single(n);

  finalize();

  return 0;
}
