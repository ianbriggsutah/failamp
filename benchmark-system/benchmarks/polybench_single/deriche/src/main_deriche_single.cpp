

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_deriche_single(long w, long h,
		      DATA_TYPE alpha,
		      DATA_TYPE* __restrict__ imgIn,  // w by h
		      DATA_TYPE* __restrict__ imgOut, // w by h
		      DATA_TYPE* __restrict__ y1,     // w by h
		      DATA_TYPE* __restrict__ y2);    // w by h




void
data_init_deriche_single(long w, long h,
			 DATA_TYPE* __restrict__ alpha,
			 DATA_TYPE* __restrict__ imgIn, // w by h
			 int seed)
{
  long i, j;
  *alpha = (DATA_TYPE) w;
  init_single_2d(w, h, imgIn, seed);

  for (i=0; i<w; i++) {
    for (j=0; j<h; j++) {
      imgIn[i*h + j] /= RAND_MAX;
    }
  }
}


void
run_deriche_single(long w, long h)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE* imgIn = malloc_single_2d(w, h);
  DATA_TYPE* imgOut = malloc_single_2d(w, h);
  DATA_TYPE* y1 = malloc_single_2d(w, h);
  DATA_TYPE* y2 = malloc_single_2d(w, h);

  size_t bytes = sizeof(DATA_TYPE) * (4*w*h);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tderiche\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", w, h);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_deriche_single(w, h, &alpha, imgIn, w+h);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_deriche_single(w, h, alpha, imgIn, imgOut, y1, y2);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("imgIn\n");
  // print_single_2d(w, h, imgIn);
  // printf("imgOut\n");
  // print_single_2d(w, h, imgOut);
  // printf("y1\n");
  // print_single_2d(w, h, y1);
  // printf("y2\n");
  // print_single_2d(w, h, y2);

  free_single_2d(w, h, imgIn);
  free_single_2d(w, h, imgOut);
  free_single_2d(w, h, y1);
  free_single_2d(w, h, y2);
}


int
main(int vargc, char** vargv)
{
  long w, h;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &w) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &h) != 0)) {
    printf("Usage:\n");
    printf("%s [w h]\n", argv[0]);
    printf("  2 <= w <= LONG_MAX\n");
    printf("  2 <= h <= LONG_MAX\n");
    return 1;
  }

  run_deriche_single(w, h);

  finalize();

  return 0;
}
