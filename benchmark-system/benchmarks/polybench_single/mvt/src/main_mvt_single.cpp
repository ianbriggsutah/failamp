

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_mvt_single(long n,
		  DATA_TYPE* __restrict__ x1,  // n
		  DATA_TYPE* __restrict__ x2,  // n
		  DATA_TYPE* __restrict__ y_1, // n
		  DATA_TYPE* __restrict__ y_2, // n
		  DATA_TYPE* __restrict__ A);  // n by n




void
data_init_mvt_single(long n,
		     DATA_TYPE* __restrict__ x1,  // n
		     DATA_TYPE* __restrict__ x2,  // n
		     DATA_TYPE* __restrict__ y_1, // n
		     DATA_TYPE* __restrict__ y_2, // n
		     DATA_TYPE* __restrict__ A,   // n by n
		     int seed)
{
  init_single_1d(n, x1, seed);
  init_single_1d(n, x2, seed+1);
  init_single_1d(n, y_1, seed+2);
  init_single_1d(n, y_2, seed+3);
  init_single_2d(n, n, A, seed+4);
}


void
run_mvt_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* x1 = malloc_single_1d(n);
  DATA_TYPE* x2 = malloc_single_1d(n);
  DATA_TYPE* y_1 = malloc_single_1d(n);
  DATA_TYPE* y_2 = malloc_single_1d(n);
  DATA_TYPE* A = malloc_single_2d(n, n);

  size_t bytes = sizeof(DATA_TYPE) * (4*n + n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tmvt\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_mvt_single(n, x1, x2, y_1, y_2, A, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_mvt_single(n, x1, x2, y_1, y_2, A);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("x1\n");
  // print_single_1d(n, x1);
  // printf("x2\n");
  // print_single_1d(n, x2);
  // printf("y_1\n");
  // print_single_1d(n, y_1);
  // printf("y_2\n");
  // print_single_1d(n, y_2);
  // printf("A\n");
  // print_single_2d(n, n, A);

  free_single_1d(x1);
  free_single_1d(x2);
  free_single_1d(y_1);
  free_single_1d(y_2);
  free_single_2d(n, n, A);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_mvt_single(n);

  finalize();

  return 0;
}
