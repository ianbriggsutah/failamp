

#include "utilities.hpp"




void
kernel_atax_single(long m, long n,
		   DATA_TYPE* __restrict__ A,   // m by n
		   DATA_TYPE* __restrict__ x,   // n
		   DATA_TYPE* __restrict__ y,   // n
		   DATA_TYPE* __restrict__ tmp) // m
{
  long i, j;

  for (i=0; i<n; i++) {
    y[i] = 0;
  }
  for (i=0; i<m; i++) {
    tmp[i] = (DATA_TYPE) 0.0;
    for (j=0; j<n; j++) {
      tmp[i] = tmp[i] + A[i*n + j] * x[j];
    }
    for (j=0; j<n; j++) {
      y[j] = y[j] + A[i*n + j] * tmp[i];
    }
  }
}
