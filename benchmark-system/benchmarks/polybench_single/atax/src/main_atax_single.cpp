

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>






void
kernel_atax_single(long m, long n,
		   DATA_TYPE* __restrict__ A,    // m by n
		   DATA_TYPE* __restrict__ x,    // n
		   DATA_TYPE* __restrict__ y,    // n
		   DATA_TYPE* __restrict__ tmp); // m




void
data_init_atax_single(long m, long n,
		      DATA_TYPE* __restrict__ A, // m by n
		      DATA_TYPE* __restrict__ x, // n
		      int seed)
{
  init_single_2d(m, n, A, seed);
  init_single_1d(n, x, seed+1);
}


void
run_atax_single(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* A = malloc_single_2d(m, n);
  DATA_TYPE* x = malloc_single_1d(n);
  DATA_TYPE* y = malloc_single_1d(n);
  DATA_TYPE* tmp = malloc_single_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (m*n + 2*n + m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tatax\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_atax_single(m, n, A, x, n + m);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_atax_single(m, n, A, x, y, tmp);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_single_2d(m, n, A);
  // printf("x\n");
  // print_single_1d(n, x);
  // printf("y\n");
  // print_single_1d(n, y);
  // printf("tmp\n");
  // print_single_1d(n, tmp);

  free_single_2d(m, n, A);
  free_single_1d(x);
  free_single_1d(y);
  free_single_1d(tmp);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &m) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_atax_single(m, n);

  finalize();

  return 0;
}
