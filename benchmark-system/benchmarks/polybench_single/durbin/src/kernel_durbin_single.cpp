

#include "utilities.hpp"




void
kernel_durbin_single(long n,
		     DATA_TYPE* __restrict__ r, // n
		     DATA_TYPE* __restrict__ y) // n
{
  DATA_TYPE *z = (DATA_TYPE*) malloc(sizeof(DATA_TYPE)*n);
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE sum;

  long i,k;

  y[0] = -r[0];
  beta = (DATA_TYPE) 1.0;
  alpha = -r[0];

  for (k=1; k<n; k++) {
    beta = (1-alpha*alpha)*beta;
    sum = (DATA_TYPE) 0.0;
    for (i=0; i<k; i++) {
      sum += r[k-i-1]*y[i];
    }
    alpha = - (r[k] + sum)/beta;

    for (i=0; i<k; i++) {
      z[i] = y[i] + alpha*y[k-i-1];
    }
    for (i=0; i<k; i++) {
      y[i] = z[i];
    }
    y[k] = alpha;
  }

  free(z);
}
