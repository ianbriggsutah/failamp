

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_durbin_single(long n,
		     DATA_TYPE* __restrict__ r,  // n
		     DATA_TYPE* __restrict__ y); // n




void
data_init_durbin_single(long n,
			DATA_TYPE* __restrict__ r, // n
			int seed)
{
  init_single_1d(n, r, seed);
}


void
run_durbin_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* r = malloc_single_1d(n);
  DATA_TYPE* y = malloc_single_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (2*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tdurbin\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_durbin_single(n, r, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_durbin_single(n, r, y);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("r\n");
  // print_single_1d(n, r);
  // printf("y\n");
  // print_single_1d(n, y);

  free_single_1d(r);
  free_single_1d(y);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_durbin_single(n);

  finalize();

  return 0;
}
