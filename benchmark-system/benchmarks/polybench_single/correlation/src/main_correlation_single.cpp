

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_correlation_single(long m, long n,
			  DATA_TYPE float_n,
			  DATA_TYPE* __restrict__ data,    // n by m
			  DATA_TYPE* __restrict__ corr,    // m by m
			  DATA_TYPE* __restrict__ mean,    // m
			  DATA_TYPE* __restrict__ stddev); // m




void
data_init_correlation_single(long m, long n,
			     DATA_TYPE* __restrict__ float_n,
			     DATA_TYPE* __restrict__ data, // n by m
			     int seed)
{
  *float_n = (DATA_TYPE) m;
  init_single_2d(n, m, data, seed);
}


void
run_correlation_single(long m, long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE float_n;
  DATA_TYPE* data = malloc_single_2d(n, m);
  DATA_TYPE* corr = malloc_single_2d(m, m);
  DATA_TYPE* mean = malloc_single_1d(m);
  DATA_TYPE* stddev = malloc_single_1d(m);

  size_t bytes = sizeof(DATA_TYPE) * (n*m + m*m + 2*m);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tcorrelation\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\n", m, n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_correlation_single(m, n, &float_n, data, m+n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_correlation_single(m, n, float_n, data, corr, mean, stddev);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("data\n");
  // print_single_2d(n, m, data);
  // printf("corr\n");
  // print_single_2d(m, m, corr);
  // printf("mean\n");
  // print_single_1d(m, mean);
  // printf("stddev\n");
  // print_single_1d(m, stddev);

  free_single_2d(n, m, data);
  free_single_2d(m, m, corr);
  free_single_1d(mean);
  free_single_1d(stddev);
}


int
main(int vargc, char** vargv)
{
  long m, n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 3) ||
      (parse_long(argv[1], 2, LONG_MAX, &m) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [m n]\n", argv[0]);
    printf("  2 <= m <= LONG_MAX\n");
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_correlation_single(m, n);

  finalize();

  return 0;
}
