

#include "utilities.hpp"




void
kernel_correlation_single(long m, long n,
			  DATA_TYPE float_n,
			  DATA_TYPE* __restrict__ data,   // n by m
			  DATA_TYPE* __restrict__ corr,   // m by m
			  DATA_TYPE* __restrict__ mean,   // m
			  DATA_TYPE* __restrict__ stddev) // m
{
  long i, j, k;

  DATA_TYPE eps = (DATA_TYPE) 0.1;

  for (j=0; j<m; j++) {
    mean[j] = (DATA_TYPE) 0.0;
    for (i=0; i<n; i++) {
      mean[j] += data[i*m + j];
    }
    mean[j] /= float_n;
  }

  for (j=0; j<m; j++) {
    stddev[j] = (DATA_TYPE) 0.0;
    for (i=0; i<n; i++) {
      stddev[j] += (data[i*m + j] - mean[j]) * (data[i*m + j] - mean[j]);
    }
    stddev[j] /= float_n;
    stddev[j] = SQRT(stddev[j]);
    /* The following in an inelegant but usual way to handle
       near-zero std. dev. values, which below would cause a zero-
       divide. */
    stddev[j] = stddev[j] <= eps ? (DATA_TYPE) 1.0 : stddev[j];
  }

  /* Center and reduce the column vectors. */
  for (i=0; i<n; i++) {
    for (j=0; j<m; j++) {
      data[i*m + j] -= mean[j];
      data[i*m + j] /= SQRT(float_n) * stddev[j];
    }
  }

  /* Calculate the m * m correlation matrix. */
  for (i=0; i<(m-1); i++) {
    corr[i*m + i] = (DATA_TYPE) 1.0;
    for (j=i+1; j<m; j++) {
      corr[i*m + j] = (DATA_TYPE) 0.0;
      for (k=0; k<n; k++) {
	corr[i*m + j] += (data[k*m + j] * data[k*m + j]);
      }
      corr[j*m + i] = corr[i*m + j];
    }
  }

  corr[(m-1)*m + (m-1)] = (DATA_TYPE) 1.0;
}
