

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_nussinov_single(long n,
		       DATA_TYPE* __restrict__ seq,    // n
		       DATA_TYPE* __restrict__ table); // n by n




void
data_init_nussinov_single(long n,
			  DATA_TYPE* __restrict__ seq, // n
			  int seed)
{
  long i;
  srandom(seed);
  for (i=0; i<n; i++) {
    seq[i] = random() % 4;
  }
}


void
run_nussinov_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* seq = malloc_single_1d(n);
  DATA_TYPE* table = malloc_single_2d(n, n);

  size_t bytes = sizeof(DATA_TYPE) * (n + n*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tnussinov\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_nussinov_single(n, seq, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_nussinov_single(n, seq, table);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("seq\n");
  // print_single_1d(n, seq);
  // printf("table\n");
  // print_single_2d(n, n, table);

  free_single_1d(seq);
  free_single_2d(n, n, table);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_nussinov_single(n);

  finalize();

  return 0;
}
