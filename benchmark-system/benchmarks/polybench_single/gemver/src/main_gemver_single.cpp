

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>






void
kernel_gemver_single(long n,
		     DATA_TYPE alpha,
		     DATA_TYPE beta,
		     DATA_TYPE* __restrict__ A,  // n by n
		     DATA_TYPE* __restrict__ u1, // n
		     DATA_TYPE* __restrict__ v1, // n
		     DATA_TYPE* __restrict__ u2, // n
		     DATA_TYPE* __restrict__ v2, // n
		     DATA_TYPE* __restrict__ w,  // n
		     DATA_TYPE* __restrict__ x,  // n
		     DATA_TYPE* __restrict__ y,  // n
		     DATA_TYPE* __restrict__ z); // n




void
data_init_gemver_single(long n,
			DATA_TYPE* __restrict__ alpha,
			DATA_TYPE* __restrict__ beta,
			DATA_TYPE* __restrict__ A,  // n by n
			DATA_TYPE* __restrict__ u1, // n
			DATA_TYPE* __restrict__ v1, // n
			DATA_TYPE* __restrict__ u2, // n
			DATA_TYPE* __restrict__ v2, // n
			DATA_TYPE* __restrict__ w,  // n
			DATA_TYPE* __restrict__ x,  // n
			DATA_TYPE* __restrict__ y,  // n
			DATA_TYPE* __restrict__ z,  // n
			int seed)
{
  *alpha = ((DATA_TYPE) n);
  *beta = ((DATA_TYPE) n*2 - 1);
  init_single_2d(n, n, A, seed);
  init_single_1d(n, u1, seed+1);
  init_single_1d(n, v1, seed+2);
  init_single_1d(n, u2, seed+3);
  init_single_1d(n, v2, seed+2);
  init_single_1d(n, w, seed+5);
  init_single_1d(n, x, seed+6);
  init_single_1d(n, y, seed+7);
  init_single_1d(n, z, seed+8);
}


void
run_gemver_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE* A = malloc_single_2d(n, n);
  DATA_TYPE* u1 = malloc_single_1d(n);
  DATA_TYPE* v1 = malloc_single_1d(n);
  DATA_TYPE* u2 = malloc_single_1d(n);
  DATA_TYPE* v2 = malloc_single_1d(n);
  DATA_TYPE* w = malloc_single_1d(n);
  DATA_TYPE* x = malloc_single_1d(n);
  DATA_TYPE* y = malloc_single_1d(n);
  DATA_TYPE* z = malloc_single_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (n*n + 8*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tgemver\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_gemver_single(n, &alpha, &beta, A, u1, v1, u2, v2, w, x, y, z, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_gemver_single(n, alpha, beta, A, u1, v1, u2, v2, w, x, y, z);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_single_2d(n, n, A);
  // printf("u1\n");
  // print_single_1d(n, u1);
  // printf("v1\n");
  // print_single_1d(n, v1);
  // printf("u2\n");
  // print_single_1d(n, u2);
  // printf("v2\n");
  // print_single_1d(n, v2);
  // printf("w\n");
  // print_single_1d(n, w);
  // printf("x\n");
  // print_single_1d(n, x);
  // printf("y\n");
  // print_single_1d(n, y);
  // printf("z\n");
  // print_single_1d(n, z);


  free_single_2d(n, n, A);
  free_single_1d(u1);
  free_single_1d(v1);
  free_single_1d(u2);
  free_single_1d(v2);
  free_single_1d(w);
  free_single_1d(x);
  free_single_1d(y);
  free_single_1d(z);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_gemver_single(n);

  finalize();

  return 0;
}
