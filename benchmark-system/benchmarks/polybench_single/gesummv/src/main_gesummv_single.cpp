

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>






void
kernel_gesummv_single(long n,
		      DATA_TYPE alpha,
		      DATA_TYPE beta,
		      DATA_TYPE* __restrict__ A,   // n by n
		      DATA_TYPE* __restrict__ B,   // n by n
		      DATA_TYPE* __restrict__ tmp, // n
		      DATA_TYPE* __restrict__ x,   // n
		      DATA_TYPE* __restrict__ y);  // n




void
data_init_gesummv_single(long n,
			 DATA_TYPE* __restrict__  alpha,
			 DATA_TYPE* __restrict__  beta,
			 DATA_TYPE* __restrict__ A,   // n by n
			 DATA_TYPE* __restrict__ B,   // n by n
			 DATA_TYPE* __restrict__ x,   // n
			 int seed)
{
  *alpha = ((DATA_TYPE) n);
  *beta = ((DATA_TYPE) n*2 - 1);
  init_single_2d(n, n, A, seed);
  init_single_2d(n, n, B, seed+1);
  init_single_1d(n, x, seed+2);
}


void
run_gesummv_single(long n)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE* A = malloc_single_2d(n, n);
  DATA_TYPE* B = malloc_single_2d(n, n);
  DATA_TYPE* tmp = malloc_single_1d(n);
  DATA_TYPE* x = malloc_single_1d(n);
  DATA_TYPE* y = malloc_single_1d(n);

  size_t bytes = sizeof(DATA_TYPE) * (2*n*n + 3*n);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tgesummv\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\n", n);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_gesummv_single(n, &alpha, &beta, A, B, x, n);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_gesummv_single(n, alpha, beta, A, B, tmp, x, y);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_single_2d(n, n, A);
  // printf("B\n");
  // print_single_2d(n, n, B);
  // printf("tmp\n");
  // print_single_1d(n, tmp);
  // printf("x\n");
  // print_single_1d(n, x);
  // printf("y\n");
  // print_single_1d(n, y);

  free_single_2d(n, n, A);
  free_single_2d(n, n, B);
  free_single_1d(tmp);
  free_single_1d(x);
  free_single_1d(y);
}


int
main(int vargc, char** vargv)
{
  long n;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 2) ||
      (parse_long(argv[1], 2, LONG_MAX, &n) != 0)) {
    printf("Usage:\n");
    printf("%s [n]\n", argv[0]);
    printf("  2 <= n <= LONG_MAX\n");
    return 1;
  }

  run_gesummv_single(n);

  finalize();

  return 0;
}
