

#include "utilities.hpp"




void
kernel_gesummv_single(long n,
		      DATA_TYPE alpha,
		      DATA_TYPE beta,
		      DATA_TYPE* __restrict__ A,   // n by n
		      DATA_TYPE* __restrict__ B,   // n by n
		      DATA_TYPE* __restrict__ tmp, // n
		      DATA_TYPE* __restrict__ x,   // n
		      DATA_TYPE* __restrict__ y)   // n
{
  long i, j;

  for (i=0; i<n; i++) {
    tmp[i] = (DATA_TYPE) 0.0;
    y[i] = (DATA_TYPE) 0.0;
    for (j=0; j<n; j++) {
      tmp[i] = A[i*n + j] * x[j] + tmp[i];
      y[i] = B[i*n + j] * x[j] + y[i];
    }
    y[i] = alpha * tmp[i] + beta * y[i];
  }
}
