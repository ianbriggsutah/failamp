

#include "utilities.hpp"




void
kernel_2mm_single(long ni, long nj, long nk, long nl,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE* __restrict__ tmp, // ni by nj
		  DATA_TYPE* __restrict__ A,   // ni by nk
		  DATA_TYPE* __restrict__ B,   // nk by nj
		  DATA_TYPE* __restrict__ C,   // nj by nl
		  DATA_TYPE* __restrict__ D)   // ni by nl
{
  long i, j, k;

  /* D := alpha*A*B*C + beta*D */
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      tmp[i*nj + j] = (DATA_TYPE) 0.0;
      for (k=0; k<nk; ++k) {
	tmp[i*nj + j] += alpha * A[i*nk + k] * B[k*nj + j];
      }
    }
  }
  for (i=0; i<ni; i++) {
    for (j=0; j<nl; j++) {
      D[i*nl + j] *= beta;
      for (k=0; k<nj; ++k) {
	D[i*nl + j] += tmp[i*nj + k] * C[k*nl + j];
      }
    }
  }
}
