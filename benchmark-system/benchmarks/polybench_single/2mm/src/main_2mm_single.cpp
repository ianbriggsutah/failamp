

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>






void
kernel_2mm_single(long ni, long nj, long nk, long nl,
		  DATA_TYPE alpha,
		  DATA_TYPE beta,
		  DATA_TYPE* __restrict__ tmp, // ni by nj
		  DATA_TYPE* __restrict__ A,   // ni by nk
		  DATA_TYPE* __restrict__ B,   // nk by nj
		  DATA_TYPE* __restrict__ C,   // nj by nl
		  DATA_TYPE* __restrict__ D);  // ni by nl




void
data_init_2mm_single(long ni, long nj, long nk, long nl,
		     DATA_TYPE* __restrict__ alpha,
		     DATA_TYPE* __restrict__ beta,
		     DATA_TYPE* __restrict__ A, // ni by nk
		     DATA_TYPE* __restrict__ B, // nk by nj
		     DATA_TYPE* __restrict__ C, // nj by nl
		     DATA_TYPE* __restrict__ D, // ni by nl
		     int seed)
{
  *alpha = ((DATA_TYPE) ni);
  *beta = ((DATA_TYPE) nj);
  init_single_2d(ni, nk, A, seed);
  init_single_2d(nk, nj, B, seed+1);
  init_single_2d(nj, nl, C, seed+2);
  init_single_2d(ni, nl, D, seed+3);
}


void
run_2mm_single(long ni, long nj, long nk, long nl)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE alpha;
  DATA_TYPE beta;
  DATA_TYPE* tmp = malloc_single_2d(ni, nj);
  DATA_TYPE* A = malloc_single_2d(ni, nk);
  DATA_TYPE* B = malloc_single_2d(nk, nj);
  DATA_TYPE* C = malloc_single_2d(nj, nl);
  DATA_TYPE* D = malloc_single_2d(ni, nl);

  size_t bytes = sizeof(DATA_TYPE) * (ni*nj + ni*nk + nk*nj + nj*nl + ni*nl);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\t2mm\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\t%ld\t%ld\n", ni, nj, nk, nl);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_2mm_single(ni, nj, nk, nl, &alpha, &beta, A, B, C, D, ni+nj+nk+nl);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_2mm_single(ni, nj, nk, nl, alpha, beta, tmp, A, B, C, D);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("tmp\n");
  // print_single_2d(ni, nj, tmp);
  // printf("A\n");
  // print_single_2d(ni, nk, A);
  // printf("B\n");
  // print_single_2d(nk, nj, B);
  // printf("C\n");
  // print_single_2d(nj, nl, C);
  // printf("D\n");
  // print_single_2d(ni, nl, D);

  free_single_2d(ni, nj, tmp);
  free_single_2d(ni, nk, A);
  free_single_2d(nk, nj, B);
  free_single_2d(nj, nl, C);
  free_single_2d(ni, nl, D);
}


int
main(int vargc, char** vargv)
{
  long ni, nj, nk, nl;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 5) ||
      (parse_long(argv[1], 2, LONG_MAX, &ni) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &nj) != 0) ||
      (parse_long(argv[3], 2, LONG_MAX, &nk) != 0) ||
      (parse_long(argv[4], 2, LONG_MAX, &nl) != 0)) {
    printf("Usage:\n");
    printf("%s [ni nj nk nl]\n", argv[0]);
    printf("  2 <= ni <= LONG_MAX\n");
    printf("  2 <= nj <= LONG_MAX\n");
    printf("  2 <= nk <= LONG_MAX\n");
    printf("  2 <= nl <= LONG_MAX\n");
    return 1;
  }

  run_2mm_single(ni, nj, nk, nl);

  finalize();

  return 0;
}
