

#include "utilities.hpp"




void
kernel_syr2k_single(long n, long m,
		    DATA_TYPE alpha,
		    DATA_TYPE beta,
		    DATA_TYPE* __restrict__ C,   // n by n
		    DATA_TYPE* __restrict__ A,   // n by m
		    DATA_TYPE* __restrict__ B)   // n by m
{
  long i, j, k;

  //BLAS PARAMS
  //UPLO  = 'L'
  //TRANS = 'N'
  //A is NxM
  //B is NxM
  //C is NxN
  for (i=0; i<n; i++) {
    for (j=0; j<=i; j++) {
      C[i*n + j] *= beta;
    }
    for (k=0; k<m; k++) {
      for (j=0; j<=i; j++) {
	C[i*n + j] += A[j*m + k]*alpha*B[i*m + k] + B[j*m + k]*alpha*A[i*m + k];
      }
    }
  }
}
