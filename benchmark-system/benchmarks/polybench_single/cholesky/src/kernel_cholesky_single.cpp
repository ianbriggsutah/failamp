

#include "utilities.hpp"




void
kernel_cholesky_single(long n,
		       DATA_TYPE* __restrict__ A) // n by n
{
  long i, j, k;

  for (i=0; i<n; i++) {
    //j<i
    for (j=0; j<i; j++) {
      for (k=0; k<j; k++) {
	A[i*n + j] -= A[i*n + k] * A[j*n + k];
      }
      A[i*n + j] /= A[j*n + j];
    }
    // i==j case
    for (k=0; k<i; k++) {
      A[i*n + i] -= A[i*n + k] * A[i*n + k];
    }
    A[i*n + i] = SQRT(A[i*n + i]);
  }
}
