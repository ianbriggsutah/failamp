

#include "runtime_utilities.hpp"
#include "utilities.hpp"


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
kernel_doitgen_single(long nr, long nq, long np,
		      DATA_TYPE* __restrict__ A,    // nr by nq by np
		      DATA_TYPE* __restrict__ C4,   // np by np
		      DATA_TYPE* __restrict__ sum); // np




void
data_init_doitgen_single(long nr, long nq, long np,
			 DATA_TYPE* __restrict__ A,  // nr by nq by np
			 DATA_TYPE* __restrict__ C4, // np by np
			 int seed)
{
  init_single_3d(nr, nq, np, A, seed);
  init_single_2d(np, np, C4, seed+1);
}


void
run_doitgen_single(long nr, long nq, long np)
{
  time_series init_time;
  time_series kernel_time;

  init_time.start();
  DATA_TYPE* A = malloc_single_3d(nr, nq, np);
  DATA_TYPE* C4 = malloc_single_2d(np, np);
  DATA_TYPE* sum = malloc_single_1d(np);

  size_t bytes = sizeof(DATA_TYPE) * (nr*nq*np + np*np + np);
  double kb = bytes / 1024;
  double mb = kb / 1024;

  printf("Benchmark:\tdoitgen\n");
  printf("Layout:   \tsingle\n");
  printf("Data_Type:\t%s\n", DATA_TYPE_NAME);
  printf("Input_size:\t%ld\t%ld\t%ld\n", nr, nq, np);
  printf("Memory Footprint:\t%f\n", mb);

  data_init_doitgen_single(nr, nq, np, A, C4, nr+nq+np);
  init_time.end();

  printf("init_time: %1.15e\n", init_time.average());

  kernel_time.start();
  kernel_doitgen_single(nr, nq, np, A, C4, sum);
  kernel_time.end();

  printf("kernel_time: %1.15e\n", kernel_time.average());

  // printf("A\n");
  // print_single_3d(nr, nq, np, A);
  // printf("C4\n");
  // print_single_2d(np, np, C4);
  // printf("sum\n");
  // print_single_1d(np, sum);

  free_single_3d(nr, nq, np, A);
  free_single_2d(np, np, C4);
  free_single_1d(sum);
}


int
main(int vargc, char** vargv)
{
  long nr, nq, np;

  int argc;
  char **argv = init(vargc, vargv, &argc);

  if ((argc != 4) ||
      (parse_long(argv[1], 2, LONG_MAX, &nr) != 0) ||
      (parse_long(argv[2], 2, LONG_MAX, &nq) != 0) ||
      (parse_long(argv[3], 2, LONG_MAX, &np) != 0)) {
    printf("Usage:\n");
    printf("%s [nr nq np]\n", argv[0]);
    printf("  2 <= nr <= LONG_MAX\n");
    printf("  2 <= nq <= LONG_MAX\n");
    printf("  2 <= np <= LONG_MAX\n");
    return 1;
  }

  run_doitgen_single(nr, nq, np);

  finalize();

  return 0;
}
