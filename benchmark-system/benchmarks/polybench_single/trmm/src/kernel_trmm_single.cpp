

#include "utilities.hpp"




void
kernel_trmm_single(long m, long n,
		   DATA_TYPE alpha,
		   DATA_TYPE* __restrict__ A, // m by m
		   DATA_TYPE* __restrict__ B) // m by n
{
  long i, j, k;

  //BLAS parameters
  //SIDE   = 'L'
  //UPLO   = 'L'
  //TRANSA = 'T'
  //DIAG   = 'U'
  // => Form  B := alpha*A**T*B.
  // A is MxM
  // B is MxN
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      for (k=i+1; k<m; k++) {
	B[i*n + j] += A[k*m + i] * B[k*n + j];
      }
      B[i*n + j] = alpha * B[i*n + j];
    }
  }
}
