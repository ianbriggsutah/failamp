

.PHONY: default
default:
	@echo "You must choose a build or run type:"
	@echo "  Build types:"
	@echo "    normal"
	@echo "    failamp"
	@echo "    presage"
	@echo "    normal-smack"
	@echo "    normal-vulfi"
	@echo "    failamp-assert"
	@echo "    failamp-smack"
	@echo "    failamp-vulfi"
	@echo "    presage-vulfi"
	@echo "above may be suffixed with -ir or -asm to build llvm ir or assembly"

FUNCTION_LIST ?= "kernel_${BN}"


SUPPORT=${FAILAMP_POLYBENCH_HOME}/support

UTILITIES=${SUPPORT}/utilities/lib/utilities.o
UTILITIES_INC=${SUPPORT}/utilities/include
CPLUS_INCLUDE_PATH:=${UTILITIES_INC}:${CPLUS_INCLUDE_PATH}

FAILAMP_RUNTIME_INC=${SUPPORT}/failamp_runtime/include

PRESAGE_RUNTIME_INC=${SUPPORT}/presage_runtime/include

VERIFIER_ASSERT=${SUPPORT}/verifier_assert/lib/verifier_assert.o

SMACK_INC=${SMACK_HOME}/share/smack/include

VULFI_INC=${SUPPORT}/vulfi_runtime/include

VULFI_RUNTIME_BC=${SUPPORT}/vulfi_runtime/bytecode/Corrupt.bc


BN=${BENCHMARK_NAME}




.PHONY: normal
normal: bin/normal_main_${BN}

.PHONY: normal-asm
normal-asm: assembly/normal_kernel_${BN}.s

.PHONY: normal-ir
normal-ir: llvm_ir/normal_kernel_${BN}.ll


.PHONY: failamp
failamp: bin/failamp_main_${BN}

.PHONY: failamp-asm
failamp-asm: assembly/failamp_kernel_${BN}.s

.PHONY: failamp-ir
failamp-ir: llvm_ir/failamp_kernel_${BN}.ll


.PHONY: presage
presage: bin/presage_main_${BN}

.PHONY: presage-asm
presage-asm: assembly/presage_kernel_${BN}.s

.PHONY: presage-ir
presage-ir: llvm_ir/presage_kernel_${BN}.ll


.PHONY: normal-smack
normal-smack: bin/normal_smack_main_${BN}

.PHONY: normal-smack-asm
normal-smack-asm: assembly/normal_smack_kernel_${BN}.s

.PHONY: normal-smack-ir
normal-smack-ir: llvm_ir/normal_smack_kernel_${BN}.ll


.PHONY: normal-vulfi
normal-vulfi: bin/normal_vulfi_main_${BN}

.PHONY: normal-vulfi-asm
normal-vulfi-asm: assembly/normal_vulfi_kernel_${BN}.s

.PHONY: normal-vulfi-ir
normal-vulfi-ir: llvm_ir/normal_vulfi_kernel_${BN}.ll


.PHONY: failamp-assert
failamp-assert: bin/failamp_assert_main_${BN}

.PHONY: failamp-assert-asm
failamp-assert-asm: assembly/failamp_assert_kernel_${BN}.s

.PHONY: failamp-assert-ir
failamp-assert-ir: llvm_ir/failamp_assert_kernel_${BN}.ll


.PHONY: failamp-smack
failamp-smack: bin/failamp_smack_main_${BN}

.PHONY: failamp-smack-asm
failamp-smack-asm: assembly/failamp_smack_kernel_${BN}.s

.PHONY: failamp-smack-ir
failamp-smack-ir: llvm_ir/failamp_smack_kernel_${BN}.ll


.PHONY: failamp-vulfi
failamp-vulfi: bin/failamp_vulfi_main_${BN}

.PHONY: failamp-vulfi-asm
failamp-vulfi-asm: assembly/failamp_vulfi_kernel_${BN}.s

.PHONY: failamp-vulfi-ir
failamp-vulfi-ir: llvm_ir/failamp_vulfi_kernel_${BN}.ll


.PHONY: presage-vulfi
presage-vulfi: bin/presage_vulfi_main_${BN}

.PHONY: presage-vulfi-asm
presage-vulfi-asm: assembly/presage_vulfi_kernel_${BN}.s

.PHONY: presage-vulfi-ir
presage-vulfi-ir: llvm_ir/presage_vulfi_kernel_${BN}.ll




# Rules that create kernel bytecode

bytecode/normal_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/normal_kernel_${BN}.bc

bytecode/failamp_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_FAILAMP ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp_failamp_kernel_${BN}.bc
	opt -load LLVMFailAmp.so -failamp ${FAILAMP_OPT_FLAGS} -fn ${FUNCTION_LIST} -mode "detect" bytecode/tmp_failamp_kernel_${BN}.bc -o bytecode/failamp_kernel_${BN}.bc


bytecode/presage_kernel_${BN}.bc: bytecode/normal_kernel_${BN}.bc
	clang++ -DUSE_PRESAGE ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp_presage_kernel_${BN}.bc
	opt -load LLVMPresage.so -presage ${PRESAGE_OPT_FLAGS} -fn ${FUNCTION_LIST} -fl kernel_${BN}.cpp -mode "detect" -bcn ${BN} bytecode/tmp_presage_kernel_${BN}.bc -o bytecode/presage_kernel_${BN}.bc

bytecode/normal_smack_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_SMACK -I${SMACK_INC} ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/normal_smack_kernel_${BN}.bc

bytecode/normal_vulfi_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_VULFI -DINST -I${VULFI_INC} ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp1_normal_vulfi_kernel_${BN}.bc
	llvm-link ${VULFI_RUNTIME_BC} bytecode/tmp1_normal_vulfi_kernel_${BN}.bc -o bytecode/tmp2_normal_vulfi_kernel_${BN}.bc
	opt -load LLVMVulfi.so -vulfi -fn ${FUNCTION_LIST} -fsa "addg" -lang "C++" -arch "x86" -dbgf "fault_sites_data.csv" < bytecode/tmp2_normal_vulfi_kernel_${BN}.bc > bytecode/normal_vulfi_kernel_${BN}.bc

bytecode/failamp_assert_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_FAILAMP ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp_failamp_assert_kernel_${BN}.bc
	opt -load LLVMFailAmp.so -failamp ${FAILAMP_OPT_FLAGS} -fn ${FUNCTION_LIST} -mode "smack" bytecode/tmp_failamp_assert_kernel_${BN}.bc -o bytecode/failamp_assert_kernel_${BN}.bc

bytecode/failamp_vulfi_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_FAILAMP -DUSE_VULFI -DINST ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp1_failamp_vulfi_kernel_${BN}.bc
	opt -load LLVMFailAmp.so -failamp ${FAILAMP_OPT_FLAGS} -fn ${FUNCTION_LIST} -mode "detect" bytecode/tmp1_failamp_vulfi_kernel_${BN}.bc -o bytecode/tmp2_failamp_vulfi_kernel_${BN}.bc
	llvm-link ${VULFI_RUNTIME_BC} bytecode/tmp2_failamp_vulfi_kernel_${BN}.bc -o bytecode/tmp3_failamp_vulfi_kernel_${BN}.bc
	opt -load LLVMVulfi.so -vulfi -fn ${FUNCTION_LIST} -fsa "addg" -lang "C++" -arch "x86" -dbgf "fault_sites_data.csv" < bytecode/tmp3_failamp_vulfi_kernel_${BN}.bc > bytecode/failamp_vulfi_kernel_${BN}.bc

bytecode/failamp_smack_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_FAILAMP -DUSE_SMACK -I${SMACK_INC} ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp_failamp_smack_kernel_${BN}.bc
	opt -load LLVMFailAmp.so -failamp ${FAILAMP_OPT_FLAGS} -fn ${FUNCTION_LIST} -mode "smack" bytecode/tmp_failamp_smack_kernel_${BN}.bc -o bytecode/failamp_smack_kernel_${BN}.bc

bytecode/presage_vulfi_kernel_${BN}.bc: src/kernel_${BN}.cpp | bytecode
	clang++ -DUSE_PRESAGE -DUSE_VULFI -DINST ${TARGET_CXXFLAGS} src/kernel_${BN}.cpp -emit-llvm -c -o bytecode/tmp1_presage_vulfi_kernel_${BN}.bc
	opt -load LLVMPresage.so -presage ${PRESAGE_OPT_FLAGS} -fn ${FUNCTION_LIST} -mode "detect" bytecode/tmp1_presage_vulfi_kernel_${BN}.bc -o bytecode/tmp2_presage_vulfi_kernel_${BN}.bc
	llvm-link ${VULFI_RUNTIME_BC} bytecode/tmp2_presage_vulfi_kernel_${BN}.bc -o bytecode/tmp3_presage_vulfi_kernel_${BN}.bc
	opt -load LLVMVulfi.so -vulfi -fn ${FUNCTION_LIST} -fsa "addg" -lang "C++" -arch "x86" -dbgf "fault_sites_data.csv" < bytecode/tmp3_presage_vulfi_kernel_${BN}.bc > bytecode/presage_vulfi_kernel_${BN}.bc




# Rules that turn bytecode into llvm ir

llvm_ir/normal_kernel_${BN}.ll: bytecode/normal_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/normal_kernel_${BN}.bc -o llvm_ir/normal_kernel_${BN}.ll

llvm_ir/failamp_kernel_${BN}.ll: bytecode/failamp_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/failamp_kernel_${BN}.bc -o llvm_ir/failamp_kernel_${BN}.ll

llvm_ir/presage_kernel_${BN}.ll: bytecode/presage_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/presage_kernel_${BN}.bc -o llvm_ir/presage_kernel_${BN}.ll

llvm_ir/normal_smack_kernel_${BN}.ll: bytecode/normal_smack_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/normal_smack_kernel_${BN}.bc -o llvm_ir/normal_smack_kernel_${BN}.ll

llvm_ir/normal_vulfi_kernel_${BN}.ll: bytecode/normal_vulfi_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/normal_vulfi_kernel_${BN}.bc -o llvm_ir/normal_vulfi_kernel_${BN}.ll

llvm_ir/failamp_assert_kernel_${BN}.ll: bytecode/failamp_assert_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/failamp_assert_kernel_${BN}.bc -o llvm_ir/failamp_assert_kernel_${BN}.ll

llvm_ir/failamp_vulfi_kernel_${BN}.ll: bytecode/failamp_vulfi_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/failamp_vulfi_kernel_${BN}.bc -o llvm_ir/failamp_vulfi_kernel_${BN}.ll

llvm_ir/failamp_smack_kernel_${BN}.ll: bytecode/failamp_smack_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/failamp_smack_kernel_${BN}.bc -o llvm_ir/failamp_smack_kernel_${BN}.ll

llvm_ir/presage_vulfi_kernel_${BN}.ll: bytecode/presage_vulfi_kernel_${BN}.bc | llvm_ir
	llvm-dis bytecode/presage_vulfi_kernel_${BN}.bc -o llvm_ir/presage_vulfi_kernel_${BN}.ll




# Rules that turn bytecode into assembly

assembly/normal_kernel_${BN}.s: bytecode/normal_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/normal_kernel_${BN}.bc -o assembly/normal_kernel_${BN}.s

assembly/failamp_kernel_${BN}.s: bytecode/failamp_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/failamp_kernel_${BN}.bc -o assembly/failamp_kernel_${BN}.s

assembly/presage_kernel_${BN}.s: bytecode/presage_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/presage_kernel_${BN}.bc -o assembly/presage_kernel_${BN}.s

assembly/normal_smack_kernel_${BN}.s: bytecode/normal_smack_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/normal_smack_kernel_${BN}.bc -o assembly/normal_smack_kernel_${BN}.s

assembly/normal_vulfi_kernel_${BN}.s: bytecode/normal_vulfi_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/normal_vulfi_kernel_${BN}.bc -o assembly/normal_vulfi_kernel_${BN}.s

assembly/failamp_assert_kernel_${BN}.s: bytecode/failamp_assert_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/failamp_assert_kernel_${BN}.bc -o assembly/failamp_assert_kernel_${BN}.s

assembly/failamp_vulfi_kernel_${BN}.s: bytecode/failamp_vulfi_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/failamp_vulfi_kernel_${BN}.bc -o assembly/failamp_vulfi_kernel_${BN}.s

assembly/failamp_smack_kernel_${BN}.s: bytecode/failamp_smack_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/failamp_smack_kernel_${BN}.bc -o assembly/failamp_smack_kernel_${BN}.s

assembly/presage_vulfi_kernel_${BN}.s: bytecode/presage_vulfi_kernel_${BN}.bc | assembly
	llc ${LLCFLAGS} bytecode/presage_vulfi_kernel_${BN}.bc -o assembly/presage_vulfi_kernel_${BN}.s




# Rules that create executable binaries (or smack bc files)

bin/normal_main_${BN}: src/main_${BN}.cpp bytecode/normal_kernel_${BN}.bc | bin
	clang++ ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/normal_kernel_${BN}.bc -o bin/normal_main_${BN}

bin/failamp_main_${BN}: src/main_${BN}.cpp bytecode/failamp_kernel_${BN}.bc | bin
	clang++ -DUSE_FAILAMP -I${FAILAMP_RUNTIME_INC} ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/failamp_kernel_${BN}.bc -o bin/failamp_main_${BN}

bin/presage_main_${BN}: src/main_${BN}.cpp bytecode/presage_kernel_${BN}.bc | bin
	clang++ -DUSE_PRESAGE ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/presage_kernel_${BN}.bc -o bin/presage_main_${BN}

bin/normal_smack_main_${BN}: src/main_${BN}.cpp bytecode/normal_smack_kernel_${BN}.bc | bin
	clang++ -DUSE_SMACK ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/normal_smack_kernel_${BN}.bc -emit-llvm -o bin/normal_smack_main_${BN}.bc

bin/normal_vulfi_main_${BN}: src/main_${BN}.cpp bytecode/normal_vulfi_kernel_${BN}.bc | bin
	clang++ -DUSE_VULFI -I${VULFI_INC} -I${FAILAMP_RUNTIME_INC} ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/normal_vulfi_kernel_${BN}.bc -o bin/normal_vulfi_main_${BN}

bin/failamp_assert_main_${BN}: src/main_${BN}.cpp bytecode/failamp_assert_kernel_${BN}.bc | bin
	clang++ -DUSE_FAILAMP -I${FAILAMP_RUNTIME_INC} ${CXXFLAGS} -lm ${VERIFIER_ASSERT} ${UTILITIES} src/main_${BN}.cpp bytecode/failamp_assert_kernel_${BN}.bc -o bin/failamp_assert_main_${BN}

bin/failamp_vulfi_main_${BN}: src/main_${BN}.cpp bytecode/failamp_vulfi_kernel_${BN}.bc | bin
	clang++ -DUSE_FAILAMP -DUSE_VULFI -I${FAILAMP_RUNTIME_INC}  -I${VULFI_INC} ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/failamp_vulfi_kernel_${BN}.bc -o bin/failamp_vulfi_main_${BN}

bin/failamp_smack_main_${BN}: src/main_${BN}.cpp bytecode/failamp_smack_kernel_${BN}.bc | bin
	clang++ -DUSE_FAILAMP -DUSE_SMACK -I${FAILAMP_RUNTIME_INC} ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/failamp_smack_kernel_${BN}.bc -emit-llvm -o bin/failamp_smack_main_${BN}.bc

bin/presage_vulfi_main_${BN}: src/main_${BN}.cpp bytecode/presage_vulfi_kernel_${BN}.bc | bin
	clang++ -DUSE_PRESAGE -DUSE_VULFI -I${FAILAMP_RUNTIME_INC} -I${VULFI_INC} ${CXXFLAGS} -lm ${UTILITIES} src/main_${BN}.cpp bytecode/presage_vulfi_kernel_${BN}.bc -o bin/presage_vulfi_main_${BN}




# Rules that create directories

assembly:
	mkdir -p assembly

bin:
	mkdir -p bin

bytecode:
	mkdir -p bytecode

llvm_ir:
	mkdir -p llvm_ir

obj:
	mkdir -p obj




# Clean

.PHONY: clean
clean:
	$(RM) -r assembly
	$(RM) -r bin
	$(RM) -r bytecode
	$(RM) -r llvm_ir
	$(RM) -r obj
	$(RM) addg_fault_sites_data.csv

