

.PHONY: default
default:
	@echo "You must choose a build or run type:"
	@echo "  Build types:"
	@echo "    normal"
	@echo "    failamp"
	@echo "    presage"
	@echo "    normal-smack"
	@echo "    normal-vulfi"
	@echo "    failamp-assert"
	@echo "    failamp-smack"
	@echo "    failamp-vulfi"
	@echo "    presage-vulfi"
	@echo "above may be suffixed with -ir or -asm to build llvm ir or assembly"


TOPTARGETS := normal normal-asm normal-ir
TOPTARGETS += failamp failamp-asm failamp-ir
TOPTARGETS += presage presage-asm presage-ir
TOPTARGETS += normal-smack normal-smack-asm normal-smack-ir
TOPTARGETS += normal-vulfi normal-vulfi-asm normal-vulfi-ir
TOPTARGETS += failamp-assert failamp-assert-asm failamp-assert-ir
TOPTARGETS += failamp-smack failamp-smack-asm failamp-smack-ir
TOPTARGETS += failamp-vulfi failamp-vulfi-asm failamp-vulfi-ir
TOPTARGETS += presage-vulfi presage-vulfi-asm presage-vulfi-ir
TOPTARGETS += clean

SUBDIRS := $(wildcard */.)

.PHONY: $(TOPTARGETS) $(SUBDIRS)
$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)



SUB_DIRS = $(sort $(dir $(wildcard */)))

