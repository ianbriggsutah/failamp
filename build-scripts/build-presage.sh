#!/bin/bash


# Exit on error
set -e


# Directory this file is in
BUILD_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$( cd "${BUILD_SCRIPT_DIR}" && cd ..  && pwd )"


# Optionally get the install version
if [ $# -eq 0 ]
then
  VERSION=7.0.1
else
    VERSION="$1"
    case "${VERSION}" in
	7.0.1) ;;
	7.0.0) ;;
	6.0.1) ;;
	6.0.0) ;;
	5.0.2) ;;
	5.0.1) ;;
	5.0.0) ;;
	4.0.1) ;;
	4.0.0) ;;
	3.9.1) ;;
	3.9.0) ;;
	3.8.1) ;;
	3.8.0) ;;
	*)
	    echo "Unsupported llvm+Presage version '${VERSION}'"
	    exit 0
	    ;;
    esac

    echo "Building specified version '${VERSION}'"
fi


# Locations
PRESAGE_HOME="${BASE}/Presage"
PRESAGE_SOURCE="${PRESAGE_HOME}/Presage.src"
PRESAGE_BUILD="${PRESAGE_HOME}/Presage-${VERSION}.build"
PRESAGE_INSTALL="${PRESAGE_HOME}/Presage-${VERSION}.install"


# Test for reinstall
if [ -f "${BASE}/activate-presage-${VERSION}" ]
then
  echo "Presage ${VERSION} was already installed"
  echo "To force a reinstall remove ${BASE}/activate-presage-${VERSION}"
  exit 0
fi


# Install llvm
if [ ! -f "${BASE}/activate-llvm-${VERSION}" ]
then
  "${BUILD_SCRIPT_DIR}/build-llvm.sh" "${VERSION}"
fi
LLVM_INSTALL="${BASE}/llvm-versions/llvm-${VERSION}.install"


# Make sure source is there
if [ ! -d "${PRESAGE_SOURCE}" ]
then
  mkdir -p "${PRESAGE_HOME}"
  pushd "${PRESAGE_HOME}"
  git clone https://github.com/utahfmr/PRESAGE.git "${PRESAGE_SOURCE}"
  popd
fi


# Force new build and install
rm -rf "${PRESAGE_BUILD}"
mkdir "${PRESAGE_BUILD}"
rm -rf "${PRESAGE_INSTALL}"
mkdir "${PRESAGE_INSTALL}"


# Configure, Build, and Install
pushd "${PRESAGE_BUILD}"
source "${BASE}/activate-llvm-${VERSION}"
cmake "${PRESAGE_SOURCE}" \
      -DCMAKE_INSTALL_PREFIX="${PRESAGE_INSTALL}" \
      -DCMAKE_PREFIX_PATH="${LLVM_INSTALL}"
make
make install
popd


# Add activation scripts
SRC_SCRIPT="${BUILD_SCRIPT_DIR}/resources/activate_presage_VERSION"
DST_SCRIPT="${BASE}/activate-presage-${VERSION}"
sed "s|%VERSION%|${VERSION}|g" "${SRC_SCRIPT}" > "${DST_SCRIPT}"
cp "${BUILD_SCRIPT_DIR}/resources/deactivate_presage" "${BASE}/deactivate-presage"


echo "To add or remove Presage ${VERSION} from your path use:"
echo "'source ./activate-presage-${VERSION}' and 'source ./deactivate-presage'"

