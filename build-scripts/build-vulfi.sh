#!/bin/bash


# Exit on error
set -e


# Directory this file is in
BUILD_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$( cd "${BUILD_SCRIPT_DIR}" && cd ..  && pwd )"


# Optionally get the install version
if [ $# -eq 0 ]
then
  VERSION=7.0.0
else
    VERSION="$1"
    case "${VERSION}" in
	8.0.1) ;;
	8.0.0) ;;
	7.1.0) ;;
	7.0.1) ;;
	7.0.0) ;;
	6.0.1) ;;
	6.0.0) ;;
	5.0.2) ;;
	5.0.1) ;;
	5.0.0) ;;
	4.0.1) ;;
	4.0.0) ;;
	3.9.1) ;;
	3.9.0) ;;
	3.8.1) ;;
	3.8.0) ;;
	*)
	    echo "Unsupported llvm+Vulfi version '${VERSION}'"
	    exit 0
	    ;;
    esac

    echo "Building specified llvm+vulfi version '${VERSION}'"
fi


# Test for reinstall
if [ -f "${BASE}/activate-vulfi-${VERSION}" ]
then
  echo "Vulfi ${VERSION} was already installed"
  echo "To force a reinstall remove ${BASE}/activate-vulfi-${VERSION}"
  exit 0
fi


# Locations
VULFI_HOME="${BASE}/Vulfi"
VULFI_SOURCE="${VULFI_HOME}/Vulfi.src"
VULFI_BUILD="${VULFI_HOME}/Vulfi_${VERSION}.build"
VULFI_INSTALL="${VULFI_HOME}/Vulfi_${VERSION}.install"


# Install llvm
if [ ! -f "${BASE}/activate-llvm-${VERSION}" ]
then
  "${BUILD_SCRIPT_DIR}/build-llvm.sh" "${VERSION}"
fi
LLVM_INSTALL="${BASE}/llvm-versions/llvm-${VERSION}.install"


# Make sure source is there
if [ ! -d "${VULFI_SOURCE}" ]
then
  mkdir -p "${VULFI_HOME}"
  pushd "${VULFI_HOME}"
  git clone https://github.com/utahfmr/VULFI.git "${VULFI_SOURCE}"
  pushd "${VULFI_SOURCE}"
  patch -p0 < "${BUILD_SCRIPT_DIR}/resources/vulfi.patch"
  popd
  popd
fi


# Force new build and install
rm -rf "${VULFI_BUILD}"
mkdir "${VULFI_BUILD}"
rm -rf "${VULFI_INSTALL}"
mkdir "${VULFI_INSTALL}"


# Configure, Build, and Install
pushd "${VULFI_BUILD}"
source "${BASE}/activate-llvm-${VERSION}"
cmake "${VULFI_SOURCE}" \
      -DCMAKE_INSTALL_PREFIX="${VULFI_INSTALL}" \
      -DCMAKE_PREFIX_PATH="${LLVM_INSTALL}"
make
make install
popd


# Add activation scripts
SRC_SCRIPT="${BUILD_SCRIPT_DIR}/resources/activate_vulfi_VERSION"
DST_SCRIPT="${BASE}/activate-vulfi-${VERSION}"
sed "s|%VERSION%|${VERSION}|g" "${SRC_SCRIPT}" > "${DST_SCRIPT}"
cp "${BUILD_SCRIPT_DIR}/resources/deactivate_vulfi" "${BASE}/deactivate-vulfi"


echo "To add or remove Vulfi ${VERSION} from your path use:"
echo "'source ./activate-vulfi-${VERSION}' and 'source ./deactivate-vulfi'"

