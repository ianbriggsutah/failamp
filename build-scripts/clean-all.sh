#!/bin/bash


# Exit on error
set -e


# Directory this file is in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$( cd "${SCRIPT_DIR}" && cd ..  && pwd )"

cd ${BASE}

rm -f activate-*
rm -f deactivate-*
rm -rf llvm-versions
rm -rf Presage
rm -rf Vulfi
rm -rf Smack
rm -rf Failamp-*
