#!/bin/bash


# Exit on error
set -e


# Directory this file is in
BUILD_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$( cd "${BUILD_SCRIPT_DIR}" && cd ..  && pwd )"


# Optionally get the install version
if [ $# -eq 0 ]
then
  VERSION=7.0.1
else
    VERSION="$1"
    case "${VERSION}" in
	8.0.1) ;;
	8.0.0) ;;
	7.1.0) ;;
	7.0.1) ;;
	7.0.0) ;;
	6.0.1) ;;
	6.0.0) ;;
	5.0.2) ;;
	5.0.1) ;;
	5.0.0) ;;
	4.0.1) ;;
	4.0.0) ;;
	3.9.1) ;;
	3.9.0) ;;
	3.8.1) ;;
	3.8.0) ;;
	*)
	    echo "Unsupported llvm+FailAmp version '${VERSION}'"
	    exit 0
	    ;;
    esac

    echo "Building specified version '${VERSION}'"
fi


# Locations
FAILAMP_HOME="${BASE}/FailAmp"
FAILAMP_SOURCE="${FAILAMP_HOME}/FailAmp.src"
FAILAMP_BUILD="${FAILAMP_HOME}/FailAmp-${VERSION}.build"
FAILAMP_INSTALL="${FAILAMP_HOME}/FailAmp-${VERSION}.install"


# Test for reinstall
if [ -f "${BASE}/activate-failamp-${VERSION}" ]
then
  echo "FailAmp ${VERSION} was already installed"
  echo "To force a reinstall remove ${BASE}/activate-failamp-${VERSION}"
  exit 0
fi


# Install llvm
if [ ! -f "${BASE}/activate-llvm-${VERSION}" ]
then
  "${BUILD_SCRIPT_DIR}/build-llvm.sh" "${VERSION}"
fi
LLVM_INSTALL="${BASE}/llvm-versions/llvm-${VERSION}.install"


# Force new build and install
rm -rf "${FAILAMP_BUILD}"
mkdir "${FAILAMP_BUILD}"
rm -rf "${FAILAMP_INSTALL}"
mkdir "${FAILAMP_INSTALL}"


# Configure, Build, and Install
pushd "${FAILAMP_BUILD}"
source "${BASE}/activate-llvm-${VERSION}"
cmake "${FAILAMP_SOURCE}" \
      -DCMAKE_INSTALL_PREFIX="${FAILAMP_INSTALL}" \
      -DCMAKE_PREFIX_PATH="${LLVM_INSTALL}"
make
make install
popd


# Add activation scripts
SRC_SCRIPT="${BUILD_SCRIPT_DIR}/resources/activate_failamp_VERSION"
DST_SCRIPT="${BASE}/activate-failamp-${VERSION}"
sed "s|%VERSION%|${VERSION}|g" "${SRC_SCRIPT}" > "${DST_SCRIPT}"
cp "${BUILD_SCRIPT_DIR}/resources/deactivate_failamp" "${BASE}/deactivate-failamp"


echo "To add or remove FailAmp ${VERSION} from your path use:"
echo "'source ./activate-failamp-${VERSION}' and 'source ./deactivate-failamp'"

