#!/bin/bash


# Exit on error
set -e


# Directory this file is in
BUILD_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$( cd "${BUILD_SCRIPT_DIR}" && cd ..  && pwd )"


# Test for reinstall
if [ -f ${BASE}/activate-smack ]
then
  echo "Smack was already installed"
  echo "To force a reinstall remove ${BASE}/activate-smack"
  exit 0
fi


# Install llvm
VERSION=3.9.1
if [ ! -f "${BASE}/activate-llvm-${VERSION}" ]
then
  "${BUILD_SCRIPT_DIR}/build-llvm.sh" "${VERSION}"
fi


# Locations
SMACK_HOME="${BASE}/Smack"
SMACK_SOURCE="${SMACK_HOME}/Smack.src"
SMACK_BUILD="${SMACK_HOME}/Smack.build"
SMACK_INSTALL="${SMACK_HOME}/Smack.install"


# Make sure everything was cleaned from prior runs
rm -rf "${SMACK_HOME}"
mkdir -p "${SMACK_SOURCE}"


# Download smack
pushd "${SMACK_HOME}"
git clone https://github.com/smackers/smack.git Smack.src
popd


# Checkout develop branch
pushd "${SMACK_SOURCE}"
git checkout develop
popd


# Build
pushd "${SMACK_SOURCE}/test"
sed -i "s|if timeouts > 0 or failed > 0 or unknowns > 0:|if failed > 0:|g" regtest.py
popd
pushd "${SMACK_SOURCE}/bin"
source "${BASE}/activate-llvm-${VERSION}"
sed -i "s|BUILD_LLVM=1|BUILD_LLVM=0|g" build.sh
sed -i "s|sudo make install|make install|g" build.sh
./build.sh --prefix "${SMACK_INSTALL}"


# Copy activation scripts
cp "${BUILD_SCRIPT_DIR}/resources/activate_smack" "${BASE}/activate-smack"
cp "${BUILD_SCRIPT_DIR}/resources/deactivate_smack" "${BASE}/deactivate-smack"


echo "To add or remove smack from your path use 'source ./[de]activate-smack'"
