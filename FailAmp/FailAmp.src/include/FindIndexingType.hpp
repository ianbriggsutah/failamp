#ifndef FINDINDEXINGTYPE_HPP
#define FINDINDEXINGTYPE_HPP


#include "wrappers/Type.hpp"
#include "wrappers/Value.hpp"

#include <vector>




void
FindIndexingType(llvm::Value* pointer,
		 llvm::Type** indexing_type,
		 std::vector<llvm::Value*>& new_pointers);


#endif // #ifndef FIND_INDEXING_TYPE_HPP
