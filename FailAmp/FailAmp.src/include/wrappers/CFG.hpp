#ifndef WRAPPERS_CFG_HPP
#define WRAPPERS_CFG_HPP


#include "llvm/Config/llvm-config.h"




#if  (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==8)	\
  || (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==9)	\
  || (LLVM_VERSION_MAJOR==4 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==5 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==6 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==1)	\
  || (LLVM_VERSION_MAJOR==8 && LLVM_VERSION_MINOR==0)
#include "llvm/IR/CFG.h"

#else
#warning "Unsuported llvm"
#include "llvm/IR/CFG.h"

#endif


#endif // #ifndef WRAPPERS_CFG_HPP
