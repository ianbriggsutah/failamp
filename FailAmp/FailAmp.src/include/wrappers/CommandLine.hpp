#ifndef WRAPPERS_COMMANDLINE_HPP
#define WRAPPERS_COMMANDLINE_HPP


#include "llvm/Config/llvm-config.h"




#if  (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==8)	\
  || (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==9)	\
  || (LLVM_VERSION_MAJOR==4 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==5 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==6 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==0)	\
  || (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==1)	\
  || (LLVM_VERSION_MAJOR==8 && LLVM_VERSION_MINOR==0)
#include "llvm/Support/CommandLine.h"

#else
#warning "Unsuported llvm"
#include "llvm/Support/CommandLine.h"

#endif


#endif // #ifndef WRAPPERS_COMMANDLINE_HPP
