#ifndef WRAPPERS_MODULE_HPP
#define WRAPPERS_MODULE_HPP


#include "llvm/Config/llvm-config.h"




#if  (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==8)	\
  || (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==9)	\
  || (LLVM_VERSION_MAJOR==4 && LLVM_VERSION_MINOR==0)	\

#include "llvm/IR/Module.h"
#define GET_OR_INSERT_FUNCTION(mod, name, ty1, ty2)	\
  (M).getOrInsertFunction((name), (ty1), (ty2), NULL);

#elif (LLVM_VERSION_MAJOR==5 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==6 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==1)	\
  ||  (LLVM_VERSION_MAJOR==8 && LLVM_VERSION_MINOR==0)

#include "llvm/IR/Module.h"
#define GET_OR_INSERT_FUNCTION(mod, name, ty1, ty2)	\
  (M).getOrInsertFunction((name), (ty1), (ty2));

#else
#warning "Unsuported llvm"
#include "llvm/IR/Module.h"
#define GET_OR_INSERT_FUNCTION(mod, name, ty1, ty2)	\
  (M).getOrInsertFunction((name), (ty1), (ty2));

#endif


#endif // #ifndef WRAPPERS_MODULE_HPP
