#ifndef WRAPPERS_INSTRUCTIONS_HPP
#define WRAPPERS_INSTRUCTIONS_HPP


#include "llvm/Config/llvm-config.h"




#if  (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==8)	\
  || (LLVM_VERSION_MAJOR==3 && LLVM_VERSION_MINOR==9)
#include "llvm/IR/Instructions.h"
#define MOVE_BEFORE(instruction, block, target_instruction)		\
  do { instruction->moveBefore(&*target_instruction); } while(0)

#elif (LLVM_VERSION_MAJOR==4 && LLVM_VERSION_MINOR==0)  \
  ||  (LLVM_VERSION_MAJOR==5 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==6 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==0)	\
  ||  (LLVM_VERSION_MAJOR==7 && LLVM_VERSION_MINOR==1)	\
  ||  (LLVM_VERSION_MAJOR==8 && LLVM_VERSION_MINOR==0)

#include "llvm/IR/Instructions.h"
#define MOVE_BEFORE(instruction, block, target_instruction)		\
  do { instruction->moveBefore(block, target_instruction); } while(0)

#else
#warning "Unsuported llvm"
#include "llvm/IR/Instructions.h"
#define MOVE_BEFORE(instruction, block, target_instruction)		\
  do { instruction->moveBefore(block, target_instruction); } while(0)

#endif


#endif // #ifndef WRAPPERS_INSTRUCTIONS_HPP
