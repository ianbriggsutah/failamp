#ifndef PROCESSDEPENDANTPOINTER_HPP
#define PROCESSDEPENDANTPOINTER_HPP


#include "wrappers/Value.hpp"

#include <vector>
#include <unordered_set>




bool
ProcessDependentPointer(llvm::Value* pointer,
			std::vector<llvm::Value*>& new_pointers,
			std::unordered_set<llvm::BasicBlock*>& error_blocks);


#endif // #ifndef PROCESSDEPENDANTPOINTER_HPP
