#ifndef LOGGING_HPP
#define LOGGING_HPP


#include "wrappers/raw_ostream.hpp"

#include <string>




extern int GLOBAL_LEVEL;
const int ALWAYS = -100;
const int QUIET = -10;
const int NORMAL = 0;
const int VERBOSE = 10;
const int DEBUG = 20;


llvm::raw_ostream& log(int level);

llvm::raw_ostream& p_log(int indent, int level);

llvm::raw_ostream& warn();

void unsupported(const std::string s);

void internal_error(const std::string s);


#endif // #ifndef LOGGING_HPP
