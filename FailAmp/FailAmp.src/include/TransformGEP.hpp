#ifndef TRANSFORMGEP_HPP
#define TRANSFORMGEP_HPP


#include "wrappers/BasicBlock.hpp"
#include "wrappers/Value.hpp"

#include <vector>
#include <unordered_map>




extern int smack_mode;

extern int opt_none;
extern int opt_no_delta;
extern int opt_no_target;
extern int opt_stride;

void
TransformGEP(llvm::BasicBlock* BB,
	     std::unordered_map<llvm::Value*,llvm::Value*> pointer_map,
	     llvm::Value* current_pointer,
	     llvm::Value* current_index,
	     llvm::Value** outgoing_pointer,
	     llvm::Value** outgoing_index,
	     std::vector<llvm::Value*>& new_pointers);


#endif // #ifndef TRANSFORMGEP_HPP
