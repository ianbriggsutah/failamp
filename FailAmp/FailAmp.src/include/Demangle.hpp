#ifndef DEMANGLE_HPP
#define DEMANGLE_HPP


#include "wrappers/StringRef.hpp"

#include <string>




std::string demangle(const llvm::StringRef name);

std::string demangle(const std::string& name);

std::string demangle(const char* name);


#endif // #ifndef DEMANGLE_HPP
