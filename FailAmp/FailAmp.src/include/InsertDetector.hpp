#ifndef INSERTDETECTOR_HPP
#define INSERTDETECTOR_HPP


#include "wrappers/BasicBlock.hpp"
#include "wrappers/Value.hpp"

#include <unordered_set>




void
InsertDetector(llvm::Value* original_ptr,
	       llvm::Value* walked_ptr,
	       llvm::Value* current_index,
	       llvm::BasicBlock* from_BB,
	       llvm::BasicBlock* to_BB,
	       std::unordered_set<llvm::BasicBlock*>& error_blocks);


#endif // #ifndef INSERTDETECTOR_HPP
