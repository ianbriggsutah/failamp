#ifndef COMMANDARGUMENTS_HPP
#define COMMANDARGUMENTS_HPP


#include "wrappers/CommandLine.hpp"

#include <string>




static llvm::cl::opt<std::string>
function_name("fn",
	      llvm::cl::desc("Name of the function(s) to be targeted"),
	      llvm::cl::value_desc("func1 func2 func3"),
	      llvm::cl::init(""),
	      llvm::cl::ValueRequired);

static llvm::cl::opt<std::string>
mode("mode",
     llvm::cl::desc("What mode FailAmp should be in (see README.txt)"),
     llvm::cl::value_desc(""),
     llvm::cl::init("detect"),
     llvm::cl::ValueRequired);

// The '-q' option is already used by opt.
// static llvm::cl::opt<bool>
// quiet("q",
//       llvm::cl::desc("Output only warnings"));
const bool quiet = false;


static llvm::cl::opt<bool>
verbose("v",
	llvm::cl::desc("Verbose output"));

static llvm::cl::opt<bool>
debug("d",
      llvm::cl::desc("Debug output"));


#endif // #ifndef COMMANDARGUMENTS_HPP
