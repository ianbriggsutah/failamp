#ifndef PROCESSARGPOINTER_HPP
#define PROCESSARGPOINTER_HPP


#include "wrappers/BasicBlock.hpp"
#include "wrappers/Value.hpp"

#include <vector>
#include <unordered_set>




bool
ProcessArgPointer(llvm::Value* pointer,
		  llvm::BasicBlock* entry,
		  std::vector<llvm::Value*>& new_pointers,
		  std::unordered_set<llvm::BasicBlock*>& error_blocks);


#endif // #ifndef PROCESSARGPOINTER_HPP
