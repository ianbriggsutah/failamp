

#include "FindIndexingType.hpp"
#include "InsertDetector.hpp"
#include "Logging.hpp"
#include "ProcessDependentPointer.hpp"
#include "TransformGEP.hpp"

#include "wrappers/CFG.hpp"
#include "wrappers/Constants.hpp"
#include "wrappers/Instructions.hpp"

#include <unordered_map>
#include <utility>




using namespace std;
using namespace llvm;


static bool
color_paths(BasicBlock* current_BB,
	   unordered_set<BasicBlock*>& colored_blocks,
	   unordered_map<BasicBlock*,unordered_set<BasicBlock*>>& completed_edges)
{
  bool need_this_block = false;

  // If we reached a colored block this path should be colored
  if (colored_blocks.count(current_BB) == 1) {
    return true;
  }

  // If this is the first time seeing the block
  // make the set for its outgoing connections
  if (completed_edges.count(current_BB) == 0) {
    completed_edges[current_BB] = unordered_set<BasicBlock*>{};
  }

  // Grab outgoing connections set
  auto &end_nodes = completed_edges[current_BB];

  // Look through all outgoing connections
  for (succ_iterator SI = succ_begin(current_BB), E = succ_end(current_BB);
       SI != E;
       ++SI) {
    auto *succ_BB = *SI;
    // If we haven't explored here, recur
    if (end_nodes.count(succ_BB) == 0) {
      end_nodes.insert(succ_BB);
      need_this_block |= color_paths(succ_BB, colored_blocks, completed_edges);
    }
  }

  // We are on a path that needs to be colored
  if (need_this_block) {
    colored_blocks.insert(current_BB);
  }

  return need_this_block;
}


static void
color_paths(BasicBlock* current_BB,
	   unordered_set<BasicBlock*>& colored_blocks)
{
  unordered_map<BasicBlock*,unordered_set<BasicBlock*>> completed_edges;
  color_paths(current_BB, colored_blocks, completed_edges);
}


static void
find_colored_blocks(unordered_set<Value*>& pointers,
		   unordered_set<BasicBlock*>& colored_blocks)
{
  vector<Instruction*> work_stack;
  unordered_set<Instruction*> completed;

  // Fill up work stack from pointer sources
  for (auto *p : pointers) {
    Instruction *inst = cast<Instruction>(p);
    work_stack.push_back(inst);
  }

  // Color all blocks which have the pointer(s) in them (created, used, or phi'd)
  while (!work_stack.empty()) {
    auto *inst = work_stack.back();
    work_stack.pop_back();

    // Skip already processed instructions
    if (completed.count(inst) == 1) {
      continue;
    }

    // This block is needed
    colored_blocks.insert(inst->getParent());

    // Look through all users of this instruction
    for (auto *U : inst->users()) {
      auto *next_inst = cast<Instruction>(U);
      // Add PHIs to the work stack
      auto *phi = dyn_cast<PHINode>(U);
      if (phi != nullptr) {
	work_stack.push_back(next_inst);
      } else {
	// Color end user's blocks
	colored_blocks.insert(inst->getParent());
      }
    }

    completed.insert(inst);
  }

  // From those blocks color all blocks which lay along a path which
  // start at a colored block and leads to a colored block
  size_t old_size = 0;

  // Each iteration of coloring paths may lead to a next generation of paths
  //   that need to be colored. Iterate until no more new paths are found
  while (old_size != colored_blocks.size()) {
    old_size = colored_blocks.size();
    for (auto *BB : colored_blocks) {
      for (succ_iterator SI = succ_begin(BB), E = succ_end(BB); SI != E; ++SI) {
	auto *succ_BB = *SI;

	if (colored_blocks.count(succ_BB) == 0) {
	  color_paths(succ_BB, colored_blocks);
	}
      }
    }
  }
}


static void
find_other_sources(Value* pointer,
		   unordered_set<Value*>& pointers,
		   unordered_set<Value*>& phis,
		   vector<Value*>& new_pointers)
{
  vector<PHINode*> work_stack;
  auto *pointeri = cast<Instruction>(pointer);
  for (auto *U : pointeri->users()) {
    if (auto *phi = dyn_cast<PHINode>(U)) {
      phis.insert(cast<Value>(phi));
      work_stack.push_back(phi);
    }
  }

  unordered_set<Value*> completed;
  unordered_set<PHINode*> worked;
  while (!work_stack.empty()) {
    auto *phi = work_stack.back();
    work_stack.pop_back();

    if (worked.count(phi) == 1) {
      continue;
    }

    // Look forward
    auto *inst = cast<Instruction>(phi);
    for (auto *U : inst->users()) {
      if (auto *phi = dyn_cast<PHINode>(U)) {
    	work_stack.push_back(phi);
      }
    }

    // Look back
    for (unsigned i=0; i<phi->getNumIncomingValues(); i++) {
      auto *val = phi->getIncomingValue(i);

      if ((completed.count(val) == 1) || (pointers.count(val) == 1)) {
	continue;
      }

      // Found a load
      if (LoadInst *load = dyn_cast<LoadInst>(val)) {
	// Have we already found it?
	if (pointers.count(val) == 0) {
	  p_log(4, VERBOSE) << "Found seperate pointer source: " << *val << "\n";

	  // If it was going to be looked at, remove it from that list
	  for (auto it = new_pointers.begin(); it != new_pointers.end(); ) {
	    if (*it == val) {
	      it = new_pointers.erase(it);
	    } else {
	      ++it;
	    }
	  }

	  pointers.insert(val);
	}
      }

      // Grab any PHIs
      if (auto *phi = dyn_cast<PHINode>(val)) {
	phis.insert(val);
	work_stack.push_back(phi);
      }

      completed.insert(val);
    }

    worked.insert(phi);
  }
}


static Value*
get_original(unordered_set<Value*> pointers, BasicBlock* BB)
{
  for (auto *p : pointers) {
    Instruction *inst = cast<Instruction>(p);
    if (inst->getParent() == BB || p->isUsedInBasicBlock(BB)) {
      return p;
    }
  }
  assert(0);
  return nullptr;
}


static bool
isUsedInBasicBlock(unordered_set<Value*> pointers, BasicBlock* BB)
{
  for (auto *p : pointers) {
    if (p->isUsedInBasicBlock(BB)) {
      return true;
    }
  }

  return false;
}


bool
ProcessDependentPointer(Value* pointer,
			vector<Value*>& new_pointers,
			unordered_set<BasicBlock*>& error_blocks)
{
  // First find the pointer and indexing type
  // Note that this pointer may have more than one source
  Type *pointer_type = pointer->getType();
  Type *index_type = nullptr;
  unordered_set<Value*> pointers = {pointer};
  unordered_set<Value*> phis;
  find_other_sources(pointer, pointers, phis, new_pointers);
  for (auto *p : pointers) {
    FindIndexingType(p, &index_type, new_pointers);
  }

  // Initialize pointer map
  unordered_map<Value*,Value*> pointer_map;
  for (auto *p : pointers) {
    pointer_map[p] = ConstantInt::get(index_type, 0);
  }

  // Find all blocks we will need to add PHIs to
  unordered_set<BasicBlock*> start_BBs;
  for (auto *p : pointers) {
    auto *inst = cast<Instruction>(p);
    start_BBs.insert(inst->getParent());
  }
  unordered_set<BasicBlock*> colored_blocks;
  find_colored_blocks(pointers, colored_blocks);

  // Add phis into the pointers we are processing now that start blocks is set
  for (auto *ph : phis) {
    pointers.insert(ph);
  }

  unordered_map<BasicBlock*,PHINode*> phis_for_original;
  unordered_map<BasicBlock*,PHINode*> phis_for_pointers;
  unordered_map<BasicBlock*,PHINode*> phis_for_indices;

  // Add PHIs into all target blocks (PHIs that end up having a single value will be removed later)
  for (auto *current_BB : colored_blocks) {

    // Don't add a PHI to blocks where the pointer(s) are created
    if (start_BBs.count(current_BB) == 1) {
      p_log(4, VERBOSE) << "Skipping PHI addition for starting block: " << current_BB->getName() << "\n";
      continue;
    }

    // Add PHIs for original pointer and index
    auto *original_PHI = PHINode::Create(pointer_type, 0, "original_pointer", current_BB->getFirstNonPHI());
    auto *index_PHI = PHINode::Create(index_type, 0, "incoming_index", current_BB->getFirstNonPHI());
    phis_for_original[current_BB] = original_PHI;
    phis_for_indices[current_BB] = index_PHI;

    // If there is already a phi in place use that instead
    bool found = false;
    for (auto *p : phis) {
      auto *phi = cast<PHINode>(p);
      if (phi->getParent() == current_BB) {
	found = true;
	phis_for_pointers[current_BB] = phi;
	p_log(4, VERBOSE) << "Using inplace PHI: " << *phi << "\n";
	while (phi->getNumIncomingValues() > 0) {
	  phi->removeIncomingValue((unsigned)0, false);
	}
      }
    }
    // Otherwise create one
    if (!found) {
      auto *pointer_PHI = PHINode::Create(pointer_type, 0, "incoming_pointer", current_BB->getFirstNonPHI());
      phis_for_pointers[current_BB] = pointer_PHI;
    }
  }

  // Go through target blocks hooking up PHIs and transforming GEP instructions
  for (auto *current_BB : colored_blocks) {
    p_log(3, VERBOSE) << "Target: " << current_BB->getName() << "\n";

    // Grab PHIs
    auto *original_PHI = phis_for_original[current_BB];
    auto *pointer_PHI = phis_for_pointers[current_BB];
    auto *index_PHI = phis_for_indices[current_BB];

    // Look back and hook up sentinel values for non-target blocks
    for (pred_iterator PI = pred_begin(current_BB), E = pred_end(current_BB); PI != E; ++PI) {
      auto *pred_BB = *PI;

      // Skip the cration blocks and any blocks internal to the colored section
      if (start_BBs.count(current_BB) == 1 || colored_blocks.count(pred_BB) == 1) {
    	continue;
      }

      // While this can occur normally, it may be an indication of a coding error on my part, so log it
      warn() << "Block not covered is incoming, setting to null: " << pred_BB->getName() << "\n";
      assert(original_PHI != nullptr);
      assert(pointer_PHI != nullptr);
      assert(index_PHI != nullptr);
      original_PHI->addIncoming(Constant::getNullValue(pointer_type), pred_BB);
      pointer_PHI->addIncoming(Constant::getNullValue(pointer_type), pred_BB);
      index_PHI->addIncoming(ConstantInt::get(index_type, 0), pred_BB);
    }

    // Do we need to modify the pointer?
    Value *outgoing_original;
    Value *outgoing_pointer;
    Value *outgoing_index;

    // Yes, one of the pointer(s) is used in the block
    if (isUsedInBasicBlock(pointers, current_BB)) {
      Value *p = get_original(pointers, current_BB);

      // ... and it is a creation block
      if (start_BBs.count(current_BB) == 1) {
	outgoing_original = p;
	TransformGEP(current_BB, pointer_map, p, ConstantInt::get(index_type, 0), &outgoing_pointer, &outgoing_index, new_pointers);

	// ... and it's a normal block
      } else {
	outgoing_original = phis_for_original[current_BB];
	TransformGEP(current_BB, pointer_map, cast<Value>(phis_for_pointers[current_BB]), cast<Value>(phis_for_indices[current_BB]), &outgoing_pointer, &outgoing_index, new_pointers);
      }

      // No, this is a creation block so set index to 0 and capture pointers
    } else  if (start_BBs.count(current_BB) == 1) {
      Value *p = get_original(pointers, current_BB);
      outgoing_original = p;
      outgoing_pointer = p;
      outgoing_index = ConstantInt::get(index_type, 0);

      // No, this is a passthrough block
    } else {
      outgoing_original = cast<Value>(original_PHI);
      outgoing_pointer = cast<Value>(pointer_PHI);
      outgoing_index = cast<Value>(index_PHI);
    }

    // Look forward and push outgoing values to the next blocks
    for (succ_iterator SI = succ_begin(current_BB), E = succ_end(current_BB); SI != E; ++SI) {
      auto *succ_BB = *SI;
      // if (error_blocks.count(succ_BB) == 1) {
      // 	continue;
      // }

      // Only push to blocks we have colored that are not creation blocks
      if (colored_blocks.count(succ_BB) == 1 && start_BBs.count(succ_BB) != 1) {
	phis_for_original[succ_BB]->addIncoming(outgoing_original, current_BB);
	phis_for_indices[succ_BB]->addIncoming(outgoing_index, current_BB);
	phis_for_pointers[succ_BB]->addIncoming(outgoing_pointer, current_BB);

	// Exiting our target area, insert detector
      } else {
	InsertDetector(outgoing_original, outgoing_pointer, outgoing_index, current_BB, succ_BB, error_blocks);
      }
    }
  }

  return true;
}
