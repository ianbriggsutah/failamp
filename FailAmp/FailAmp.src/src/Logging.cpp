

#include "Logging.hpp"




using namespace llvm;
using namespace std;


int GLOBAL_LEVEL;


raw_ostream&
log(int level)
{
  if (level <= GLOBAL_LEVEL) {
    errs() << "FailAmp: ";
    return errs();
  } else {
    return nulls();
  }
}


raw_ostream&
p_log(int indent, int level)
{
  if (level <= GLOBAL_LEVEL) {
    errs() << "FailAmp: ";
    for (int i=0; i<indent; i++) {
      errs() << "  ";
    }
    return errs();
  } else {
    return nulls();
  }
}


raw_ostream&
warn()
{
  errs() << "FailAmp: WARNING: ";
  return errs();
}


void
unsupported(const string s)
{
  errs() << "Fatal error, unsupported feature: " << s << "\n";
  exit(0);
}


void
internal_error(const string s)
{
  errs() << "Fatal error, internal error: " << s << "\n";
  exit(0);
}
