

#include "CleanPhis.hpp"
#include "Logging.hpp"

#include "wrappers/BasicBlock.hpp"
#include "wrappers/Instruction.hpp"
#include "wrappers/Instructions.hpp"




using namespace llvm;
using namespace std;


void
CleanPhis(Function &F)
{
  vector<PHINode*> to_erase;
  p_log(0, NORMAL) << "Cleaning unneeded phis\n";

  // Go through each block
  for (BasicBlock &BB : F) {

    // Find the first instruction that cannot be a PHI
    Instruction *stopping_point = BB.getFirstNonPHIOrDbg();

    // Go through the block stopping at the above instruction
    for (Instruction &I : BB) {

      // Look for PHIs
      PHINode *phi = dyn_cast<PHINode>(&I);
      if (phi != nullptr) {

	// with one incoming value
	if (phi->getNumIncomingValues() == 1) {
	  to_erase.push_back(phi);
	}

	// or which do not change within a simple lasso
	if (phi->getNumIncomingValues() == 2) {
	  auto* BB_0 = phi->getIncomingBlock(0);
	  auto* phi_0 = phi->getIncomingValue(0);
	  auto* BB_1 = phi->getIncomingBlock(1);
	  auto* phi_1 = phi->getIncomingValue(1);
	  if ((BB_0 == &BB && phi_0 == phi) || (BB_1 == &BB && phi_1 == phi)) {
	    to_erase.push_back(phi);
	  }
	}
      }

      // Stop if we are through the PHI section
      if (&I == stopping_point) {
	break;
      }
    }
  }

  // Replace the PHI uses with the single value they represent
  for (PHINode* phi : to_erase) {
    p_log(1, VERBOSE) << *phi << "\n";
    assert(phi->getNumIncomingValues() <= 2);

    if (phi->getNumIncomingValues() == 0) {
      phi->eraseFromParent();
      continue;
    }

    Value *val;
    if (phi->getNumIncomingValues() == 2) {
      if (phi->getIncomingValue(0) == phi) {
	val = phi->getIncomingValue(1);
      } else {
	val = phi->getIncomingValue(0);
      }
    } else {
      val = phi->getIncomingValue(0);
    }

    phi->replaceAllUsesWith(val);
    phi->eraseFromParent();
  }
}
