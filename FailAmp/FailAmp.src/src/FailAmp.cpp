

#include <sstream>
#include <string>
#include <unordered_set>

#include "wrappers/Function.hpp"
#include "wrappers/Module.hpp"
#include "wrappers/Pass.hpp"
#include "wrappers/Type.hpp"

#include "CommandArguments.hpp"
#include "Demangle.hpp"
#include "Logging.hpp"
#include "ProcessFunction.hpp"
#include "TransformGEP.hpp"




using namespace llvm;
using namespace std;


int smack_mode;
int opt_none;
int opt_no_delta;
int opt_no_target;
int opt_stride;


namespace {
  struct FailAmp : public FunctionPass {
    static char ID;
    static unordered_set<string> seen_functions;
    static unordered_set<string> target_functions;
    FailAmp() : FunctionPass(ID) {}

    bool doInitialization(Module &M) override {

      // Create raise function per POSIX.
      // Note: this is lacking attributes.
      LLVMContext& context = M.getContext();
      Constant *raise_c = GET_OR_INSERT_FUNCTION(M,
						 "raise",
						 Type::getInt32Ty(context),
						 Type::getInt32Ty(context));
      assert(raise_c != nullptr);

      // What mode are we in?
      if (mode == "detect") {
	smack_mode = 0;
      } else if (mode == "smack" || mode == "assert") {
	smack_mode = 1;
	Constant *smack_c = GET_OR_INSERT_FUNCTION(M,
						   "__VERIFIER_assert",
						   Type::getVoidTy(context),
						   Type::getInt32Ty(context));
	assert(smack_c != nullptr);
      } else {
	unsupported("mode must be one of {detect smack assert}");
      }

      // Break argument along spaces to get multiple function names
      stringstream ss(function_name);
      string buf;
      while (ss >> buf) {
	target_functions.insert(buf);
      }

      // Log level
      GLOBAL_LEVEL = NORMAL;
      if (quiet)   { GLOBAL_LEVEL = QUIET;   }
      if (verbose) { GLOBAL_LEVEL = VERBOSE; }
      if (debug)   { GLOBAL_LEVEL = DEBUG;   }

      // Log
      p_log(0, VERBOSE) << "Setup:\n";
      p_log(1, VERBOSE) << "Targets:\n";
      for (const string &s : target_functions) {
	p_log(2, VERBOSE) << s << '\n';
      }
      p_log(0, NORMAL) << "Running:\n";

      opt_none = 0;
      opt_no_delta = 0;
      opt_no_target = 0;
      opt_stride = 0;

      return false;
    }


    bool runOnFunction(Function &F) override {

      // Get the function name and see if we need to process it
      string real_name = demangle(F.getName());
      seen_functions.insert(real_name);
      if (target_functions.count(real_name) != 0) {
      	p_log(1, VERBOSE) << "Found: " << real_name << '\n';
      } else {
	p_log(1, VERBOSE) << "Seen: " << real_name << '\n';
	return false;
      }

      // Process target function
      p_log(2, DEBUG) << "Before:\n" << F << "\n";
      bool modified = ProcessFunction(F);

      return modified;
    }


    bool doFinalization(Module &M) override {

      // No teardown actually needed, just logging
      p_log(0, VERBOSE) << "Teardown:\n";
      for (const string &s : target_functions) {
	if (seen_functions.count(s) != 0) {
	  p_log(1, VERBOSE) << "Found: " << s << '\n';
	} else {
	  warn() << "MISSING: " << s << '\n';
	}
      }

      p_log(0, NORMAL) << "GEP Transform Counts\n";
      p_log(0, NORMAL) << "None\t" << opt_none << "\n";
      p_log(0, NORMAL) << "No Delta\t" << opt_no_delta << "\n";
      p_log(0, NORMAL) << "No target\t" << opt_no_target << "\n";
      p_log(0, NORMAL) << "Stride\t" << opt_stride << "\n";

      return false;
    }

  };
}


char FailAmp::ID = 0;
unordered_set<string> FailAmp::seen_functions = unordered_set<string>();
unordered_set<string> FailAmp::target_functions = unordered_set<string>();
static RegisterPass<FailAmp> X("failamp", "structured address protection");
