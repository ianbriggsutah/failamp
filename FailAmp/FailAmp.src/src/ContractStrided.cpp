

#include "ContractStrided.hpp"
#include "TransformGEP.hpp"
#include "Logging.hpp"

#include "wrappers/BasicBlock.hpp"
#include "wrappers/CFG.hpp"
#include "wrappers/Instruction.hpp"
#include "wrappers/Instructions.hpp"

#include <unordered_map>




using namespace llvm;
using namespace std;


static Value*
find_diff(BasicBlock* pred, Value* ap, Value* bp)
{
  Value* a = ap;
  if (auto* app = dyn_cast<PHINode>(ap)) {
    if ( app->getBasicBlockIndex(pred) != -1) {
      a = app->getIncomingValueForBlock(pred);
    }
  }

  Value* b = bp;
  if (auto* bpp = dyn_cast<PHINode>(bp)) {
    if (bpp->getBasicBlockIndex(pred) != -1) {
      b = bpp->getIncomingValueForBlock(pred);
    }
  }

  p_log(0, NORMAL) << "Diff on pred" << pred->getName() << "\n" << *a << "\nand\n" << *b << "\n";

  // Both are constant
  auto* ac = dyn_cast<ConstantInt>(a);
  auto* bc = dyn_cast<ConstantInt>(b);
  if (ac != nullptr && bc != nullptr) {
    auto av = ac->getValue();
    auto bv = bc->getValue();
    APInt diff = av;
    diff -= bv;
    p_log(0, NORMAL) << "Contracted\n\n";
    return ConstantInt::get(a->getContext(), diff);
  }

  // b this time equals a last time, so difference is how a changed
  if (b == ap) {
    auto ai = cast<Instruction>(a);
    if (ai->getOpcode() == Instruction::Add
	&& ai->getOperand(0) == ap) {
      p_log(0, NORMAL) << "Contracted\n\n";
      return ai->getOperand(1);
    }
    if (ai->getOpcode() == Instruction::Add
	&& ai->getOperand(1) == ap) {
      p_log(0, NORMAL) << "Contracted\n\n";
      return ai->getOperand(0);
    }
  }

  p_log(0, NORMAL) << "Failed\n\n";
  return nullptr;
}




static bool
find_simple_stride(BasicBlock* BB, Value* a, Value* b,
		   unordered_map<BasicBlock*, Value*>& pred_map)
{
  p_log(0, NORMAL) << "Trying\n" << *a << "\nand\n" << *b << "\n\n";
  for (auto it = pred_begin(BB), et = pred_end(BB); it != et; ++it) {
    BasicBlock* predecessor = *it;
    auto* diff = find_diff(predecessor, a, b);
    if (diff == nullptr) {
        p_log(0, NORMAL) << "Failed\n\n\n\n";
      return false;
    }
    pred_map[predecessor] = diff;
  }

  p_log(0, NORMAL) << "Contracted\n\n\n\n";
  return true;
}




bool
ContractStrided(Function &F)
{
  p_log(0, NORMAL) << "Finding strided access\n";
  bool ever_modified = false;
  bool modified;

  do {
    modified = false;

    // Go through each block
    for (BasicBlock &BB : F) {

      // Go through the block
      for (Instruction &I : BB) {

	// Look for Subtracts
	if (I.getOpcode() != Instruction::Sub) {
	  continue;
	}

	unordered_map<BasicBlock*, Value*> pred_map;
	bool strided = find_simple_stride(&BB, I.getOperand(0), I.getOperand(1),
					  pred_map);

	if (!strided) {
	  continue;
	}

	opt_no_target -= 1;
	opt_stride += 1;

	auto* stride_phi = PHINode::Create(I.getType(),
					   0,
					   "stride",
					   BB.getFirstNonPHI());

	for (pair<BasicBlock*, Value*> el : pred_map) {
	  stride_phi->addIncoming(el.second, el.first);
	}

	I.replaceAllUsesWith(stride_phi);
	I.eraseFromParent();
	modified = true;
	ever_modified = true;
	break;
      }

      if (modified) {
	break;
      }
    }

    if (modified) {
      break;
    }
  } while (modified);

  return ever_modified;
}
