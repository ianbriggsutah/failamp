

#include "Logging.hpp"
#include "TransformGEP.hpp"

#include "wrappers/CFG.hpp"
#include "wrappers/Constants.hpp"
#include "wrappers/InstrTypes.hpp"
#include "wrappers/Instructions.hpp"
#include "wrappers/Module.hpp"

#include <csignal>
#include <unordered_set>
#include <unordered_map>
#include <utility>




using namespace llvm;
using namespace std;


// static Value*
// find_diff(BasicBlock* pred, Value* ap, Value* bp)
// {
//   p_log(0, NORMAL) << "Diff\n" << *ap << "\n  and\n" << *bp << "\n";
//   Value* a = ap;
//   if (auto* app = dyn_cast<PHINode>(ap)) {
//     if ( app->getBasicBlockIndex(pred) != -1) {
//       a = app->getIncomingValueForBlock(pred);
//     }
//   }

//   Value* b = bp;
//   if (auto* bpp = dyn_cast<PHINode>(bp)) {
//     if (bpp->getBasicBlockIndex(pred) != -1) {
//       b = bpp->getIncomingValueForBlock(pred);
//     } else {
//       b = ap;
//     }
//   }

//   // Both are constant
//   auto* ac = dyn_cast<ConstantInt>(a);
//   auto* bc = dyn_cast<ConstantInt>(b);
//   if (ac != nullptr && bc != nullptr) {
//     auto av = ac->getValue();
//     auto bv = bc->getValue();
//     APInt diff = av;
//     diff -= bv;
//     return ConstantInt::get(a->getContext(), diff);
//   }

//   // b this time equals a last time, so difference is how a changed
//   if (b == ap) {
//     auto ai = cast<Instruction>(a);
//     if (ai->getOpcode() == Instruction::Add
// 	&& ai->getOperand(0) == ap) {
//       return ai->getOperand(1);
//     }
//     if (ai->getOpcode() == Instruction::Add
// 	&& ai->getOperand(1) == ap) {
//       return ai->getOperand(0);
//     }
//   }

//   return nullptr;
// }



// static bool
// find_simple_stride(BasicBlock* BB, Value* a, Value* b,
// 		   unordered_map<BasicBlock*, Value*>& pred_map)
// {
//   for (auto it = pred_begin(BB), et = pred_end(BB); it != et; ++it) {
//     BasicBlock* predecessor = *it;
//     auto* diff = find_diff(predecessor, a, b);
//     if (diff == nullptr) {
//       return false;
//     }
//     pred_map[predecessor] = diff;
//   }

//   return true;
// }


static bool
is_pointer_in_map(unordered_map<Value*,Value*> pointer_map, Value* pointer)
{
  for (auto pair : pointer_map) {
    Value* ptr = pair.first;
    if (ptr == pointer) {
      return true;
    }
  }
  return false;
}




void
TransformGEP(BasicBlock* BB,
	     unordered_map<Value*,Value*> pointer_map,
	     Value* current_pointer,
	     Value* current_index,
	     Value** outgoing_pointer,
	     Value** outgoing_index,
	     vector<Value*> &new_pointers)
{
  // Keep track of the GEPs that we make, so we don't re-transform ourselfs
  unordered_set<GetElementPtrInst*> created;

  // keeps track of when we modify/need a restart
  bool done = false;

  // Modifying invalidates the iterator, so we must restart from the top of the
  // block when we modify
  while (!done) {
    done = true;

    // Go through the block from the top
    for (Instruction &instr : *BB) {
      auto *inst = &instr;

      // Is it a GEP? That we haven't created ourselfs?
      auto *gep = dyn_cast<GetElementPtrInst>(inst);
      if ((gep == nullptr) || (created.count(gep) == 1)) {
	continue;
      }

      // Grab instruction, pointer from instruction, and index
      auto *gepi = cast<Instruction>(gep);
      Value* pointer_operand = gep->getPointerOperand();
      Value* index_operand = *gep->idx_begin();

      // That uses this pointer?
      if (!is_pointer_in_map(pointer_map, pointer_operand)) {
	continue;
      }

      p_log(3, VERBOSE) << "Transforming GEP in block: '"
			<< BB->getName()
			<< "' ("
			<< *gep
			<< ")\n";

      // coming into the block we have the invariant that
      //   current_pointer == GEP(original_pointer, current_index)
      // and we encounter a GEP of the form
      //   q = GEP(p, o)
      // where p is either the original_pointer or was calculated
      // as a chain of GEPs of the original_pointer
      //
      // first we must know what the absolute offset from the original_pointer
      // this pointer_operand is, we get this from our
      // pointer_map(pointer->offset) and holds the invariant that
      //   pointer == GEP(original_pointer, offset)
      // so we get a value, absolute_offset,  with this invariant
      //   p == GEP(original_pointer, absolute_offset)
      //
      // then we need to calulate what the absolute offset for q will be, called
      // the target_index
      //   target_index = absolute_offset + o
      //
      // since we know this absolute offset, and the current_index we can
      // calculate the difference between
      //   delta_index = target_index - current_index
      //
      // then the GEP can be replaced
      // q = GEP(current_pointer, delta_index)
      //
      // and from then on our current_pointer is q, and our current_index is
      // target_index
      //
      // So we turn
      //   q = GEP(p, o)
      // into
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = GEP(current_pointer, delta_index)
      //
      //
      // If we translate this into c-syntax the reasoning for correctness is more
      // clear.
      //   q = p + o
      // where q and p are pointers and o is an integer
      // we know
      //   current_pointer == original_pointer + current_index
      //   p == original_pointer + absolute_offset
      // so we can rewrite q
      //   q = original_pointer + absolute_offset + o
      // and pull out the integers
      //   target_index = absolute_offset + o
      //   q = original_pointer + target_index
      // then add zero in the form current_index - current_index
      //   target_index = absolute_offset + o
      //   q = original_pointer + current_index - current_index + target_index
      // substitute current pointer
      //   target_index = absolute_offset + o
      //   q = current_pointer - current_index + target_index
      // pull out the integer math
      //   target_index = absolute_offset + o
      //   delta_index = - current_index + target_index
      //   q = current_pointer + target_index
      // and turn the addition fo a negative into a subtraction
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      //
      // to show that q in the last step is equal to the q we started with we
      // can substitute in the invariant for current_pointer
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = original_pointer + current_index + delta_index
      // then inplace delta_index
      //   target_index = absolute_offset + o
      //   q = original_pointer + current_index + target_index - current_index
      // cancel out current_index
      //   target_index = absolute_offset + o
      //   q = original_pointer + target_index
      // inplace target_index
      //   q = original_pointer + absolute_offset + o
      // use the invariant for p
      //   q = p + o
      // and we have our original q
      //
      //
      // in some cases this calculation can be simplified
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      //
      // if absolute_offset == 0
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      // then replace absolute_offset
      //   target_index = 0 + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      // simplify
      //   target_index = o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      // inplace target_index
      //   delta_index = o - current_index
      //   q = current_pointer + target_index
      //
      //
      // if current_index == absolute_offset then this simplification occurs
      //   target_index = absolute_offset + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      // replace absolute_offset with current_index
      //   target_index = current_index + o
      //   delta_index = target_index - current_index
      //   q = current_pointer + target_index
      // inplace target_index
      //   delta_index = current_index + o - current_index
      //   q = current_pointer + target_index
      // cancel current_index
      //   delta_index = o
      //   q = current_pointer + target_index
      // inplace delta_index
      //   q = current_pointer + o

      // First get the absolute offset of the pointer operand and
      // find out if absolute_offset == 0
      Value* absolute_offset = pointer_map[pointer_operand];
      bool is_original_base = false;
      ConstantInt* absolute_int = dyn_cast<ConstantInt>(absolute_offset);
      if (absolute_int != NULL) {
	is_original_base = absolute_int->isZero();
      }


      Value* target_index;
      Value* delta_index;
      if (current_index == absolute_offset) {
	// If the current pointer is at the absolute_offset we only
	// need to compute the target_index
	opt_no_target += 1;
	target_index = BinaryOperator::Create(Instruction::BinaryOps::Add,
					      absolute_offset,
					      index_operand,
					      "target_index",
					      gepi);
	delta_index = index_operand;
      } else if (is_original_base) {
	// Otherwise, if the pointer operand is the original pointer we only
	// need to compute the delta
	target_index = index_operand;

	// // if there is a stride present use that for the delta index
	// unordered_map<BasicBlock*, Value*> pred_map;
	// bool strided = find_simple_stride(BB, target_index, current_index,
	// 				  pred_map);
	// if (strided) {
	//   opt_stride += 1;

	//   auto* stride_phi = PHINode::Create(target_index->getType(),
	// 				     0,
	// 				     "stride",
	// 				     BB->getFirstNonPHI());

	//   for (pair<BasicBlock*, Value*> el : pred_map) {
	//     stride_phi->addIncoming(el.second, el.first);
	//   }

	//   delta_index = stride_phi;
	// } else {
	  opt_no_target += 1;
	  delta_index = BinaryOperator::Create(Instruction::BinaryOps::Sub,
					       target_index,
					       current_index,
					       "delta_index",
					       gepi);
	  //	}
      } else {
	// Otherwise, we have to compute both
	opt_none += 1;
	target_index = BinaryOperator::Create(Instruction::BinaryOps::Add,
					      absolute_offset,
					      index_operand,
					      "target_index",
					      gepi);
	delta_index = BinaryOperator::Create(Instruction::BinaryOps::Sub,
					     target_index,
					     current_index,
					     "delta_index",
					     gepi);

      }

      // Insert the new gep
      vector<Value*> delta_idx_holder{delta_index};
      ArrayRef<Value*> indices{delta_idx_holder};
      Type* res_type = gep->getResultElementType();
      assert(res_type != nullptr);
      assert(current_pointer != nullptr);
      assert(indices.size() == 1);
      assert(indices[0] != nullptr);
      assert(gepi != nullptr);
      GetElementPtrInst *new_pointer = GetElementPtrInst::Create(res_type,
								 current_pointer,
								 indices,
								 "new_pointer",
								 gepi);

      // Put our GEP in place
      gep->replaceAllUsesWith(new_pointer);

      // update the pointer map and creation set
      pointer_map[new_pointer] = target_index;
      created.insert(new_pointer);

      // update current pointer and index
      current_index = target_index;
      current_pointer = new_pointer;

      // Either remove the old gep or keep it for an assert
      if (smack_mode) {
	Module *mod = BB->getModule();
	Constant *smack_assert_c = mod->getFunction("__VERIFIER_assert");
	Function *smack_assert = cast<Function>(smack_assert_c);
	CmpInst *compare = CmpInst::Create(Instruction::ICmp,
					   CmpInst::ICMP_EQ,
					   new_pointer,
					   gep,
					   "compare");
	compare->insertAfter(gep);
	Type* smack_type = smack_assert->arg_begin()->getType();
	CastInst *compare_int = CastInst::CreateIntegerCast(compare,
							    smack_type,
							    false,
							    "compare_int");
	compare_int->insertAfter(compare);
	vector<Value*> vec = {compare_int};
	ArrayRef<Value*> smack_assert_args(vec);
	CallInst *smack_assert_call = CallInst::Create(smack_assert,
						       smack_assert_args,
						       "");
	smack_assert_call->insertAfter(compare_int);
	DebugLoc deb = gepi->getDebugLoc();
	if (deb) {
	  smack_assert_call->setDebugLoc(deb);
	}

	// lie about creating the old gep so we don't re-relativize it
	created.insert(gep);
      } else {

	// Remove the old GEP
	gep->eraseFromParent();
      }

      p_log(4, VERBOSE) << "into" << " (" << *new_pointer << ")\n";

      // See if this gep is loaded to create a new pointer
      auto val = cast<Value>(new_pointer);
      vector<Value*> work_stack = {val};
      unordered_set<Value*> completed;
      while (!work_stack.empty()) {
	Value *current = work_stack.back();
	work_stack.pop_back();

	// Skip over values already processed
	if (completed.count(current) == 1) {
	  continue;
	}

	for (auto *U : current->users()) {
	  // See if this user creates a new pointer to track
	  auto* load = dyn_cast<LoadInst>(U);
	  if ((load != nullptr) && (load->getType()->isPointerTy())) {
	    p_log(4, VERBOSE) << "Found dependent pointer from GEP: "
			      << *load
			      << "\n";
	    new_pointers.push_back(cast<Value>(load));
	  }

	  // Traverse PHIs
	  if (auto *phi = dyn_cast<PHINode>(U)) {
	    work_stack.push_back(U);
	  }
	}

	completed.insert(current);
      }

      // There may be more than one user of the target pointer in a basic block,
      // restart
      done = false;
      break;
    }
  }

  // The inout arguments get the final reletave pointer and index
  *outgoing_pointer = current_pointer;
  *outgoing_index = current_index;
}

