

#include "CleanPhis.hpp"
#include "ContractStrided.hpp"
#include "Demangle.hpp"
#include "Logging.hpp"
#include "ProcessArgPointer.hpp"
#include "ProcessDependentPointer.hpp"
#include "ProcessFunction.hpp"

#include "wrappers/BasicBlock.hpp"
#include "wrappers/Value.hpp"

#include <vector>




using namespace llvm;
using namespace std;


bool
ProcessFunction(Function &F)
{
  p_log(0, NORMAL) << "Processing " << demangle(F.getName()) << "\n";

  if (F.doesNotAccessMemory()) {
    p_log(0, NORMAL) << "Function "
		     << demangle(F.getName())
		     << " does not access memory\n";
    return false;
  }

  if (F.isVarArg()) {
    warn() << "Function "
	   << demangle(F.getName())
	   << " is vararg, not currently handled\n";
    return false;
  }

  bool modified = false;
  BasicBlock *entry = &(F.getEntryBlock());
  vector<Value*> new_pointers;
  unordered_set<BasicBlock*>  error_blocks;

  // Get all argument pointers and process them
  // Each may find dependent pointers and add them to the new_pointers list for
  // later processing.
  for (auto &arg : F.args()) {
    if (arg.getType()->isPointerTy()) {
      p_log(2, VERBOSE) << "\n\nProcessing argument pointer: " << arg << "\n";
      Value *pointer = cast<Value>(&arg);
      modified |= ProcessArgPointer(pointer, entry, new_pointers, error_blocks);
    }
  }

  // Process the list.
  // Dependent pointers may still be found and added to the list. Hence we cannot
  // use an iterator.
  while (!new_pointers.empty()) {
    Value *pointer = new_pointers.back();
    assert(pointer != nullptr);
    new_pointers.pop_back();
    p_log(2, VERBOSE) << "\n\nProcessing dependent pointer: "
		      << *pointer
		      << "\n";
    modified |= ProcessDependentPointer(pointer, new_pointers, error_blocks);
  }

  // There is alot of PHI bloat that occurs. Remove useless PHIs.
  CleanPhis(F);

  // Find strided access
  modified |= ContractStrided(F);

  // There is alot of PHI bloat that occurs. Remove useless PHIs.
  CleanPhis(F);

  return modified;
}
