

#include "FindIndexingType.hpp"
#include "InsertDetector.hpp"
#include "Logging.hpp"
#include "ProcessArgPointer.hpp"
#include "TransformGEP.hpp"

#include "wrappers/CFG.hpp"
#include "wrappers/Constants.hpp"
#include "wrappers/Instruction.hpp"
#include "wrappers/Instructions.hpp"
#include "wrappers/Type.hpp"
#include "wrappers/Value.hpp"

#include <unordered_map>
#include <unordered_set>




using namespace llvm;
using namespace std;


static bool
any_used_in_basic_block(unordered_map<Value*,Value*> &pointer_map,
			BasicBlock* current_BB)
{
  for (auto pair : pointer_map) {
    auto ptr = pair.first;
    if (ptr->isUsedInBasicBlock(current_BB)) {
      return true;
    }
  }
  return false;
}


bool
ProcessArgPointer(Value *pointer,
		  BasicBlock *entry,
		  vector<Value*> &new_pointers,
		  unordered_set<BasicBlock*> &error_blocks)
{
  assert(pointer != nullptr);
  assert(entry != nullptr);

  // First find the pointer and indexing type
  Type *pointer_type = pointer->getType();
  assert(pointer_type != nullptr);
  Type *index_type = nullptr;
  FindIndexingType(pointer, &index_type, new_pointers);

  // See if this pointer is indexed
  if (index_type == nullptr) {
    p_log(3, VERBOSE) << "Pointer never used in a GEP\n";
    return false;
  }

  // Initialize pointer map
  unordered_map<Value*,Value*> pointer_map =
    {{pointer, ConstantInt::get(index_type, 0)}, };

  // Now traverse all blocks flowing the pointer and index
  vector<BasicBlock*> work_stack = {entry};
  unordered_set<BasicBlock*> completed = {entry};
  unordered_map<BasicBlock*,PHINode*> phis_for_pointers;
  unordered_map<BasicBlock*,PHINode*> phis_for_indices;
  unordered_map<BasicBlock*,Value*> outgoing_pointers = {{entry, pointer}, };
  unordered_map<BasicBlock*,Value*> outgoing_indices =
    {{entry, ConstantInt::get(index_type, 0)}, };

  while (!work_stack.empty()) {

    // New block
    BasicBlock *current_BB = work_stack.back();
    assert(current_BB != nullptr);
    work_stack.pop_back();

    // Do we need to modify the pointer?
    if (any_used_in_basic_block(pointer_map, current_BB)) {

      // Grab what is coming into this block
      Value* incoming_pointer;
      Value* incoming_index;
      if (current_BB == entry) {
	incoming_pointer = pointer;
	incoming_index = ConstantInt::get(index_type, 0);
      } else {
	assert(phis_for_pointers[current_BB] != nullptr);
	assert(phis_for_indices[current_BB] != nullptr);
	incoming_pointer = cast<Value>(phis_for_pointers[current_BB]);
	incoming_index = cast<Value>(phis_for_indices[current_BB]);
      }

      // Transform and get what comes out of the block
      Value* outgoing_pointer;
      Value* outgoing_index;
      TransformGEP(current_BB, pointer_map, incoming_pointer, incoming_index,
		   &outgoing_pointer, &outgoing_index, new_pointers);
      assert(outgoing_pointer != nullptr);
      assert(outgoing_index != nullptr);
      outgoing_pointers[current_BB] = outgoing_pointer;
      outgoing_indices[current_BB] = outgoing_index;
      pointer_map[outgoing_pointer] = outgoing_index;
      // No modification, place outgoing values for passthrough blocks
    } else if (outgoing_pointers.count(current_BB) == 0) {
      assert(outgoing_indices.count(current_BB) == 0);
      assert(phis_for_pointers[current_BB] != nullptr);
      assert(phis_for_indices[current_BB] != nullptr);

      outgoing_pointers[current_BB] = cast<Value>(phis_for_pointers[current_BB]);
      outgoing_indices[current_BB] = cast<Value>(phis_for_indices[current_BB]);
    }

    // Look at all outgoing connections
    bool exit_node = true;
    for (succ_iterator SI = succ_begin(current_BB), E = succ_end(current_BB);
	 SI != E;
	 ++SI) {
      auto *succ_BB = *SI;
      exit_node = false;

      // Skip error blocks
      if (error_blocks.count(succ_BB) == 1) {
	continue;
      }

      // We will need to process this block later
      if (completed.count(succ_BB) == 0) {
	completed.insert(succ_BB);
	work_stack.push_back(succ_BB);
      }

      // Should we create a new PHI?
      if (phis_for_pointers.count(succ_BB) == 0) {
	assert(phis_for_indices.count(succ_BB) == 0);

	phis_for_pointers[succ_BB] = PHINode::Create(pointer_type,
						     0,
						     "incoming_arg_pointer",
						     succ_BB->getFirstNonPHI());
	phis_for_indices[succ_BB] = PHINode::Create(index_type,
						    0,
						    "incoming_arg_index",
						    succ_BB->getFirstNonPHI());
      }

      // Add to PHIs
      phis_for_pointers[succ_BB]->addIncoming(outgoing_pointers[current_BB],
					      current_BB);
      phis_for_indices[succ_BB]->addIncoming(outgoing_indices[current_BB],
					     current_BB);
    }

    // Do we need to add a detector?
    if ((exit_node) && (error_blocks.count(current_BB) != 1)) {

      // Split the exit node and place a detector on the new edge
      auto *split_pt = current_BB->getTerminator();
      auto *new_term_BB = current_BB->splitBasicBlock(split_pt,
						      "new_terminator_block");
      p_log(3, VERBOSE) << "Splitting terminator into "
			<< current_BB->getName()
			<< " and "
			<< new_term_BB->getName()
			<< "\n";

      assert(outgoing_pointers[current_BB] != nullptr);
      assert(outgoing_indices[current_BB] != nullptr);
      InsertDetector(pointer, cast<Value>(outgoing_pointers[current_BB]),
		     cast<Value>(outgoing_indices[current_BB]), current_BB,
		     new_term_BB, error_blocks);
    }
  }

  return true;
}
