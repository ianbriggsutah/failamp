

#include <csignal>
#include <unordered_set>

#include "wrappers/CFG.hpp"
#include "wrappers/Constants.hpp"
#include "wrappers/InstrTypes.hpp"
#include "wrappers/Instruction.hpp"
#include "wrappers/Instructions.hpp"
#include "wrappers/Module.hpp"

#include "Logging.hpp"
#include "TransformGEP.hpp"




using namespace std;
using namespace llvm;


void
InsertDetector(Value *original_ptr,
	       Value *walked_ptr,
	       Value *current_index,
	       BasicBlock *from_BB,
	       BasicBlock *to_BB,
	       unordered_set<BasicBlock*> &error_blocks)
{
  // Don't put detectors where the source or target Block are error blocks created by us
  if ((error_blocks.count(to_BB) == 1) || (error_blocks.count(from_BB))) {
    p_log(4, VERBOSE) << "Refusing to add detector between " << from_BB->getName() << " and " << to_BB->getName() << "\n";
    return;
  }

  p_log(4, VERBOSE) << "Inserting detector between " << from_BB->getName() << " and " << to_BB->getName() << "\n";

  // Create new block to check that indexing worked
  BasicBlock* check_BB = BasicBlock::Create(from_BB->getContext(), "indexing_check_block", from_BB->getParent(), 0);
  vector<Value*> indices_inside{current_index};
  ArrayRef<Value*> indices{indices_inside};
  GetElementPtrInst *compare_ptr = GetElementPtrInst::Create(cast<PointerType>(original_ptr->getType()->getScalarType())->getElementType(), original_ptr, indices, "compare_pointer", check_BB);
  LLVMContext& context = from_BB->getContext();
  IntegerType *int64_ty = Type::getInt64Ty(context);
  PtrToIntInst *walked_ptr_int = new PtrToIntInst(walked_ptr, int64_ty, "walked_pointer_int", check_BB);
  PtrToIntInst *compare_ptr_int = new PtrToIntInst(compare_ptr, int64_ty, "compare_pointer_int", check_BB);
  CmpInst *compare = CmpInst::Create(Instruction::ICmp,CmpInst::ICMP_NE, walked_ptr_int, compare_ptr_int, "compare", check_BB);
  p_log(5, VERBOSE) << "New check block: " << check_BB->getName() << "\n";

  // Create new error block for if the indexing failed
  BasicBlock* error_BB = BasicBlock::Create(from_BB->getContext(), "indexing_error_block", from_BB->getParent(), 0);
  error_blocks.insert(error_BB);
  p_log(5, VERBOSE) << "New error block: " << error_BB->getName() << "\n";
  // auto *retty = from_BB->getParent()->getReturnType();
  // if (retty->isVoidTy()) {
  //   auto *ret = ReturnInst::Create(context, nullptr, error_BB);
  // } else {
  //   auto *ret = ReturnInst::Create(context, Constant::getNullValue(retty), error_BB);
  // }

  Module *mod = from_BB->getModule();

  // Create raise function per POSIX.
  // Note: this is lacking attributes.
  Constant *raise_c = mod->getFunction("raise");
  Function *raise = cast<Function>(raise_c);

  // Give argument SIGUSR1 to raise and call it before returning in the error
  // branch.
  ArrayRef<Value*> raise_args(ConstantInt::get(context, APInt(32, SIGUSR1, true)));
  CallInst *raise_call = CallInst::Create(raise, raise_args, "raise_result", error_BB);
  MOVE_BEFORE(raise_call, *error_BB, error_BB->begin());


  // Connect the blocks: from->check
  //                     check(on fail)->error
  //                     check(on pass)->to
  //                     error->to

  // Go through the from block's terminators, if it was going to our 'to' block
  // then make it go to check
  auto *term = from_BB->getTerminator();
  bool found_to = false;
  for (unsigned i=0; i<term->getNumSuccessors(); i++) {
    auto *BB = term->getSuccessor(i);
    if (BB == to_BB) {
      term->setSuccessor(i, check_BB);
      found_to = true;
    }
  }
  if (!found_to) {
    internal_error("Unable to find destination block!");
  }
  // Connect check to error and check to 'to'
  BranchInst::Create(error_BB, to_BB, compare, check_BB);
  // connect error to 'to'
  BranchInst::Create(to_BB, error_BB);

  // Fix any PHIs
  Instruction *stopping_point = to_BB->getFirstNonPHIOrDbg();
  for (Instruction &I : *to_BB) {
    if (&I == stopping_point) {
      break;
    }

    auto *phi = dyn_cast<PHINode>(&I);
    if (phi == nullptr) {
      continue;
    }

    p_log(6, VERBOSE) << "Looking at phi: " << *phi << "\n";

    Value *v = nullptr;;
    for (unsigned i=0; i<phi->getNumIncomingValues(); i++) {
      auto *BB = phi->getIncomingBlock(i);
      if (BB == from_BB || BB == check_BB) {
	p_log(7, VERBOSE) << "Updating index: " << i << "\n";
	phi->setIncomingBlock(i, check_BB);
	// Add on a phi argument for the error block
	v = phi->getIncomingValue(i);
       }
    }
    if (v != nullptr) {
      p_log(7, VERBOSE) << "Adding error block phi arg: " << *v << "\n";
      phi->addIncoming(v, error_BB);
    }
  }
}
