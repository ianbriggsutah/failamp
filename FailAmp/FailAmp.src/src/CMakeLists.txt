include_directories(../include)
add_compile_options(-std=c++11)




if (${LLVM_VERSION_MAJOR} GREATER 7)
  add_llvm_library(LLVMFailAmp MODULE
    CleanPhis.cpp
    ContractStrided.cpp
    Demangle.cpp
    FailAmp.cpp
    FindIndexingType.cpp
    InsertDetector.cpp
    Logging.cpp
    ProcessArgPointer.cpp
    ProcessDependentPointer.cpp
    ProcessFunction.cpp
    TransformGEP.cpp
    )

else()
  add_llvm_loadable_module(LLVMFailAmp
    CleanPhis.cpp
    ContractStrided.cpp
    Demangle.cpp
    FailAmp.cpp
    FindIndexingType.cpp
    InsertDetector.cpp
    Logging.cpp
    ProcessArgPointer.cpp
    ProcessDependentPointer.cpp
    ProcessFunction.cpp
    TransformGEP.cpp
    )

endif()



install(
  TARGETS LLVMFailAmp
  DESTINATION lib
  COMPONENT library
  )
