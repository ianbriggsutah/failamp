

#include "FindIndexingType.hpp"
#include "Logging.hpp"

#include "wrappers/Instructions.hpp"

#include <unordered_set>




using namespace std;
using namespace llvm;


static bool
_FindIndexingType(Value *pointer,
		  Type **indexing_type,
		  unordered_set<PHINode*> &seen,
		  vector<Value*> &new_pointers)
{
  // Go through users
  bool found_type = false;
  for (auto *U : pointer->users()) {

    // Traverse PHIs
    auto *phi = dyn_cast<PHINode>(U);
    if (phi != nullptr) {
      auto *val = cast<Value>(phi);
      if (seen.count(phi) == 0) {
	seen.insert(phi);
	found_type |= _FindIndexingType(val, indexing_type, seen, new_pointers);
      }
    }

    // See if there is a direct load
    // THis is why we can't just return when we find the indexing type
    auto *load = dyn_cast<LoadInst>(U);
    if ((load != nullptr) && (load->getType()->isPointerTy())) {
      p_log(4, VERBOSE) << "Found dependent pointer from direct load: "
			<< *load
			<< "\n";
      auto *val = cast<Value>(load);
      new_pointers.push_back(val);
    }

    // Find a gep and get the indexing type
    auto *gep = dyn_cast<GetElementPtrInst>(U);
    if (!found_type && gep != nullptr) {
      if (gep->getNumIndices() != 1) {
	p_log(0, ALWAYS) << "Unable to handle: "
			 << *gep
			 << "\n";
	unsupported("Only 1D arrays are currently supported\n");
      }
      Value *original_idx = *gep->idx_begin();
      *indexing_type = original_idx->getType();
      found_type = true;
    }
  }

  return found_type;
}


void
FindIndexingType(Value *pointer,
		 Type **indexing_type,
		 vector<Value*> &new_pointers)
{
  unordered_set<PHINode*> seen;
  if (!_FindIndexingType(pointer, indexing_type, seen, new_pointers)) {
    warn() << "Unable to find pointer indexing type!\n";
    // warn() << "Unable to find pointer indexing type! Assuming i32\n";
    // LLVMContext& context = pointer->getContext();
    // *indexing_type = Type::getInt32Ty(context);
  }
}
