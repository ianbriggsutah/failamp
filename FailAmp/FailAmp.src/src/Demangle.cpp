

#include "Demangle.hpp"

#include <cassert>
#include <cxxabi.h>




using namespace abi;
using namespace llvm;
using namespace std;


string
demangle(const StringRef name)
{
  string s_name = name.str();
  return demangle(s_name);
}


string
demangle(const string &name)
{
  const char *c_name = name.c_str();
  return demangle(c_name);
}


string
demangle(const char *name)
{
  assert(name != nullptr);

  int status = -3;

  char *c_res;
  c_res = __cxa_demangle(name, nullptr, nullptr, &status);
  if (status == 0) {
    assert(c_res != nullptr);
  } else {
    c_res = (char*) malloc(sizeof(char) * (strlen(name)+1));
    assert(c_res != nullptr);
    memcpy(c_res, name, strlen(name)+1);
  }

  char *c = c_res;
  while (*c != '\0' && *c != '(') {
    c++;
  }
  *c = '\0';

  string res = string(c_res);
  free(c_res);

  return res;
}
